"""
Brain images example for stable article
"""

from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

name = "brain_test"

# Solver parameters
max_iters  = 5000
tolerance  = 1e-6
printEvery = 50
Nmax       = 7

# Physical paramters
mu    = 5.8
lamb  = 8.7
alpha = 1e4
dt    = 1e-2/alpha

# Load images
from PIL.Image import open as PilOpen
import numpy as np
Rimg = np.array(PilOpen("images/brainR.gif"))/255
Timg = np.array(PilOpen("images/brainT.gif"))/255


reg    = Regularizer.ElasticPrimalRegularizer(mu, lamb)
params = Parameters.ElasticRegistrationParamters()
params.mu      = mu
params.lamb    = lamb
params.alpha   = alpha
params.dt      = dt
params.bc_type = "Neumann"
params.time_regularization_norm = "H1"

u = None
# Do a multimesh approach
for n in range(3, Nmax + 1):
    N = 2**n
    print("Solver for {} elements per side".format(N))
    mesh = UnitSquareMesh(N, N)
    images = Images.NPYImages(mesh, Rimg, Timg)
    sim    = Similarity.SimilaritySSD(images)
    RP = RegistrationProblem.ElasticRegistration(sim, reg, images, params, mesh)
    
    solverParameters =  Parameters.solverParameters()
    solverParameters.max_iters = max_iters
    solverParameters.tolerance = tolerance 
    RP.setup()
    if u:
        RP.u_n.interpolate(u)
    sol = RP.solve(solverParameters, printEvery)
    u = sol[0].sub(RP.u_index).copy(True)
RP.export(name, "result")


# Then perform iterations with mixed solver
params.s_degree = 1
params.u_degree = 0
params.kernel_dimension = 3
params.dt = dt/alpha
params.time_regularization_norm = "L2"

RPmixed = RegistrationProblem.MixedElasticRegistration(sim, reg, images, params, mesh)

solverParameters =  Parameters.solverParameters()
solverParameters.max_iters = 1000
solverParameters.tolerance = tolerance 
RPmixed.setup()
RPmixed.u_n.interpolate(RP.u_n)
solMixed = RPmixed.solve(solverParameters, printEvery=1)
RPmixed.export(name, "result_mixed")
#  Visualization
import matplotlib.pyplot as plt

plt.subplot(121)
plot(RP.images.R, mesh=mesh)
plt.subplot(122)
plot(RP.images.T, mesh=mesh)
plt.show()

