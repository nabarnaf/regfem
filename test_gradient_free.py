#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 10:23:32 2019

@author: nico
"""

from dolfin import *
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True
set_log_level(40)
maxiter = 100
tol     = 1e-6
N = 50
mesh = UnitSquareMesh(N, N)

###### TEST FOR GRADIENT FREE ######
    
from RegFem.Images import TranslationImages
from RegFem.Similarity import SimilaritySSD
from RegFem.Regularizer import GradientPrimalRegularizer
from RegFem.KernelHandler import Translationize

im = TranslationImages(mesh)
sim = SimilaritySSD(im)
reg = GradientPrimalRegularizer()
alpha = Constant(10)
dt = Constant(1)

u_el = VectorElement('CG', triangle, 1)
tr_e = VectorElement('R', triangle, 0, 2)

V = FunctionSpace(mesh, MixedElement(u_el, tr_e, tr_e))

u, _rho, _lamb = TrialFunctions(V)
v, _xi, _eta = TestFunctions(V)
rho  = Translationize(_rho, mesh)
lamb = Translationize(_lamb, mesh)
xi   = Translationize(_xi, mesh)
eta  = Translationize(_eta, mesh)

u_n = Function(V.sub(0).collapse())
im.T.u = u_n

nu = FacetNormal(mesh)
F  = grad(u_n) + Identity(2)
a = 1/dt*dot(u, v) + reg.a(u, v) + dot(v - eta, rho) + dot(u - lamb, xi) 
a = a*dx
f = 1/dt*dot(u_n, v)*dx + alpha*sim.F*div(inv(F)*v)*dx - alpha*sim.F*dot(inv(F)*v, nu)*ds

sol = Function(V)
err = 1
it  = 0
while err > tol and it < maxiter:
    
    solve(a==f, sol)
    err = errornorm(sol.sub(0), u_n, degree_rise = 0)
    assign(u_n, sol.sub(0))
    if it%10==0: print(it, err, flush=True)
    it += 1


import matplotlib.pyplot as plt
plt.subplot(221)
plot(im.R, mesh=mesh)
plt.subplot(222)
plot(im.T, mesh=mesh)
plt.subplot(223)
u_n.vector().zero()
plot(im.T, mesh=mesh)
