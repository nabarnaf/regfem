from dolfin import *

N = 30
mesh = UnitSquareMesh(N, N)

# Problem types
PRIMAL = 0
MIXED  = 1

# Parameters
alpha = Constant(1e0)
dt    = Constant(1e-1)

# Fixed point iteration parameters
tol, maxiter = 1e-10, 100

# Function space
Vpr  = VectorFunctionSpace(mesh, 'CG', 1)
Vmix = VectorFunctionSpace(mesh, 'DG', 0)
p  = 7/2
x0 = 1e-5
f = lambda u: as_vector((1 - (dot(u,u))**p , 1 + (dot(u,u))**p)) * ln( x0 + sqrt(dot(u,u)))

# Error measure for each fixed point iteration
def error(u_n, u_nn):
	return sqrt(assemble(1 / dt / dt * dot(u_n - u_nn, u_n - u_nn) * dx))

# Convert global constants into ufl vectors
def const2vec(rm):
	return rm[0] * Constant((1,0)) + rm[1] * Constant((0,1))

def solve_primal_iteration(u_n):
	el_u  = VectorElement('CG', triangle, 1)
	el_rm = VectorElement('R', triangle, 0, 2)
	V = FunctionSpace(mesh, MixedElement(el_u, el_rm))
	u, _lm = TrialFunctions(V)
	v, _xi = TestFunctions(V)
	lm, xi = const2vec(_lm), const2vec(_xi)

	A = (inner(grad(u), grad(v)) + dot(u, xi) + dot(v, lm)) * dx
	L = alpha * ( dot(f(u_n), v))* dx

	sol = Function(V)
	solve(A == L, sol)
	return sol.sub(0)

def solve_mixed_iteration(u_n):
	el_u = VectorElement('DG', triangle, 0)
	el_s = VectorElement('BDM', triangle, 1)
	el_rm = VectorElement('R', triangle, 0, 2)
	V = FunctionSpace(mesh, MixedElement(el_s, el_u, el_rm))

	s, u, _lm = TrialFunctions(V)
	t, v, _xi = TestFunctions(V)
	lm, xi = const2vec(_lm), const2vec(_xi)

	A = (-dot(div(s), v) + inner(s, t) + dot(u, div(t)) - dot(v, lm) + dot(u, xi) ) * dx
	L = alpha * dot(f(u_n), v) * dx

	sol = Function(V)
	bc  = DirichletBC(V.sub(0), Constant( ((0, 0), (0, 0)) ), "on_boundary")
	solve( A == L, sol, bc)
	return sol.sub(1)

def solve_problem(problem_type = PRIMAL):
	u_n = Function(Vpr) if problem_type == PRIMAL else Function(Vmix)
	u_n.vector()[:] = 1
	err, it = 1, 0
	while err > tol and it < maxiter:
		if problem_type == PRIMAL:
			u_curr = solve_primal_iteration(u_n)
		elif problem_type == MIXED:
			u_curr = solve_mixed_iteration(u_n)
		else: 
			raise("Problem type not implemented")
		err    = error(u_n, u_curr)
		print("Iteration {}, error = {}".format(it, err))
		assign(u_n, u_curr)
		it += 1

solve_problem(PRIMAL)
solve_problem(MIXED)