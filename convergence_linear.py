#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for convergence of primal registration
"""


from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

name = "convergence_primal"

# Solver parameters
Nreference = 8

# Physical paramters
mu    = 1
lamb  = 1
alpha = 1e-4
beta  = 1


# Get reference solution
# print("------- Compute reference solution")
#meshRef = UnitSquareMesh(2**Nreference, 2**Nreference)
#uRef, _ = getSolution(meshRef)
# Get reference solution
print("------- Compute reference solution")
import sympy as sp
from time import time
from sympy.abc import x, y, t
from sympy.functions import Max, Min
from RegFem.sympy2fenics import grad, epsilon, hookeTensor, div, sympy2exp, infer_polynomial_degree

current_t = time()

# First create target image composition
u_ex  = 1e-4*sp.Matrix([0.1 * sp.cos(sp.pi*x) * sp.sin(sp.pi*y) + 0.5 * (x * (1 - x) * y * (1 - y))**2/lamb, 
                  -0.1 * sp.sin(sp.pi*x) * sp.cos(sp.pi*y) + 0.5*(x * (1 - x) * y * (1 - y))**2/lamb])

# Then generate differential operators
sigma = hookeTensor(epsilon(u_ex, 2), mu, lamb, 2)

# Finally create the rigid motion multipliers
integrate_x  = lambda f: sp.integrate(f, (x,0,1))
integrate_y  = lambda f: sp.integrate(f, (y,0,1))
integrate_2d = lambda f: integrate_x(integrate_y(f))

RMs = [sp.Matrix([1,0]), sp.Matrix([0,1]), sp.Matrix([-y + 0.5, x - 0.5])]
u_ex_0 = integrate_2d(u_ex.dot(RMs[0])) * RMs[0]
u_ex_1 = integrate_2d(u_ex.dot(RMs[1])) * RMs[1]
u_ex_2 = integrate_2d(u_ex.dot(RMs[2])) * RMs[2]
lamb_rm = u_ex_0 + u_ex_1 + u_ex_2
rho     = beta * lamb_rm

# Now create load
f_exp = -div(sigma) + rho

u_ex = Expression(sympy2exp(u_ex), degree = 10, cell = triangle)
# Generate relevant objects
f    = Expression(sympy2exp(f_exp), degree = 10, cell = triangle)


def getSolution(mesh):
    images = Images.TranslationImages(mesh)
    reg    = Regularizer.ElasticPrimalRegularizer(mu, lamb)
    sim    = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    params.mu      = mu
    params.lamb    = lamb
    params.alpha   = alpha
    params.bc_type = "Neumann"
    params.do_time_regularization = False

    RP = RegistrationProblem.ElasticRegistration(sim, reg, images, params, mesh)
    RP.setup()

    u_n = Function(RP.getDisplacementSpace())
    images = Images.TranslationImagesUFL(mesh, u_n) # EDITING HERE
    a  = RP.a
    tt = TestFunctions(RP.V)
    F = dot(f, tt[RP.u_index]) * dx(mesh)

    sol = Function(RP.V)
    solve(a == F, sol)
    u = Function(RP.V.sub(RP.u_index).collapse())
    assign(u, sol.sub(RP.u_index))
    return u, RP.V.dim()


dofs = []
hs   = []
eL2s = []
eH1s = []
for n in range(2, Nreference):
    print("Solving for {} elements per side".format(2**n))
    mesh = UnitSquareMesh(2**n, 2**n)
    u, dof = getSolution(mesh)
    eL2s.append(errornorm(u_ex, u, "L2"))
    eH1s.append(errornorm(u_ex, u, "H1"))
    dofs.append(dof)
    hs.append(mesh.hmax())
    
# Finally calculate errors
rL2s = []
rH1s = []
Nsteps = len(dofs)
for i in range(1, Nsteps):
    lnhh = ln(hs[i]/hs[i-1])
    rL2s.append(ln(eL2s[i]/eL2s[i-1])/lnhh)
    rH1s.append(ln(eH1s[i]/eH1s[i-1])/lnhh)
    
out_lines = []
names = "dofs & hh  &  eL2  &  rL2  &  eH1  &  rH1  \\\\"
print(names)
out_lines.append(names)
for i in range(Nsteps):
    if i ==0:
        out = " {} & {:.3e} &  {:.3e}  & --  & {:.3e}  &  -- \\\\".format(dofs[i], hs[i], eL2s[i], eH1s[i])
        print(out)
        out_lines.append(out)
    else:
        out = " {} & {:.3e} &  {:.3e}  & {:.3e}  & {:.3e}  &  {:.3e} \\\\".format(dofs[i], hs[i], eL2s[i], rL2s[i-1], eH1s[i], rH1s[i-1])
        print(out)
        out_lines.append(out)

#with open("linear_convergence_primal_{:.1E}".format(alpha), 'w') as f:
#    f.writelines(out_lines)
