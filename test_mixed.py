from dolfin import *
import matplotlib.pyplot as plt

set_log_level(100)
N = 20
mesh = UnitSquareMesh(N, N)
nu = FacetNormal(mesh)
# Elastic setting

lamb, mu = Constant(10), Constant(1)
alpha = Constant(50)
C     = lambda ten: lamb*tr(ten)*Identity(2)+2*mu*ten
Cinv  = lambda ten: 0.5/mu*ten - lamb/(4*mu*(lamb+mu))*tr(ten)*Identity(2)
eps   = lambda vec: sym(grad(vec))
dt    = 1/alpha**2
maxiter = 200
tol = 1e-4
printEvery = 10

from RegFem.Images import TranslationImages
from RegFem.Similarity import SimilaritySSD
from RegFem.RigidMotionsHandler import RigidMotionize


class Sides(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and (near(x[0],0.0) or near(x[0], 1.0))
    
class Covers(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and (near(x[1],0.0) or near(x[1], 1.0))
Sides = Sides(); Covers = Covers()
# Functional setting
s_el = VectorElement('BDM', triangle, 1) # H_div
skew_el = FiniteElement('DG', triangle, 0) # Skew tensor
u_el  = VectorElement('DG', triangle, 0) # Displacement
rm_el = VectorElement('R', triangle, 0, 3) # Rigid motions

V = FunctionSpace(mesh, MixedElement(s_el, rm_el, u_el, skew_el, rm_el))
bc_side = DirichletBC(V.sub(0).sub(0), Constant([0.,0.]), Sides)
bc_cover = DirichletBC(V.sub(0).sub(1), Constant([0.,0.]), Covers)
bcs = [bc_side, bc_cover]

_sigma, _rho, u, _phi, _l  = TrialFunctions(V)
_tau, _eta, v, _psi, _xi   = TestFunctions(V)

sigma = _sigma # transposed for boundary conditions
tau = _tau
phi = as_matrix([[0, _phi], [-_phi, 0]])
psi = as_matrix([[0, _psi], [-_psi, 0]])

# Now we extend RM modes for comfortability
toRM = lambda _x: RigidMotionize(_x, mesh)

rho = toRM(_rho)
l   = toRM(_l)
xi  = toRM(_xi)
eta = toRM(_eta)

# Bilinear forms and problem

dx = dx(mesh)
A = lambda sig, rho, tau, eta: inner(Cinv(sig), tau) 
B = lambda tau, eta, v, psi, xi: (dot(div(tau), v)
                            + inner(tau, psi)
                            + dot(xi - v, eta))
AA = A(sigma, rho, tau, eta)
BT = B(tau, eta, u, phi, l)
BB = B(sigma, rho, v, psi, xi)
CC = -dot(l, xi)
a =  AA + BT + BB + CC
dx = dx(mesh)
a = a*dx


im = TranslationImages(mesh)

sim = SimilaritySSD(im)
f = alpha*dot(sim.DF, v)*dx

# Time stab
s_n = Function(V.sub(0).collapse())
u_n = Function(V.sub(2).collapse())
l_n = Function(V.sub(4).collapse())
im.T.u=u_n
# non used terms
# - dot(l, xi)*dx, - dot(toRM(l_n), xi)*dx
a = dt*a - dot(u, v)*dx  #+ inner(sigma, tau)*dx
f = dt*f - dot(u_n, v)*dx  #+ inner(as_tensor([s_n.sub(0), s_n.sub(1)]).T, tau)*dx
sol = Function(V)
#u_n.interpolate(u_cg)
for i in range(maxiter):
    solve(a == f, sol, bcs)
    error = errornorm(sol.sub(2), u_n, degree_rise=0)/dt(0)
    assign(s_n.sub(0), sol.sub(0).sub(0))
    assign(s_n.sub(1), sol.sub(0).sub(1))
    assign(u_n, sol.sub(2))
    assign(l_n, sol.sub(4))
    if i%printEvery==0:
        print("iter {}, error={}".format(i, error))

    if error < tol:
        break

plt.subplot(131)
plot(im.T, mesh=mesh)
plt.subplot(132)
plot(im.R, mesh=mesh)
#plt.subplot(133)
#u_n.vector()[:] = 0.
#plot(T, mesh=mesh)