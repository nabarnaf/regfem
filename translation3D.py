"""
Translation example for stable article
"""

from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters

name = "translation3D"

# Solver parameters
max_iters = 1000
tolerance = 1e-6

# Physical paramters
mu    = 0.5
lamb  = 0.5
alpha = 10
dt    = 1/alpha

# Mesh
N = 4
mesh = UnitCubeMesh(N, N, N)

images = Images.TranslationImages3D(mesh)
reg    = Regularizer.ElasticPrimalRegularizer(mu, lamb)
sim    = Similarity.SimilaritySSD(images)
params = Parameters.ElasticRegistrationParamters()
params.mu      = mu
params.lamb    = lamb
params.alpha   = alpha
params.dt      = dt
params.bc_type = "Neumann"
params.kernel_dimension = 6
params.time_regularization_norm = "H1"

RP = RegistrationProblem.ElasticRegistration(sim, reg, images, params, mesh)
RP.setup()

solverParameters =  Parameters.solverParameters()
solverParameters.max_iters = max_iters
solverParameters.tolerance = tolerance 
sol = RP.solve(solverParameters)

# Export
#RP.export(name, "results")

#  Visualization
#import matplotlib.pyplot as plt
#
#plt.subplot(121)
#plot(RP.images.R, mesh=mesh)
#plt.subplot(122)
#plot(RP.images.T, mesh=mesh)
#plt.show()

