#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 22:03:49 2019

@author: nico
"""

from dolfin import *
import numpy as np
IMAGES_DEGREE = 6

function_composition_code = """

#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <dolfin/common/Array.h>
namespace py = pybind11;

#include <dolfin/function/Expression.h>
#include <dolfin/function/Function.h>

class Image: public dolfin::Expression
{
public:

  // Create expression with 1 components
  Image() : dolfin::Expression() {}

  // Function for evaluating expression on each cell
  void eval(Eigen::Ref<Eigen::VectorXd> values, Eigen::Ref<const Eigen::VectorXd> x) const override
  {
    Eigen::VectorXd dx  (2);
    Eigen::VectorXd xnew(2);

    u->eval(dx, x);
    xnew[0] =  x[0] + dx[0] + u2 * (-x[1] + 0.5);
    xnew[1] =  x[1] + dx[1] + u2 * (x[0] - 0.5);
    if(xnew[0]< 0. || xnew[0] > 1. || xnew[1] < 0. || xnew[1] > 1.){
        values[0] = 0.;
    }
    else{
        f->eval(values, xnew);
    }

  }
  std::shared_ptr<dolfin::Function> u;
  double u2;
  std::shared_ptr<dolfin::Function> f;
};

PYBIND11_MODULE(SIGNATURE, m)
{
  py::class_<Image, std::shared_ptr<Image>, dolfin::Expression>
    (m, "Image")
    .def(py::init<>())
    .def_readwrite("u", &Image::u)
    .def_readwrite("u2", &Image::u2)
    .def_readwrite("f", &Image::f);
}

"""


class Images:
  def __init__(self, R, T, dTx, dTy):
    self.R = R
    self.T = T
    self.dTx = dTx
    self.dTy = dTy
    if dTx and dTy:
      self.dT = as_vector([dTx, dTy])

  def generateDT(self, mesh, deg, T):
    VDG = FunctionSpace(mesh, 'DG', deg)
    dTx = project(T.dx(0), VDG)
    dTy = project(T.dx(1), VDG)
    return dTx, dTy

  def generateCompositionFunction(self, mesh, f, deg):
    V = FunctionSpace(mesh, 'CG', 1)
    u_temp = Function(V)
    return CompiledExpression(compile_cpp_code(function_composition_code).Image(),
                              u=u_temp.cpp_object(),
                              u2=0.0,
                              f=f.cpp_object(),
                              degree=deg,
                              domain=mesh)

  def setDisplacement(self, u):
    self.T.u = u
    self.dTx.u = u
    self.dTy.u = u


class TranslationImages(Images):
  def __init__(self, mesh, translation=(0, 0)):
    """
    Images are designed for a unit square domain. If required, a translation
    parameter can be used if Omega = [0,1]x[0,1] + (a, b)
    """
    var = 20
    cx, cy = translation
    x_c = "(x[0] - 0.3 - {})".format(cx)
    y_c = "(x[1] - 0.3 - {})".format(cy)
    xx = "pow({}, 2)".format(x_c)
    yy = "pow({}, 2)".format(y_c)
    R = Expression("exp(-var*({} + {}))".format(xx, yy),
                   degree=IMAGES_DEGREE, domain=mesh, var=var)
    x_c = "(x[0] + u[0] + u2 * (-x[1] + 0.5) - 0.7 - {})".format(cx)
    x_y = "(x[1] + u[1] + u2 * (x[0] - 0.5) - 0.7 - {})".format(cy)
    xx = "pow({}, 2)".format(x_c)
    yy = "pow({}, 2)".format(x_y)
    T = Expression("exp(-var*({} + {}))".format(xx, yy),
                   degree=IMAGES_DEGREE,
                   domain=mesh,
                   u=Constant((0., 0.)),
                   u2=0.0,
                   var=var)

    # dTx, dTy = self.generateDT(mesh, IMAGES_DEGREE - 1, T)
    dTx = Expression("-2 * var * (x[0] + u[0] + u2 * (-x[1] + 0.5) - 0.7) * exp(-var*({} + {}))".format(xx, yy),
                     degree=IMAGES_DEGREE + 1,
                     domain=mesh,
                     u=Constant((0., 0.)),
                     u2=0.0,
                     var=var)
    dTy = Expression("-2 * var * (x[1] + u[1] + u2 * (x[0] - 0.5) - 0.7) * exp(-var*({} + {}))".format(xx, yy),
                     degree=IMAGES_DEGREE + 1,
                     domain=mesh,
                     u=Constant((0., 0.)),
                     u2=0.0,
                     var=var)
    super().__init__(R, T, dTx, dTy)


class TranslationImages3D(Images):
  def __init__(self, mesh):
    """
    Images are designed for a unit square domain. If required, a translation
    parameter can be used if Omega = [0,1]x[0,1] + (a, b)
    """
    var = 20
    x_c = "(x[0] - 0.3 )"
    y_c = "(x[1] - 0.3 )"
    z_c = "(x[2] - 0.3 )"
    xx = "pow({}, 2)".format(x_c)
    yy = "pow({}, 2)".format(y_c)
    zz = "pow({}, 2)".format(z_c)
    R = Expression("exp(-var*({} + {} + {}))".format(xx, yy, zz),
                   degree=IMAGES_DEGREE, domain=mesh, var=var)
    x_c = "(x[0] + u[0] + u2 * (-x[1] + 0.5) - 0.7 )"
    y_c = "(x[1] + u[1] + u2 * (x[0] - 0.5) - 0.7)"
    z_c = "(x[2] + u[2] + u2[2] - 0.7)"
    xx = "pow({}, 2)".format(x_c)
    yy = "pow({}, 2)".format(y_c)
    zz = "pow({}, 2)".format(z_c)
    T = Expression("exp(-var*({} + {} + {}))".format(xx, yy, zz),
                   degree=IMAGES_DEGREE,
                   domain=mesh,
                   u=Constant((0., 0.)),
                   u2=0,
                   var=var)
    dTx, dTy = self.generateDT(mesh, IMAGES_DEGREE - 1, T)
    dTx = self.generateCompositionFunction(mesh, dTx, IMAGES_DEGREE)
    dTy = self.generateCompositionFunction(mesh, dTy, IMAGES_DEGREE)
    super().__init__(R, T, dTx, dTy)


class RotationImages(Images):
  def __init__(self, mesh, C=20, a=0.0):
    """
    Images are designed for a unit square domain. If required, a translation
    parameter can be used if Omega = [0,1]x[0,1] - (a, b).
    The target function is given by exp(-C([Rx]^T A Rx)), where R is the rotation matrix and A = [1 0; 0 a], which can be rewritten as
    T(x) = exp(-C(x^T M x)), with M = R^T A R. Also, x is a centered point, so x = x_real - 0.5.
    """
    theta = np.pi / 8
    R = np.array([[np.cos(theta), -np.sin(theta)],
                  [np.sin(theta), np.cos(theta)]])
    A = np.array([[1, 0], [0, a]])

    M = R.T @ A @ R  # Matrix product since python 3.5

    def matvec(A, x):
      return ["({A00})*({x0}) + ({A01})*({x1})".format(A00=A[0, 0], A01=A[0, 1], x0=x[0], x1=x[1]),
              "({A10})*({x0}) + ({A11})*({x1})".format(A10=A[1, 0], A11=A[1, 1], x0=x[0], x1=x[1])]

    def vecvec(x, y):
      return "({x0}) * ({y0}) + ({x1}) * ({y1})".format(x0=x[0], x1=x[1], y0=y[0], y1=y[1])

    xc = ("x[0] - 0.5", "x[1] - 0.5")
    xuc = ("x[0] + u[0] + u2 * (-x[1] + 0.5) - 0.5",
           "x[1] + u[1] + u2 * (x[0] - 0.5) - 0.5")

    R = Expression("exp(-C*({0}))".format(vecvec(xc, matvec(A, xc))),
                   C=C, degree=IMAGES_DEGREE, domain=mesh)

    T = Expression("exp(-C*({0}))".format(vecvec(xuc, matvec(M, xuc))), C=C,
                   degree=IMAGES_DEGREE, domain=mesh, u=Constant((0, 0), cell=triangle), u2=0.0)

    import sys
    dTx = Expression("-C * exp(-C*({0})) * {1}".format(vecvec(xuc, matvec(M, xuc)), matvec(M + M.T, xuc)[0]), C=C,
                     degree=IMAGES_DEGREE, domain=mesh,
                     u=Constant((0, 0), cell=triangle),
                     u2=0)
    dTy = Expression("-C * exp(-C*({0})) * {1}".format(vecvec(xuc, matvec(M, xuc)), matvec(M + M.T, xuc)[1]), C=C,
                     degree=IMAGES_DEGREE, domain=mesh,
                     u=Constant((0, 0), cell=triangle),
                     u2=0)

    # dTx, dTy = self.generateDT(mesh, IMAGES_DEGREE - 1, T)
    # dTx = self.generateCompositionFunction(mesh, dTx, IMAGES_DEGREE)
    # dTy = self.generateCompositionFunction(mesh, dTy, IMAGES_DEGREE)
    if sys.version_info[0] < 3:
      Images.__init__(self, R, T, dTx, dTy)
    else:
      super().__init__(R, T, dTx, dTy)


class NPYImages(Images):
  def __init__(self, mesh, Rarr, Tarr, generate_gradients=True):
    """
    Create functions from numpy arrays to avoid creating different classes
    for different image types
    """
    Rfun = self.generateImageFunction(Rarr, mesh)
    Tfun = self.generateImageFunction(Tarr, mesh)

    T = self.generateCompositionFunction(mesh, Tfun, IMAGES_DEGREE)
    if generate_gradients:
        dTx, dTy = self.generateDT(mesh, IMAGES_DEGREE - 1, Tfun)
        dTx = self.generateCompositionFunction(mesh, dTx, IMAGES_DEGREE)
        dTy = self.generateCompositionFunction(mesh, dTy, IMAGES_DEGREE)
    else:
        dTx = dTy = None

    import sys
    if sys.version_info[0] < 3:
      Images.__init__(self, Rfun, T, dTx, dTy)
    else:
      super().__init__(Rfun, T, dTx, dTy)

  def generateImageFunction(self, arr, mesh):
    """
    Code extract from fenics-diffeomorphic
    """
    # Create Function
    V = FunctionSpace(mesh, 'CG', 1)
    I = Function(V)

    # Array to store vertex values in
    import numpy as np
    vertex_values = np.zeros(mesh.num_vertices())

    # Loop through array
    if arr.ndim == 2:
      for vertex in vertices(mesh):
        Lx, Ly = arr.shape
        i = int(np.trunc((Lx - 1) * vertex.x(0)))
        j = int(np.trunc((Ly - 1) * vertex.x(1)))
        vertex_values[vertex.index()] = arr[i, j].item()
    elif arr.ndim == 3:
      for vertex in vertices(mesh):
        Lx, Ly, Lz = arr.shape
        i = int(np.trunc((Lx - 1) * vertex.x(0)))
        j = int(np.trunc((Ly - 1) * vertex.x(1)))
        k = int(np.trunc((Lz - 1) * vertex.x(2)))
        vertex_values[vertex.index()] = arr[i, j, k].item()

    # Assign vertex values at correct dofs of I
    I.vector().set_local(vertex_values[dof_to_vertex_map(V)])
    return interpolate(I, FunctionSpace(mesh, 'CG', 1))


class TranslationImagesUFL(Images):
  def __init__(self, mesh, u):

    var = 20
    x, y = SpatialCoordinate(mesh)
    xx = (x - 0.3)**2
    yy = (y - 0.3)**2
    R = exp(-var * (xx + yy))
    xx = (x + u[0] - 0.7) ** 2
    yy = (y + u[1] - 0.7) ** 2
    T = exp(-var * (xx + yy))

    dxx = 2 * (x + u[0] - 0.7)
    dyy = 2 * (x + u[1] - 0.7)
    dTx = -var * dxx * T
    dTy = -var * dyy * T
    super().__init__(R, T, dTx, dTy)
