#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 23:11:51 2018

@author: nico
"""

from dolfin import *

class TimeSolver:
    
    def __init__(self, form, dt, sol_n):
        self.form = form
        self.a = lhs(form)
        self.f = rhs(form)
        # TODO: Redo this with LinearVariationalProblem to set solver params, or maybe leave outside?
        self.A = assemble(self.a)
        self.F = assemble(self.F)
        self.sol = sol_n
        
    def solveTimeStep(self):
        pass
    
    def solve(self):
        pass