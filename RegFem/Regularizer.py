#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 22:03:06 2018

@author: nico
"""

from dolfin import *

# Auxiliary functions
def C(e, mu, lamb):
    """
    Hooke's tensor
    """
    dim  = e.ufl_shape[0]
    return 2*mu*e + lamb*tr(e)*Identity(dim)
def eps(u):
    """
    Symmetric part of gradient
    """
    return sym(grad(u))
def Cinv(e, mu, lamb):
    """
    Hooke's tensor
    """
    dim  = e.ufl_shape[0]
    mm = 2*mu
    return e/mm - lamb/(mm*(mm+dim*lamb))*tr(e)*Identity(dim)


class Regularizer:
        
    def __init__(self, name):
        self.name = name
        
    def a(self): pass


###########################################################
################# List of regularizers ####################
###########################################################
    

class ElasticPrimalRegularizer(Regularizer):
    """
    Regularizer given by elastic enery
    """    
    def __C(self, e):
        """
        Hooke's tensor
        """
        mu   = self.mu
        lamb = self.lamb
        dim  = e.ufl_shape[0]
        return 2*mu*e + lamb*tr(e)*Identity(dim)
    def __eps(self, u):
        """
        Symmetric part of gradient
        """
        return sym(grad(u))
    
    def __init__(self, mu, lamb):
        import sys
        if sys.version_info[0] < 3:
            Regularizer.__init__(self, "Elastic")
        else:
            super().__init__("Elastic")
        self.mu   = Constant(mu)
        self.lamb = Constant(lamb)
        
    def energy(self, u):
        """
        Energy potential associated to regularizer
        """
        return 0.5*inner(self.__C(self.__eps(u)), self.__eps(u))
    
    def a(self, u, v):
        """
        Neumann elastic regularizer, includes rigid motion vectors
        for guaranteeing uniqueness
        """
        C   = self.__C
        eps = self.__eps
        return inner(C(eps(u)), eps(v))
    

class GradientPrimalRegularizer(Regularizer):
    """
    Regularizer given by elastic enery
    """    

    def __init__(self):
        import sys
        if sys.version_info[0] < 3:
            Regularizer.__init__(self, "Gradient")
        else:
            super().__init__("Elastic")
        
    def energy(self, u):
        """
        Energy potential associated to regularizer
        """
        return 0.5*inner(grad(u), grad(u))
    
    def a(self, u, v):
        """
        Neumann elastic regularizer, includes rigid motion vectors
        for guaranteeing uniqueness
        """
        return inner(grad(u), grad(v))
    

class RigidMotionRegularizer(Regularizer):
    """
    Extended term which hadles regularization of the elastic kernel
    """
    def __init__(self):
        super().__init__("RM")
        
    def energy(self, xi):
        return 0.5*dot(xi, xi)
    
    def a(self, lamb, xi):
        return dot(lamb, xi)
        

class ElasticMixedRegularizer(Regularizer):
    
    def __init__(self, mu=0.5, lamb=0.5):
        super().__init__("MixedElastic")
        self.mu = Constant(mu)
        self.lamb = Constant(lamb)
        
    def energy(self):
        raise ValueError("No energy implemented for Mixed scheme")
        
    def a(self, s, rho, u, phi, tau, eta, v, psi):
        
        a  = inner(Cinv(s, self.mu, self.lamb), tau)
        B = lambda _t, _e,_v, _X: dot(_v, div(_t))+inner(_t, _X)-dot(_v, _e) 
        bt = B(tau, eta, u, phi)
        b  = B(s, rho, v, psi)
        return a+bt+b