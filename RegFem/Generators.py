#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 26 20:50:01 2019

@author: nico
"""

from dolfin import *

def generatePrimalFunctionSpace(mesh, u_degree=1, bc_type="Neumann", do_extended=True, kernel_dimension=3):
    """
    Generate function spaces for primal formulation depending on the use
    of the extended formulation
    """
    el1 = VectorElement('CG', mesh.ufl_cell(), u_degree)
    el2 = VectorElement('R', mesh.ufl_cell(), 0, kernel_dimension)
    
    if bc_type=="Dirichlet":
        V = FunctionSpace(mesh, el1)
        return V, 0
    else:
        
        if do_extended:
            V  = FunctionSpace(mesh, MixedElement(el1, el2, el2))
        else:
            V  = FunctionSpace(mesh, MixedElement(el1, el2))
            
        u_index = 0    
        return V, u_index 
    
def generateMixedFunctionSpace(mesh, u_degree=0, s_degree=1, do_extended=True, kernel_dimension=3):
    """
    Generate function spaces for mixed formulation/
    args:
        u_degree    : DG degree
        s_degree    : BDM degree
        do_extended : Flag for extended formulation
        kernel_dimension  : Dimension of kernel vector element
    """
    el_u  = VectorElement("DG", mesh.ufl_cell(), u_degree)
    el_ub = FiniteElement("R", mesh.ufl_cell(), 0, 1)
    el_s  = VectorElement("BDM", mesh.ufl_cell(), 1)
    el_k  = VectorElement("R", mesh.ufl_cell(), 0, kernel_dimension)
    el_skew = FiniteElement("DG", mesh.ufl_cell(), 0) 

    if not do_extended:
        V = FunctionSpace(mesh, MixedElement(el_s, el_k, el_u, el_skew))
        u_index = 2
        return V, u_index
    else:
        V = FunctionSpace(mesh, MixedElement(el_s, el_k, el_u, el_ub, el_skew, el_k))
        u_index = 2
        return V, u_index