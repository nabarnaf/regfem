#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 09:30:00 2019

@author: barnafi
"""

from dolfin import *

def TranslationsBase(dim, mesh):
    """
    Generates the base for the rigid motions space. Currently it does not
    orthogonalize the base.
    """
    area = assemble(1*dx(mesh))
    if dim==2:
        e1   = Constant((1,0), cell=triangle)/area
        e2   = Constant((0,1), cell=triangle)/area

        return e1, e2
    elif dim==3:
        e1 = Constant((1, 0, 0))/area
        e2 = Constant((0, 1, 0))/area
        e3 = Constant((0, 0, 1))/area
        
        return e1, e2, e3
    
    
def Translationize(vec, mesh):
    """
    Takes a scalar vector of RM coefficients and converts it to the 
    corresponding rigid motioin
    """
    dim = mesh.topology().dim()
    TR  = RigidMotionBase(dim, mesh)
    
    out = 0*TR[0]  # The compiler later optimizes zero terms and doesn't add them
    for val, rm in zip(vec, TR):
        out += val*rm
    return out
    
        

def RigidMotionBase(dim, mesh):
    """
    Generates the base for the rigid motions space. Currently it does not
    orthogonalize the base.
    """
    area = assemble(1*dx(mesh))
    if dim==2:
        e1   = Constant((1,0), cell=triangle)/area
        e2   = Constant((0,1), cell=triangle)/area
        
        e3   = Expression(['-x[1]', 'x[0]'], degree=1, domain=mesh, cell=triangle)
        e3norm = norm(e3)
        e3 = e3/e3norm
        
        e2e3 = assemble(dot(e2,e3)*dx)
        e1e3 = assemble(dot(e1,e3)*dx)
        return e1, e2, (e3 - e2e3*e2 - e1e3*e1)
    elif dim==3:
        x  = Expression(['x[0]', 'x[1]', 'x[2]'], degree=1, domain=mesh, cell=tetrahedron)
        e1 = Constant((1, 0, 0))/area
        e2 = Constant((0, 1, 0))/area
        e3 = Constant((0, 0, 1))/area
        P = lambda f, e: assemble(dot(f, e)*dx)*e
        e4 = cross(x, e1) 
        e4 = e4 - P(e4, e1) - P(e4, e2) - P(e4, e3)
        e5 = cross(x, e2)
        e5 = e5 - P(e5, e1) - P(e5, e2) - P(e5, e3)
        e6 = cross(x, e3)
        e6 = e6 - P(e6, e1) - P(e6, e2) - P(e6, e3)
        return e1, e2, e3, e4, e5, e6
    
    
def RigidMotionize(vec, mesh):
    """
    Takes a scalar vector of RM coefficients and converts it to the 
    corresponding rigid motioin
    """
    dim = mesh.topology().dim()
    RM  = RigidMotionBase(dim, mesh)
    
    out = 0*RM[0]  # The compiler later optimizes zero terms and doesn't add them
    for val, rm in zip(vec, RM):
        out += val*rm
    return out
    
        