#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  4 15:29:44 2019

@author: nico
"""

class ProblemParameters:
    
    def __init__(self, **kwargs):
        keys = kwargs.keys()
        
        self.alpha = kwargs["alpha"] if "alpha" in keys else 1.0
        self.beta = kwargs["beta"] if "beta" in keys else 1.0
        self.dt    = kwargs["dt"] if "dt" in keys else 1.0
        
        self.u_degree = kwargs["u_degree"] if "u_degree" in keys else 1
        assert self.u_degree >= 1, "Displacement degree must be a positive integer"
        
        # Stress tensor degree for mixed problems
        self.s_degree = kwargs["s_degree"] if "s_degree" in keys else 1
        
        self.bc_type = kwargs["bc_type"] if "bc_type" in keys else "Neumann"
        assert self.bc_type in ["Neumann", "Dirichlet"], "bc_type can be: Dirichlet, Neumann"
        
        self.do_time_regularization = kwargs["do_time_regularization"] if "do_time_regularization" in keys else True
        if self.do_time_regularization:
            self.time_regularization_norm = kwargs["time_regularization_norm"] if "time_regularization_norm" in keys else "L2"
        self.do_extended = kwargs["do_extended"] if "do_extended" in keys else True
            
class solverParameters:
    
    def __init__(self, **kwargs):
        keys = kwargs.keys()
        
        self.tolerance =  kwargs["tolerance"] if "tolerance" in keys else 1e-3
        self.max_iters = kwargs["max_iters"] if "max_iters" in keys else 100
        
        self.stopping_criterion = kwargs["stopping_criterion"] if "stopping_criterion" in keys else "residual"
        assert self.stopping_criterion in ["residual", "similarity", "velocity"], "Stopping criterion can be: residual, similarity or velocity"
        
        if self.stopping_criterion == "similarity":
            if "similarity_threshold" not in keys:
                raise ValueError("similarity_threshold must be given if similarity stopping criterion is used")
            self.similarity_threshold = kwargs["similarity_threshold"]
        
class ElasticRegistrationParamters(ProblemParameters):
    
    # Default values
    ElasticRegistrationParameters = { "mu"    : 0.5,
                                      "lamb"  : 0.5,
                                      "kernel_dimension": 3}
    
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
        keys = kwargs.keys()
        self.mu   = kwargs["mu"] if "mu" in keys else 0.5
        self.lamb = kwargs["lamb"] if "lamb" in keys else 0.5
        
        self.kernel_dimension = 3