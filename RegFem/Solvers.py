#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  4 15:28:44 2019

@author: nico
"""

from dolfin import *

class LinearSolver:

    def __applyBC(self, genericLA):
        if self.bc:
            if isinstance(self.bc, list):
                for b in self.bc:
                    b.apply(genericLA)
            else:
                self.bc.apply(genericLA)
    def __init__(self, a, f, bc=None, V=None):
        self.a  = a
        self.f  = f
        self.bc = bc

        self.A = PETScMatrix()
        assemble(a, tensor=self.A)
        self.__applyBC(self.A)

        # This is used by mixed method to improve conditioning due to imposition
        # of BCs through diagonalization. It sets the avg diagonal value instead of 1.
        if V:
            dofs = V.sub(0).dofmap().dofs()
            dofs_bc = bc[0].get_boundary_values().keys()  # Only one BC! Hard coded though...

            dofs_not_bc = [i for i in dofs if i not in dofs_bc]

            Amat = self.A.mat()
            count = 0
            vals = 0 
            for i in dofs_not_bc:
                vals += Amat.getValue(i, i)
                count += 1
            avg_val = vals / count  # Average diagonal value on non_bc dofs
            for i in dofs_bc:
                Amat.setValue(i, i, avg_val)
            Amat.assemble()  # setting values disassembles matrix

        # self.LU = LUSolver(self.A)#, method="default")

    def solve(self, solverParameters, Sol, f=None):

        if f:
            F = assemble(f)
        else:
            F = assemble(self.f)
        self.__applyBC(F)

        # self.LU.solve(Sol.vector(), F)
        solver = PETScKrylovSolver("gmres", "ilu")
        # solver.parameters["monitor_convergence"] = True
        solver.parameters["nonzero_initial_guess"] = True
        solver.parameters["error_on_nonconvergence"] = False
        solver.parameters["maximum_iterations"] = 2 * F.size()
        solver.solve(self.A, Sol.vector(), F)
        return Sol  # Redundant because Sol gets updated internally
