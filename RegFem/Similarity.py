#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 22:03:16 2018

@author: nico
"""

from dolfin import *

class Similarity:
    """
    Abstract class which contains a similarity measure. It holds its expression
    as well as its gradient and hessian for Newton iterations. 
    """
    def __init__(self, images, name):
        self.R = images.R
        self.T = images.T
        self.dT = images.dT
        self.name = name
        self.F   = self.F()
        self.DF  = self.DF()
        self.DDF = self.DDF()
        
    def F(self): pass
    def DF(self): pass
    def DDF(self): pass


#######################################################
################# List of measures ####################
#######################################################

class SimilaritySSD(Similarity):
    """
    Sum of squared differences similarity measure
    """
    def __init__(self, images):
        import sys
        if sys.version_info[0] < 3:
            Similarity.__init__(self, images, "SSD")
        else:
            super().__init__(images, "SSD")
    def F(self):
        return 0.5*(self.T - self.R)**2
    def DF(self):
        return (self.T - self.R)*self.dT
    def DDF(self):
        DT = self.dT
        return outer(DT, DT) + (self.T-self.R)*grad(DT)