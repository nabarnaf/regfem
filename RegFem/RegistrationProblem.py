#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 22:02:14 2018

@author: Nicolas Barnafi
"""

from dolfin import *

class RegistrationProblem:

    def __init__(self, similarity, regularizer, parameters, mesh):
        self.similarity = similarity
        self.parameters = parameters
        self.regularizer = regularizer
        self.mesh = mesh
        self.images = None
        self.u_n = None

    def setup(self): pass
    def solve(self, solverParameters): pass

    def export(self, directory, filename):
        xdmf = XDMFFile("images/{}/{}.xdmf".format(directory, filename))
        xdmf.parameters["functions_share_mesh"] = True
        xdmf.parameters["flush_output"] = True

        # Save solution field and images
        V = FunctionSpace(self.mesh, 'CG', 2)

        # Displacement
        self.u_n.rename("u", "u")
        xdmf.write(self.u_n, 1.0)

        # Reference image
        R = interpolate(self.images.R, V)
        R.rename("R", "R")
        xdmf.write(R, 1.0)

        # Target images
        T = interpolate(self.images.T, V)
        T.rename("T", "T")
        xdmf.write(T, 1.0)

        # Save unwarped target image
        uaux = self.u_n.copy(True)
        u2 = self.images.T.u2
        self.images.T.u.vector().zero()
        self.images.T.u2 = 0
        T = interpolate(self.images.T, V)
        T.rename("T0", "T0")
        xdmf.write(T, 1.0)
        self.images.T.u.vector().set_local(uaux.vector())
        self.images.T.u2 = u2
        xdmf.close()


class ElasticRegistration(RegistrationProblem):
    """
    Registration problem with null Neumann boundary conditions. If no modification
    is performed to the formulation, the solution will be orthogonal to rigid
    motions. It is solved with a fixed point method.
    """

    def __init__(self, similarity, regularizer, images, parameters, mesh):
        super().__init__(similarity, regularizer, parameters, mesh)
        self.u_degree = parameters.u_degree
        self.kernel_dimension = parameters.kernel_dimension
        self.parameters = parameters
        self.images = images
        self.mu = Constant(parameters.mu)
        self.lamb = Constant(parameters.lamb)
        self.alpha = Constant(parameters.alpha)
        self.bc_type = parameters.bc_type

    def setup(self):
        """
        Setup problem's elements: Bilinear and linear forms, functional space,
        current solution.
        """

        # Create function space and problem
        from RegFem.Generators import generatePrimalFunctionSpace
        V, u_index = generatePrimalFunctionSpace(self.mesh, self.u_degree,
                                                 self.bc_type,
                                                 self.parameters.do_extended,
                                                 self.parameters.kernel_dimension)
        self.V = V
        self.u_index = u_index

        u_n = Function(self.getDisplacementSpace())
        self.u_n = u_n
        self.images.T.u = u_n.cpp_object()
        self.images.dTx.u = u_n.cpp_object()
        self.images.dTy.u = u_n.cpp_object()

        self.__generateForms(V)

        # Setup BC
        if self.bc_type == "Dirichlet":
            self.bc = DirichletBC(V, Constant((0, 0)), "on_boundary")
        else:
            self.bc = None

    def solve(self, solverParameters, printEvery=10):
        """
        Solve problem
        """
        from RegFem.Solvers import LinearSolver
        solver = LinearSolver(self.a, self.f, self.bc)
        Sol = Function(self.V)

        max_iters = solverParameters.max_iters
        tol = solverParameters.tolerance

        err, it = 1, 0
        while err > tol and it < max_iters:

            solver.solve(solverParameters, Sol, self.f)

            err = self.__calculateIterationError(solverParameters, Sol)
            if self.bc_type == "Dirichlet":
                assign(self.u_n, Sol)
            else:
                assign(self.u_n, Sol.sub(self.u_index))
            it += 1
            if it % printEvery == 0:
                print("Iteration {}, error={:.3e}".format(it, err))
        return Sol, it

    def getDisplacementSpace(self):
        """
        Requires u_index to be assigned during setup, as well as V
        """
        if self.bc_type == "Dirichlet":
            return self.V
        else:
            return self.V.sub(self.u_index).collapse()

    def __calculateIterationError(self, solverParameters, Sol):
        if solverParameters.stopping_criterion == "similarity":
            sim = assemble(self.similarity.F * dx)
            return sim - solverParameters.similarity_threshold
        elif solverParameters.stopping_criterion == "residual":
            u = Sol.split(True)[0]
            return (u.vector() - self.u_n.vector()).norm('linf')
            # return errornorm(Sol.sub(0), self.u_n, "L2", degree_rise=0)
        elif solverParameters.stopping_criterion == "velocity":
            return errornorm(Sol.sub(0), self.u_n, "L2", degree_rise=0) / self.parameters.dt

    def __generateForms(self, V):

        # Packages to be used
        from RegFem.RigidMotionsHandler import RigidMotionize
        from RegFem.Regularizer import ElasticPrimalRegularizer
        from RegFem.FormGenerator import getTimeRegularizationForms, getExtendedTerms, getRMorthogonalization

        # Set flags
        do_extended = self.parameters.do_extended

        # Construct global components
        Reg = ElasticPrimalRegularizer(self.mu, self.lamb)
        DF = self.similarity.DF

        # Set components according to boundary conditions
        if self.bc_type == "Dirichlet":
            u = TrialFunction(V)
            v = TestFunction(V)

            a = Reg.a(u, v)
            f = -self.alpha * dot(DF, v)
            # Setup problem
            self.a = a
            self.f = f
        elif self.bc_type == "Neumann":
            if do_extended:
                u, __rho, __lamb = TrialFunctions(V)
                v, __xi, __eta = TestFunctions(V)

                rho = RigidMotionize(__rho, self.mesh)
                lamb = RigidMotionize(__lamb, self.mesh)
                xi = RigidMotionize(__xi, self.mesh)
                eta = RigidMotionize(__eta, self.mesh)

                a = Reg.a(u, v)
                f = -self.alpha * dot(DF, v)
                # Set RM orthogonality terms
                a += getRMorthogonalization(u, rho, v, xi)
                # Add extended components
                a += getExtendedTerms(rho, lamb, xi, eta, self.parameters.beta)

                self.a = a
                self.f = f
            else:
                u, __rho = TrialFunctions(V)
                v, __xi = TestFunctions(V)

                rho = RigidMotionize(__rho, self.mesh)
                xi = RigidMotionize(__xi, self.mesh)

                # Set RM orthogonality terms
                a = Reg.a(u, v)
                f = -self.alpha * dot(DF, v)
                a += getRMorthogonalization(u, rho, v, xi)

                self.a = a
                self.f = f

        # Add time regularization
        if self.parameters.do_time_regularization:
            adt, fdt = getTimeRegularizationForms(
                u, v, self.u_n, self.parameters)
            self.a += adt
            self.f += fdt

        self.a = self.a * dx(self.mesh)
        self.f = self.f * dx(self.mesh)


class MixedElasticRegistration(RegistrationProblem):
    """
    Mixed Registration problem with null Neumann boundary conditions. It is solved with a fixed point method.
    """

    def __init__(self, similarity, regularizer, images, parameters, mesh):
        super().__init__(similarity, regularizer, parameters, mesh)
        self.u_degree = parameters.u_degree
        self.s_degree = parameters.s_degree  # Sigma degree for BDM elements
        self.kernel_dimension = parameters.kernel_dimension
        self.parameters = parameters
        self.images = images
        self.mu = Constant(parameters.mu)
        self.lamb = Constant(parameters.lamb)
        self.alpha = Constant(parameters.alpha)
        self.bc_type = parameters.bc_type

    def setup(self):
        """
        Setup problem's elements: Bilinear and linear forms, functional space,
        current solution.
        """

        from RegFem.Generators import generateMixedFunctionSpace
        V, u_index = generateMixedFunctionSpace(self.mesh,
                                                u_degree=self.u_degree,
                                                s_degree=self.parameters.s_degree,
                                                do_extended=self.parameters.do_extended,
                                                kernel_dimension=self.parameters.kernel_dimension)

        self.V = V
        self.u_index = u_index

        u_n = Function(self.getDisplacementSpace())
        self.u_n = u_n
        self.u2 = 0.0
        self.images.T.u = u_n.cpp_object()
        self.images.dTx.u = u_n.cpp_object()
        self.images.dTy.u = u_n.cpp_object()
        # self.images.T.u2 = self.u2.cpp_object()
        # self.images.dTx.u2 = self.u2.cpp_object()
        # self.images.dTy.u2 = self.u2.cpp_object()

        self.__generateForms(V)
        # Setup BC

        newbc = DirichletBC(V.sub(0), Constant(
            ((0, 0), (0, 0))), "on_boundary")
        self.bc = [newbc]

    def solve(self, solverParameters, printEvery=10):
        """
        Solve problem
        """
        from RegFem.Solvers import LinearSolver
        from RegFem.RigidMotionsHandler import RigidMotionBase
        solver = LinearSolver(self.a, self.f, self.bc, self.V)
        Sol = Function(self.V)

        max_iters = solverParameters.max_iters
        tol = solverParameters.tolerance

        err, it = 1, 0
        # _, _, e2 = RigidMotionBase(self.mesh.topology().dim(), self.mesh)
        # V0 = VectorFunctionSpace(self.mesh, 'CG', 1)
        # e2 = project(e2, V0)
        # e2_copy = e2.copy(True)
        while err > tol and it < max_iters:
            # e2_copy.vector().zero()
            solver.solve(solverParameters, Sol)
            err = self.__calculateIterationError(solverParameters, Sol)

            # Update previous solution
            assign(self.u_n, Sol.sub(self.u_index))
            self.u2 = float(Sol.sub(self.u_index + 1)(0, 0))
            self.images.T.u2 = self.u2
            self.images.dTx.u2 = self.u2
            self.images.dTy.u2 = self.u2
            it += 1
            if it % printEvery == 0:
                print("Iteration {}, error={:.3e}".format(it, err))
        return Sol, it

    def getDisplacementSpace(self):
        """
        Requires u_index to be assigned during setup, as well as V
        """
        return self.V.sub(self.u_index).collapse()

    def __calculateIterationError(self, solverParameters, Sol):
        if solverParameters.stopping_criterion == "similarity":
            sim = assemble(self.similarity.F * dx)
            return sim - solverParameters.similarity_threshold
        elif solverParameters.stopping_criterion == "residual":

            from RegFem.RigidMotionsHandler import RigidMotionBase
            _, _, e2 = RigidMotionBase(self.mesh.topology().dim(), self.mesh)
            u0, ub = Sol.sub(self.u_index), Sol.sub(self.u_index + 1)
            sol_u = u0 + ub * e2
            un = self.u_n + Constant(self.u2) * e2
            err_u = sol_u - un
            error = assemble(dot(err_u, err_u) * dx)
            return error
            # return errornorm(Sol.sub(self.u_index), self.u_n, "L2", degree_rise=0)
        elif solverParameters.stopping_criterion == "velocity":
            return errornorm(Sol.sub(self.u_index), self.u_n, "L2", degree_rise=0) / self.parameters.dt

    def __generateForms(self, V):

        # Packages to be used
        from RegFem.RigidMotionsHandler import RigidMotionize, RigidMotionBase
        from RegFem.Regularizer import ElasticMixedRegularizer
        from RegFem.FormGenerator import getTimeRegularizationForms, getExtendedTerms, getRMorthogonalization

        # Set flags
        do_extended = self.parameters.do_extended

        # Construct global components
        Reg = ElasticMixedRegularizer(self.mu, self.lamb)
        DF = self.similarity.DF
        _, _, e2 = RigidMotionBase(self.mesh.topology().dim(), self.mesh)

        if do_extended:
            s, __rho, __u, __ub, __phi, __lamb = TrialFunctions(V)
            tau, __eta, __v, __vb, __psi, __xi = TestFunctions(V)

            u = __u + __ub * e2
            v = __v + __vb * e2

            rho = RigidMotionize(__rho, self.mesh)
            lamb = RigidMotionize(__lamb, self.mesh)
            xi = RigidMotionize(__xi, self.mesh)
            eta = RigidMotionize(__eta, self.mesh)
            phi = as_matrix([[0, __phi], [-__phi, 0]])
            psi = as_matrix([[0, __psi], [-__psi, 0]])

            a = Reg.a(s, rho, u, phi, tau, eta, v, psi)
            f = self.alpha * dot(DF, v)
            # Add extended components, must use changed sign
            a += -getExtendedTerms(rho, lamb, eta, xi, self.parameters.beta)

            self.a = a
            self.f = f
        else:
            s, __rho, u, __phi = TrialFunctions(V)
            tau, __eta, v, __Xi = TestFunctions(V)

            rho = RigidMotionize(__rho, self.mesh)
            xi = RigidMotionize(__xi, self.mesh)
            phi = as_matrix([[0, __phi], [-__phi, 0]])
            Xi = as_matrix([[0, __Xi], [-__Xi, 0]])

            # Set RM orthogonality terms
            a = Reg.a(s, rho, u, phi, tau, eta, v, Xi)
            f = self.alpha * dot(DF, v)
            self.a = a
            self.f = f

        # Add time regularization, inverted for mixed scheme
        if self.parameters.do_time_regularization:
            u_n = self.u_n + self.u2 * e2
            adt, fdt = getTimeRegularizationForms(
                u, v, u_n, self.parameters)
            self.a -= adt
            self.f -= fdt

        self.a = self.a * dx
        self.f = self.f * dx
