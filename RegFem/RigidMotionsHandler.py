#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 00:15:54 2018

@author: Nicolas Barnafi
"""

from dolfin import *


def RigidMotionBase(dim, mesh):
    """
    Generates the base for the rigid motions space. Currently it does not
    orthogonalize the base.
    """
    area = assemble(1 * dx(mesh))
    if dim == 2:
        e1 = Constant((1, 0), cell=triangle)
        e2 = Constant((0, 1), cell=triangle)

        # Do not modify, works like this also on Images
        e3 = Expression(['-x[1] + 0.5', 'x[0] - 0.5'], degree=1,
                        domain=mesh, cell=triangle)
        return e1, e2, e3
    elif dim == 3:
        x = Expression(['x[0]', 'x[1]', 'x[2]'], degree=1,
                       domain=mesh, cell=tetrahedron)
        e1 = Constant((1, 0, 0)) / area
        e2 = Constant((0, 1, 0)) / area
        e3 = Constant((0, 0, 1)) / area
        def P(f, e): return assemble(dot(f, e) * dx) * e
        e4 = cross(x, e1)
        e4 = e4 - P(e4, e1) - P(e4, e2) - P(e4, e3)
        e5 = cross(x, e2)
        e5 = e5 - P(e5, e1) - P(e5, e2) - P(e5, e3)
        e6 = cross(x, e3)
        e6 = e6 - P(e6, e1) - P(e6, e2) - P(e6, e3)
        return e1, e2, e3, e4, e5, e6


def RigidMotionize(vec, mesh):
    """
    Takes a scalar vector of RM coefficients and converts it to the 
    corresponding rigid motioin
    """
    dim = mesh.topology().dim()
    RM = RigidMotionBase(dim, mesh)

    # The compiler later optimizes zero terms and doesn't add them
    out = 0 * RM[0]
    for val, rm in zip(vec, RM):
        out += val * rm
    return out
