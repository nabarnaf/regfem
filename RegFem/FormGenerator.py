#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 13 21:41:05 2019

@author: Nicolas Barnafi
"""

from dolfin import *

def getTimeRegularizationForms(u, v, u_n, parameters):
    reg_type = parameters.time_regularization_norm
    if reg_type == "H1":
        inner_prod = lambda x, y: (dot(x, y) + inner(grad(x), grad(y)))
    elif reg_type == "L2":
        inner_prod = lambda x, y: dot(x,y)
    else: 
        raise ValueError("Time regularization norm type {} not supported yet".format(reg_type))
    dt = Constant(parameters.dt)
    return inner_prod(u, v)/dt, inner_prod(u_n, v)/dt

def getRMorthogonalization(u, rho, v, xi):
    return (dot(u, xi) + dot(v, rho))

def getExtendedTerms(rho, lamb, eta, xi, beta):
    return (-dot(rho, xi) - dot(lamb, eta) + Constant(beta) * dot(lamb, xi))

