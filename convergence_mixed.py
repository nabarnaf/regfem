#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for convergence of mixed registration
"""


from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
set_log_active(False)
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

name = "convergence_mixed"

# Solver parameters
max_iters = 100
tolerance = 1e-10
Nreference = 7

# Physical paramters
mu    = 1
lamb  = 1
alpha = 1e-1
beta = 1


def getSolution(mesh):
    images = Images.TranslationImages(mesh)
    reg    = Regularizer.ElasticPrimalRegularizer(mu, lamb)
    sim    = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    params.mu      = mu
    params.lamb    = lamb
    params.alpha   = alpha
    params.beta    = beta
    params.s_degree = 1
    params.u_degree = 0
    params.bc_type = "Neumann"
    params.kernel_dimension = 3
    params.do_time_regularization = False

    RP = RegistrationProblem.MixedElasticRegistration(sim, reg, images, params, mesh)

    solverParameters =  Parameters.solverParameters()
    solverParameters.max_iters = max_iters
    solverParameters.tolerance = tolerance 
    RP.setup()
    sol,_ = RP.solve(solverParameters, printEvery=1)
    sig, u, ub, phi = sol.sub(0), sol.sub(2), sol.sub(3), sol.sub(4)
    
    return sig, u, ub, phi, RP.V.dim()


# Get reference solution
print("------- Compute reference solution")
sigRef, uRef, ubRef, phiRef, dimRef = getSolution(UnitSquareMesh(2**Nreference, 2**Nreference))

meshRef = sigRef.function_space().mesh()
phiRef = project(phiRef, FunctionSpace(meshRef, 'CG', 1))
dofs = []
hs   = []
eus   = []
esigs = []
es0s  = []
ephis = []

for n in range(1, Nreference):
    dxRef = dx(meshRef)
    print("Solving for {} elements per side".format(2**n))
    mesh = UnitSquareMesh(2**n, 2**n)
    sig, u, ub, phi, dof = getSolution(mesh)
    deg_rise = 2

    # Displacement error
    from RegFem.RigidMotionsHandler import RigidMotionBase
    _,_,e2 = RigidMotionBase(2, mesh)
    uuRef = uRef + ubRef * e2
    uu    = u + ub * e2
    erru = sqrt(assemble((uuRef - uu)**2*dxRef))
    eus.append(erru)

    # Stress error
    esigs.append(errornorm(sigRef, sig, "Hdiv", degree_rise=deg_rise))
    es0s.append(errornorm(sigRef, sig, "L2", degree_rise=deg_rise))
     
    # Other variables
    ephis.append(errornorm(phiRef, phi, "L2", degree_rise=deg_rise))
    dofs.append(dof)
    hs.append(mesh.hmax())
    
# Finally calculate errors
rsigs = []
rs0s  = []
rus   = []
rphis = []
Nsteps = len(dofs)
for i in range(1, Nsteps):
    lnhh = ln(hs[i]/hs[i-1])
    rsigs.append(ln(esigs[i]/esigs[i-1])/lnhh)
    rs0s.append(ln(es0s[i]/es0s[i-1])/lnhh)
    rus.append(ln(eus[i]/eus[i-1])/lnhh)
    rphis.append(ln(ephis[i]/ephis[i-1])/lnhh)
    
print("dofs & hh  &  esigL2  &  rsigL2  &  esig  &  rsig  &  eu  &  ru & ephi & rphi  \\")
for i in range(Nsteps):
    if i ==0:
        print(" {} & {:.3e} &  {:.3e}  & -- & {:.3e}  & --  & {:.3e}  &  -- & {:.3e} & -- \\".format(dofs[i], hs[i], es0s[i], esigs[i], eus[i], ephis[i]))
    else:
        print(" {} & {:.3e} &  {:.3e}  & {:.3e}  &  {:.3e}  & {:.3e}  & {:.3e}  &  {:.3e} & {:.3e}  &  {:.3e} \\".format(dofs[i], hs[i], es0s[i], rs0s[i-1], esigs[i], rsigs[i-1], eus[i], rus[i-1], ephis[i], rphis[i-1]))
