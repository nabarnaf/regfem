from PIL.Image import open as PilOpen
import numpy as np
Rimg = np.array(PilOpen("brainR.gif")) / 255
Timg = np.array(PilOpen("brainT.gif")) / 255

Rimg_5p = Rimg + np.random.normal(0, 0.05, Rimg.shape)
Rimg_10p = Rimg + np.random.normal(0, 0.10, Rimg.shape)

Timg_5p = Timg + np.random.normal(0, 0.05, Timg.shape)
Timg_10p = Timg + np.random.normal(0, 0.10, Timg.shape)

from RegFem import Images
from dolfin import *
mesh = UnitSquareMesh(258,258)
print("Creating 5p")
img_5p = Images.NPYImages(mesh, Rimg_5p, Timg_5p, generate_gradients=False)
print("Creating 10p")
img_10p = Images.NPYImages(mesh, Rimg_10p, Timg_10p, generate_gradients=False)

writer = HDF5File(mesh.mpi_comm(), "brain_5p.h5", "w")
writer.write(img_5p.R, "R")
writer.write(img_5p.T.f, "T")


writer = HDF5File(mesh.mpi_comm(), "brain_10p.h5", "w")
writer.write(img_10p.R, "R")
writer.write(img_10p.T.f, "T")

import matplotlib.pyplot as plt
plt.subplot(221)
plot(img_5p.R, mesh=mesh)
plt.subplot(222)
plot(img_10p.R, mesh=mesh)
plt.subplot(223)
plot(img_5p.T.f, mesh=mesh)
plt.subplot(224)
plot(img_10p.T.f, mesh=mesh)
plt.show()