class test : public Expression
{
public:
             
  test() : Expression() { }
    
  void eval(Array<double>& values, const Array<double>& x) const { 
      
    Array<double> dx (2);
    u->eval(dx, x);
    dx[0] +=  x[0] ;
    dx[1] +=  x[1] ;
    // if goes out of Omega, truncate? comment for now
    //if(dx[0]< 0. || dx[0] > 1. || dx[1] < 0. || dx[1] > 1.){
    //  values[0] = 0.;
    //}
    //else{
    f->eval(values, dx);
    //}
                
  }
            
  std::shared_ptr<const Function> u; 
  std::shared_ptr<const Expression> f;
};
