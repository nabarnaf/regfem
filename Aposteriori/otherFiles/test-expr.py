from dolfin import *
parameters["form_compiler"]["cpp_optimize"] = True

mesh = UnitSquareMesh(10,10)

uex = Expression((('sin(x[0])*cos(x[1])','-cos(x[0])*sin(x[1])'),\
                  ('sin(x[0])*cos(x[1])','-cos(x[0])*sin(x[1])')), degree=3)

#u1ex = uex[0]

Ph = VectorFunctionSpace(mesh, 'BDM', 1)

uh = interpolate(uex,Ph)

Qh = FunctionSpace(mesh,'BDM',1)

u1x = uh.sub(0)

u1h = project(u1x,Qh)
