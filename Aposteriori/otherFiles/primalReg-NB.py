from __future__ import print_function
from dolfin import *

mesh = UnitSquareMesh(25, 25)
fileO = XDMFFile("outputs/out-reg.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# Elastic setting
lamb, mu = 1, 1
alpha = Constant(1000)
C   = lambda ten: lamb*tr(ten)*Identity(2)+2*mu*ten
eps = lambda vec: sym(grad(vec))
dt  = 1/(10*alpha)
maxiter = 100

#  ********* Defintion of function spaces  ******* #

P1  = FiniteElement('CG', triangle, 1)
P1v = VectorElement('CG', triangle, 1)
Rv  = VectorElement('R', triangle, 0, dim=3) #2 transl + 1 rot
V   = FunctionSpace(mesh, MixedElement(P1v, Rv, Rv))

sol = Function(V)
u, s_chi1, s_chi2  = TrialFunctions(V)
v, s_xi1, s_xi2 = TestFunctions(V)
u_n = Function(FunctionSpace(mesh,P1v))

nullspace = Constant((1,0)), Constant((0,1)), \
            Expression(("-x[1]+0.5", "x[0]+0.5"), degree = 1)

def extendRM(_vec):
    return _vec[0]*nullspace[0] + _vec[1]*nullspace[1] + _vec[2]*nullspace[2]

chi1 = extendRM(s_chi1)
chi2 = extendRM(s_chi2)
xi1  = extendRM(s_xi1)
xi2  = extendRM(s_xi2)

xu_c = "(x[0]+u[0]-0.5)"
yu_c = "(x[1]+u[1]-0.5)"

#R = Expression("x[0]>0.2 && x[0]<0.6 && x[1]>0.4 && x[1]<0.8?1:0", degree = 1)
R   = Expression("sin(2*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1])", degree = 3)
R_h = interpolate(R,FunctionSpace(mesh,P1))

#T = Expression("{0}*{0} + {1}*{1}<0.44*0.44 ? 1:0".format(xu_c, yu_c),degree = 1, domain=mesh, u = u_n)

T = Expression("cos(2*DOLFIN_PI*{0})*sin(2*DOLFIN_PI*{1})".format(xu_c, yu_c),degree = 2, domain=mesh, u = u_n)



a = dot(u,v)/dt*dx + 1/dt*inner(grad(u), grad(v))*dx \
    + inner(C(eps(u)),eps(v))*dx \
    + dot(chi2,xi1)*dx + dot(v-xi1,chi1)*dx + dot(u-chi2,xi2)*dx

f = dot(u_n,v)/dt*dx + 1/dt*inner(grad(u_n), grad(v))*dx \
    -alpha*(T-R)*dot(grad(T),v)*dx

for i in range(maxiter):
    solve(a == f, sol)
    error = errornorm(sol.sub(0), u_n)
    assign(u_n, sol.sub(0))
    if i%5==0:
        print("iter {}, error={}".format(i, error))
        sol.sub(0).rename("u","u"); fileO.write(sol.sub(0),i*1.0)
        R_h.rename("R","R"); fileO.write(R_h,i*1.0)
        T_h = interpolate(T,FunctionSpace(mesh,P1))
        T_h.rename("T","T"); fileO.write(T_h,i*1.0)
    
    if error < 1e-4:
        break
