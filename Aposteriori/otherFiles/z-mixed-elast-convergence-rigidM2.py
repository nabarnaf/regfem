from dolfin import *
import sympy2fenics as sf

parameters["allow_extrapolation"]= True
parameters["form_compiler"]["cpp_optimize"] = True
list_linear_solver_methods()
fileO = XDMFFile("outputs/mixed-elast-RM.xdmf")
fileO.parameters["functions_share_mesh"] = True


# TEST with different imposition of rigid motions - DOES NOT WORK?


# ********* Model coefficients and parameters ********* #

E     = Constant(1.0e3)
nu    = Constant(0.4)
lmbda = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu    = Constant(E/(2.*(1.+nu)))
alpha = Constant(100.0)
dt    = 1/alpha**2

Cinv  = lambda ten: 0.5/mu*ten - lmbda/(4*mu*(lmbda+mu))*tr(ten)*Identity(2)

# ******* Exact solutions and forcing terms for error analysis ****** #
Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
uu = sf.str2sympy('(0.1*cos(pi*x)*sin(pi*y)+0.5*(x*(1-x)*y*(1-y))**2/lmbda, -0.1*sin(pi*x)*cos(pi*y)+0.5*(x*(1-x)*y*(1-y))**3/lmbda)')

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('sin(2*pi*x)*sin(2*pi*y)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)

ff = alpha * ref * sf.grad(ref).transpose()
f_code = sf.sympy2exp(ff)
f_ex = Expression(f_code, degree=6, cell=triangle)

rhorho   = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex  =  Expression(rho_code, degree=6, cell=triangle, lmbda=lmbda)

sigmasigma   = 2.0*mu*sf.epsilon(uu) + lmbda*sf.div(uu)*Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=6, cell=triangle,lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=5, cell=triangle, lmbda=lmbda)

rhselas = -divsig - ff
rhs_code = sf.sympy2exp(rhselas)
rhs_elast  = Expression(rhs_code, degree=6, cell=triangle, lmbda=lmbda)


hh = []; nn = []; eu = []; ru = []; er = []; rr = []; es = []; rs = [];
rs.append(0.0); ru.append(0.0); rr.append(0.0); 

k=0; nkmax = 5;

for nk in range(nkmax):
    print( "......................Refinement level : nk = ", nk)
    nps = pow(2,nk+1)
    mesh = UnitSquareMesh(nps,nps)
    n = FacetNormal(mesh); hh.append(mesh.hmax())

    # ********* Finite dimensional spaces ********* #
    vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), k+1)
    vectorP0  = VectorElement("DG", mesh.ufl_cell(), k)
    P0        = FiniteElement("DG", mesh.ufl_cell(), k)
    RM        = VectorElement('R', triangle, 0, dim=3)
    
    Hh = FunctionSpace(mesh, MixedElement([vectorBDM,vectorBDM,RM,RM,vectorP0,P0]))

    tau1,tau2,  s_xi1,  s_xi2, v, eta12 = TestFunctions(Hh)
    sig1,sig2, s_chi1, s_chi2, u, rho12 = TrialFunctions(Hh)

    sigma = as_tensor((sig1,sig2))
    tau = as_tensor((tau1,tau2))
    
    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

    def extendRM(_v):
        return _v[0]*nullspace[0] + _v[1]*nullspace[1] + _v[2]*nullspace[2]

    xi1 = extendRM(s_xi1); xi2 = extendRM(s_xi2)
    chi1 = extendRM(s_chi1); chi2 = extendRM(s_chi2)
    
    rho = as_tensor(((0,rho12),(-rho12,0)))
    eta = as_tensor(((0,eta12),(-eta12,0)))

    nn.append(Hh.dim())


    # ******** Pure traction BCs are essential for mixed ************* #

    bcs1 = DirichletBC(Hh.sub(0), [0,0], 'on_boundary')
    bcs2 = DirichletBC(Hh.sub(1), [0,0], 'on_boundary')

    bcS = [bcs1,bcs2]
    
    # ******** Weak forms ************* #

    a  = inner(Cinv(sigma),tau)*dx
    bt = dot(u,div(tau))*dx + inner(rho,tau) *dx
    b  = dot(v,div(sigma))*dx + inner(eta,sigma) *dx

    lhsT = a + bt + b

    lhsT -= dot(chi2,xi2)*dx + dot(v-xi2,chi1)*dx + dot(u-chi2,xi1)*dx 
    
    rhsT = -dot(f_ex +rhs_elast,v)*dx 

    Sol_h = Function(Hh)
    solve(lhsT == rhsT, Sol_h, bcS)
    sig1_h,sig2_h,chi1_h,chi2_h,u_h,rho12_h = Sol_h.split(True)

    sigma_h = as_tensor((sig1_h,sig2_h))
    rho_h = as_tensor(((0,rho12_h),(-rho12_h,0)))

    # Exporting solutions
    u_h.rename("u","u"); fileO.write(u_h,nk*1.0)
    sig1_h.rename("sig1","sig1"); fileO.write(sig1_h,nk*1.0)
    sig2_h.rename("sig2","sig2"); fileO.write(sig2_h,nk*1.0)
    rho12_h.rename("rho12","rho12"); fileO.write(rho12_h,nk*1.0)
    
    # Computing errors. sigmaex and rhoex are not "expressions"
    E_s = assemble(inner(sigma_ex-sigma_h,sigma_ex-sigma_h)*dx \
                   + dot(div(sigma_h)-divsigma_ex,div(sigma_h)-divsigma_ex)*dx)
    E_rho = assemble(inner(rho_h-rho_ex,rho_h-rho_ex)*dx)
    
    es.append(pow(E_s,0.5))
    eu.append(errornorm(u_ex,u_h,'L2'))
    er.append(pow(E_rho,0.5))
   
    if(nk>0):
        ru.append(ln(eu[nk]/eu[nk-1])/ln(hh[nk]/hh[nk-1]))
        rs.append(ln(es[nk]/es[nk-1])/ln(hh[nk]/hh[nk-1]))
        rr.append(ln(er[nk]/er[nk-1])/ln(hh[nk]/hh[nk-1]))

# Generating table of convergences
print('nn   &  hh  &   e(s)  &   r(s)  &  e(u)  &   r(u)  &  e(r)  &  r(r)')
print('====================================================================')
for nk in range(nkmax):
    print('%d & %4.4g & %g & %4.4g & %3.3e & %4.4g & %3.3e & %4.4g' % (nn[nk], hh[nk], es[nk], rs[nk], eu[nk], ru[nk], er[nk], rr[nk]))
