from __future__ import print_function
from dolfin import *
import sympy2fenics as sf

parameters["form_compiler"]["cpp_optimize"] = True
parameters["allow_extrapolation"]= True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"
fileO = XDMFFile("outputs/mixed-aposte-RM.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# ******* Mechanical parameters ******* #

E     = Constant(1.0e3)
nu    = Constant(0.4)
lmbda = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu    = Constant(E/(2.*(1.+nu)))
alpha = Constant(100.0)
dt    = 1/alpha**2

Cinv  = lambda ten: 0.5/mu*ten - lmbda/(4*mu*(lmbda+mu))*tr(ten)*Identity(2)
Tcurl = lambda ten: as_vector((Dx(ten[0,1],0)-Dx(ten[0,0],1),Dx(ten[1,1],0)-Dx(ten[1,0],1)))

print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #

Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
uu = sf.str2sympy('(0.1*cos(pi*x)*sin(pi*y)+0.5*(x*(1-x)*y*(1-y))**2/lmbda, -0.1*sin(pi*x)*cos(pi*y)+0.5*(x*(1-x)*y*(1-y))**3/lmbda)')

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)
gradu = sf.grad(uu)
gradu_code = sf.sympy2exp(gradu)
gradu_ex = Expression(gradu_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('sin(2*pi*x)*sin(2*pi*y)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)

targ0 = sf.str2sympy('sin(2*pi*x)*sin(2*pi*(y+0.01))')
targ0_code = sf.sympy2exp(targ0)
T0 = Expression(targ0_code, degree=6, cell=triangle)

gradtarg0 = sf.grad(targ0)
gradtarg0_code = sf.sympy2exp(gradtarg0)
gradT0 = Expression(gradtarg0_code, degree=6, cell=triangle)

ff = alpha * (targ0-ref) * sf.grad(targ0).transpose()

rhorho   = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex  =  Expression(rho_code, degree=6, cell=triangle, lmbda=lmbda)

sigmasigma   = 2.0*mu*sf.epsilon(uu) + lmbda*sf.div(uu)*Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=6, cell=triangle,lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=5, cell=triangle, lmbda=lmbda)

load = 1/dt*uu - divsig + ff
load_code = sf.sympy2exp(load)
load_ex  = Expression(load_code, degree=6, cell=triangle, lmbda=lmbda)


# ********* Mesh refinement loop ********* #

hh = []; nn = []; eu = []; ru = []; er = []; rr = []; es = []; rs = []
eff = []
rs.append(0.0); ru.append(0.0); rr.append(0.0)

nkmax = 8

mesh = UnitSquareMesh(2, 2)
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)

for nk in range(nkmax):

    print( "......................Refinement level : nk = ", nk)
    n = FacetNormal(mesh); hh.append(mesh.hmin())
    t = as_vector((-n[1],n[0]))

    # ********* Finite dimensional spaces ********* #
    
    vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), 1)
    vectorP0  = VectorElement("DG", mesh.ufl_cell(), 0)
    P0        = FiniteElement("DG", mesh.ufl_cell(), 0)
    RM        = VectorElement('R', triangle, 0, dim=3)
    
    # Mixed space                            sigma1  sigma2    rm    u    rho
    Hh = FunctionSpace(mesh, MixedElement([vectorBDM,vectorBDM,RM,vectorP0,P0]))
    
    nn.append(Hh.dim())
    print("h_min = {}, dim(H_h) = {}".format(hh[nk], nn[nk]))
    
    tau1,tau2, s_xi,  v, eta12 = TestFunctions(Hh)
    sig1,sig2, s_chi, u, rho12 = TrialFunctions(Hh)

    sigma = as_tensor((sig1,sig2))
    tau = as_tensor((tau1,tau2))

    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

    rho = as_tensor(((0,rho12),(-rho12,0)))
    eta = as_tensor(((0,eta12),(-eta12,0)))

    # ******** Pure traction BCs are essential for mixed ************* #

    bcs1 = DirichletBC(Hh.sub(0), [0,0], 'on_boundary')
    bcs2 = DirichletBC(Hh.sub(1), [0,0], 'on_boundary')
    bcS = [bcs1,bcs2]
    
    # ******** Reference image on RHS ************* #

    R_h = interpolate(R,FunctionSpace(mesh,'CG',1))
    T_h = interpolate(T0,FunctionSpace(mesh,'CG',1))

    # ******** Weak form of the problem ************* #

    a  = inner(Cinv(sigma),tau)*dx
    bt = dot(u,div(tau))*dx + inner(rho,tau) *dx
    b  = dot(v,div(sigma))*dx + inner(eta,sigma) *dx

    calA = -1.0/dt*dot(u,v)*dx + a + bt + b + dot(s_chi,s_xi)*dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi  = s_xi[i]
        calA -= chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx
        
    calF = alpha*(T_h-R_h)*dot(grad(T_h),v)*dx - dot(load_ex,v)*dx
    
    Sol_h  = Function(Hh)
    solve(calA == calF, Sol_h, bcS)
    sig1_h, sig2_h, chi_h, u_h, rho12_h = Sol_h.split(True)
    
    sigma_h = as_tensor((sig1_h,sig2_h))
    rho_h = as_tensor(((0,rho12_h),(-rho12_h,0)))
    
    # Exporting solutions
    u_h.rename("u","u"); fileO.write(u_h,nk*1.0)
    sig1_h.rename("sig1","sig1"); fileO.write(sig1_h,nk*1.0)
    sig2_h.rename("sig2","sig2"); fileO.write(sig2_h,nk*1.0)
    rho12_h.rename("rho12","rho12"); fileO.write(rho12_h,nk*1.0)

    # Computing errors. sigmaex and rhoex are not "expressions"
    E_s = assemble(inner(sigma_ex-sigma_h,sigma_ex-sigma_h)*dx \
                   + dot(div(sigma_h)-divsigma_ex,div(sigma_h)-divsigma_ex)*dx)
    E_rho = assemble(inner(rho_h-rho_ex,rho_h-rho_ex)*dx)
    
    es.append(pow(E_s,0.5))
    eu.append(errornorm(u_ex,u_h,'L2'))
    er.append(pow(E_rho,0.5))

    if(nk>0):
        ru.append(ln(eu[nk]/eu[nk-1])/(-0.5*ln(float(nn[nk])/nn[nk-1])))
        rs.append(ln(es[nk]/es[nk-1])/(-0.5*ln(float(nn[nk])/nn[nk-1])))
        rr.append(ln(er[nk]/er[nk-1])/(-0.5*ln(float(nn[nk])/nn[nk-1])))

    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, 'DG', 0)
    w  = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)

    PsiK = (1./dt*u_h + alpha*(T_h-R_h)*grad(T_h) \
            - div(sigma_h) - load_ex)**2*w*dx \
            + (sigma_h - sigma_h.T)**2*w*dx  \
            + chi_h**2*w*dx \
            + hK**2*(Tcurl(Cinv(sigma_h)+rho_h))**2*w*dx \
            + hK**2*(Cinv(sigma_h)+rho_h)**2*w*dx \
            + avg(he)*(jump((Cinv(sigma_h)+rho_h)*t))**2*avg(w)*dS \
            + he*((Cinv(sigma_h)+rho_h)*t)**2*w*ds
    
    globalPsi=assemble(PsiK)
    globalPsi=globalPsi.get_local()
    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))

    eff.append((pow(E_s,0.5)+errornorm(u_ex,u_h,'L2')+pow(E_rho,0.5))/Psi)

    # ********* Mesh adaptivity ******** #
    tolAdapt = 1.0E-5; ref_ratio = 0.4

    if (Psi < tolAdapt and nk==nkmax):
        print("\nEstimated error < tolerance: %s < %s" % (Psi,tolAdapt))
        break
    
    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    cell_markers.set_all(True)
    for c in cells (mesh):
        cell_markers[c] = globalPsi[c.index()] > (ref_ratio * globalPsi_max)

    adapt(mesh, cell_markers)
    
    mesh.smooth(50); mesh.smooth_boundary(50)
    mesh = mesh.child()
    
    adapt(bdry, mesh); bdry = bdry.child()

# ******* Convergence history ********** *

print(' DoF  &   h    &   e(s)  &   r(s)  &  e(u)  &   r(u)  &  e(r)  &  r(r)  &  eff ')
print('============================================================================================')
for i in range(nkmax):
    print('%d & %4.4g & %3.3g & %4.4g & %3.3e & %4.4g & %3.3e &  %4.4g & %4.4g ' % (nn[i], hh[i], es[i], rs[i], eu[i], ru[i], er[i], rr[i], eff[i]))
