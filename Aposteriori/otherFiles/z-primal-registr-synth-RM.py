from __future__ import print_function
from dolfin import *

mesh = UnitSquareMesh(32, 32)
fileO = XDMFFile("outputs/z-primal-test.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

xpr_file = open( "composition.cpp" , 'r' )  
displ_compos = xpr_file.read()

# ******* Mechanical parameters ******* #

E     = Constant(5.0)
nu    = Constant(0.33)
lmbda = E*nu/((1.0+nu)*(1.0-2.0*nu))
mu    = E/(2.0*(1.0+nu))
alpha = Constant(1000.0)
dt    = 0.1/alpha

#lmbda,mu=1,1
#alpha = Constant(1000); dt = 1/(10*alpha)

C     = lambda ten: lmbda*tr(ten)*Identity(2)+2*mu*ten
eps   = lambda vec: sym(grad(vec))

print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

#  ********* Defintion of function spaces  ******* #

P1  = FiniteElement('CG', triangle, 1)
P1v = VectorElement('CG', triangle, 1)
Rv  = VectorElement('R', triangle, 0, dim=3)
V   = FunctionSpace(mesh, MixedElement(P1v, Rv))

sol = Function(V)
u, s_chi = TrialFunctions(V)
v, s_xi  = TestFunctions(V)
u_n = Function(FunctionSpace(mesh,P1v))

#2 translations plus one rotation (on the domain center)
nullspace=[Constant((1,0)),Constant((0,1)),\
           Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

#R  = Expression("sin(2*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1])",degree=3)
R   = Expression("x[0]>0.2 && x[0]<0.6 && x[1]>0.4 && x[1]<0.8?1:0", degree = 2)
R_h = interpolate(R,FunctionSpace(mesh,P1))

# For error computation, we compute T from R
#T0 = Expression('cos(2*DOLFIN_PI*(x[0]))*sin(2*DOLFIN_PI*(x[1]))', degree=3)
T0 = Expression("(x[0]-0.5)*(x[0]-0.5)+(x[1]-0.5)*(x[1]-0.5)<0.44*0.44 ? 1:0", degree=2)
T = Expression(displ_compos, domain=mesh,degree=2)
T.f = T0

a = inner(C(eps(u)),eps(v))*dx + dot(u,v)/dt*dx + 1/dt*inner(grad(u), grad(v))*dx + dot(s_chi,s_xi)*dx

for i, ns_i in enumerate(nullspace):
    chi = s_chi[i]
    xi  = s_xi[i]
    a  -= chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx 

f = -alpha*(T-R)*dot(grad(T),v)*dx + dot(u_n,v)/dt*dx + 1/dt*inner(grad(u_n), grad(v))*dx


# ******** Picard iterations **** #
res = 1.0; iter = 0; tol = 1.0E-4; maxiter = 100

while res > tol and iter < maxiter:
    T.u=u_n
    solve(a == f, sol, \
          solver_parameters={'linear_solver':'petsc'})
    u_h,chi_h=sol.split(True)
    res = pow(assemble(dot(u_h-u_n,u_h-u_n)*dx),0.5)
    assign(u_n, u_h)
    
    if iter%5==0:
        print("iter {}, error={}".format(iter, res))
        u_h.rename("u","u"); fileO.write(u_h,iter*1.0)
        R_h.rename("R","R"); fileO.write(R_h,iter*1.0)
        T_h = interpolate(T,FunctionSpace(mesh,P1))
        T_h.rename("T","T"); fileO.write(T_h,iter*1.0)


    iter += 1
