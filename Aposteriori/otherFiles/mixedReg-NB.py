from __future__ import print_function
from dolfin import *

mesh = UnitSquareMesh(25,25)
fileO = XDMFFile("outputs/mixed-initial.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

xpr_file = open( "composition.cpp" , 'r' )  
displ_compos = xpr_file.read()

E     = Constant(5.0)
nu    = Constant(0.33)
lamb  = E*nu/((1.0+nu)*(1.0-2.0*nu))
mu    = E/(2.0*(1.0+nu))
alpha = Constant(200.0)
Cinv  = lambda ten: 1./2*mu*ten - lamb/(4*mu*(mu+lamb))*tr(ten)*Identity(2)
dt    = 1/alpha**2
maxiter = 100
tol = 1e-4

rm = Constant((1,0)), Constant((0,1)), Expression(("-x[1]+0.5","x[0]+0.5"), degree = 1)
def extendRM(_vec):
    return _vec[0]*rm[0] + _vec[1]*rm[1] + _vec[2]*rm[2]

class Sides(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and (near(x[0],0.0) or near(x[0], 1.0))
    
class Covers(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and (near(x[1],0.0) or near(x[1], 1.0))
Sides = Sides(); Covers = Covers()
# Functional setting
s_el = VectorElement('BDM', triangle, 1) # H_div
skew_el = FiniteElement('DG', triangle, 0) # Skew tensor
u_el  = VectorElement('DG', triangle, 0) # Displacement
rm_el = VectorElement('R', triangle, 0, 3) # Rigid motions

V = FunctionSpace(mesh, MixedElement(s_el, skew_el, u_el, rm_el, rm_el))
bc_side = DirichletBC(V.sub(0).sub(0), Constant([0.,0.]), Sides)
bc_cover = DirichletBC(V.sub(0).sub(1), Constant([0.,0.]), Covers)
bcs = [bc_side, bc_cover]

# Function placeholders
# NAMES OF VARIABLES
# Hdiv, skew, displacement, rm_lagrange, rm_extended
# sigma, zeta, u, rho, lambda
# tau, eta, v, xi, phi
sigma, _zeta, u, s_rho, s_l  = TrialFunctions(V)
tau, _eta, v, s_xi, s_phi = TestFunctions(V)

#sigma = _sigma.T # transposed for boundary conditions
#tau = _tau.T
zeta = as_matrix([[0, _zeta], [-_zeta, 0]])
eta = as_matrix([[0, _eta], [-_eta, 0]])

# Now we extend RM modes for comfortability
rho = extendRM(s_rho)
l   = extendRM(s_l)
xi = extendRM(s_xi)
phi  = extendRM(s_phi)

# Bilinear forms and problem

dx = dx(mesh)
A = lambda (s, l), (t, phi): (inner(Cinv(s), t) + dot(l, phi))*dx
B = lambda (tau, xi), (eta, v, phi): -(dot(div(tau), v)
                - inner(tau, eta)
                + dot(v - phi, xi))*dx
AA = A((sigma, l), (tau, phi))
BT = B((tau, phi), (zeta, u, rho))
BB  = B((sigma, l), (eta, v, xi))

a =  AA + BT + BB
dx = dx(mesh)
a = inner(Cinv(sigma), tau)*dx + inner(zeta, tau)*dx + dot(div(tau), u)*dx \
    - dot(div(sigma), v)*dx + dot(l, phi)*dx + dot(v - phi, rho)*dx \
    + dot(u-l, xi)*dx + inner(sigma, eta)*dx


R = Expression("exp(-a*(pow(x[0]-0.4,2)+pow(x[1]-0.4,2)))", \
               degree = 4, a = 20)

R_h = interpolate(R,FunctionSpace(mesh,skew_el))
u_n = Function(FunctionSpace(mesh,u_el))
T0 =  Expression("exp(-a*(pow(x[0]-0.5,2)+pow(x[1]-0.5,2)))", \
                degree = 4, a = 20)

T = Expression(displ_compos, domain=mesh,degree=2)
T.f = T0

f = -alpha*(T- R)*dot(grad(T), v)*dx

# Time stab
a = dt*a + dot(u, v)*dx
f = dt*f + dot(u_n, v)*dx
sol = Function(V)

for i in range(maxiter):
    T.u=u_n
    solve(a == f, sol, bcs)
    error = errornorm(sol.sub(2), u_n)
    assign(u_n, sol.sub(2))
    if i%1==0:
        print("iter {}, error={}".format(i, error))
        u_n.rename("u","u"); fileO.write(u_n,i*1.0)
        R_h.rename("R","R"); fileO.write(R_h,i*1.0)
        T_h = interpolate(T,FunctionSpace(mesh,skew_el))
        T_h.rename("T","T"); fileO.write(T_h,i*1.0)

    if error < tol:
        break

