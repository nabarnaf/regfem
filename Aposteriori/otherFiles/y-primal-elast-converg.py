from __future__ import print_function
from dolfin import *
import sympy2fenics as sf

parameters["form_compiler"]["cpp_optimize"] = True
fileO = XDMFFile("outputs/primal-elast-converg.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# ********* Model coefficients and parameters ********* #

E     = Constant(1.0e3)
nu    = Constant(0.4)
lmbda = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu    = Constant(E/(2.*(1.+nu)))
alpha = Constant(100.0)
dt    = 1/alpha**2

C     = lambda ten: lmbda*tr(ten)*Identity(2)+2*mu*ten
eps   = lambda vec: sym(grad(vec))

print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #

Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
uu = sf.str2sympy('(0.1*cos(pi*x)*sin(pi*y)+0.5*(x*(1-x)*y*(1-y))**2/lmbda, -0.1*sin(pi*x)*cos(pi*y)+0.5*(x*(1-x)*y*(1-y))**3/lmbda)')

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)
gradu = sf.grad(uu)
gradu_code = sf.sympy2exp(gradu)
gradu_ex = Expression(gradu_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('sin(2*pi*x)*sin(2*pi*y)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)
targ = sf.str2sympy('sin(2*pi*x)*sin(2*pi*(y+0.01))')
targ_code = sf.sympy2exp(targ)
T0 = Expression(targ_code, degree=6, cell=triangle)
gradtarg = sf.grad(targ)
gradtarg_code = sf.sympy2exp(gradtarg)
gradT0 = Expression(gradtarg_code, degree=6, cell=triangle)

rhorho   = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex  =  Expression(rho_code, degree=6, cell=triangle, lmbda=lmbda)

sigmasigma   = 2.0*mu*sf.epsilon(uu) + lmbda*sf.div(uu)*Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=6, cell=triangle,lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=5, cell=triangle, lmbda=lmbda)

load = 1/dt*uu - divsig
load_code = sf.sympy2exp(load)
load_ex  = Expression(load_code, degree=6, cell=triangle, lmbda=lmbda)


# ********* Mesh refinement loop ********* #

hh = []; nn = []; eu = []; ru = []; ru.append(0.0); 

nkmax = 5

for nk in range(nkmax):
    print( "......................Refinement level : nk = ", nk)
    nps = pow(2,nk+1)
    mesh = UnitSquareMesh(nps,nps)
    hh.append(mesh.hmax())

    # ********* Finite dimensional spaces ********* #
    
    vectorP1 = VectorElement("CG", mesh.ufl_cell(), 1)
    RM       = VectorElement('R', triangle, 0, dim=3)
    
    # Mixed space 
    Hh = FunctionSpace(mesh, MixedElement([vectorP1,RM]))

    v, s_xi = TestFunctions(Hh)
    u, s_chi = TrialFunctions(Hh)

    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

    nn.append(Hh.dim())

    # ******** Pure traction BCs are natural for primal ************* #

    # nothing to do

    # ******** Reference and target images on RHS ************* #

    R_h = interpolate(R,FunctionSpace(mesh,'CG',1))
    T_h = interpolate(T0,FunctionSpace(mesh,'CG',1))
    
    # ******** Weak forms ************* #

    a  = inner(C(eps(u)),eps(v))*dx

    calA = 1.0/dt*dot(u,v)*dx + a + dot(s_chi,s_xi)*dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi  = s_xi[i]
        calA += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx

    calF = -alpha*(T_h-R_h)*dot(grad(T_h),v)*dx + dot(load_ex,v)*dx \
           +alpha*(T0-R)*dot(gradT0,v)*dx

    Sol_h = Function(Hh)
    solve(calA == calF, Sol_h)
    u_h,chi_h = Sol_h.split(True)

    # Exporting solutions
    u_h.rename("u","u"); fileO.write(u_h,nk*1.0)
    
    # Computing errors.
    eu.append(errornorm(u_ex,u_h,'H1'))
   
    if(nk>0):
        ru.append(ln(eu[nk]/eu[nk-1])/ln(hh[nk]/hh[nk-1]))
    
# ******* Convergence history ********** *
print('DoF   &  hh  &   e(u)  &   r(u)  ')
print('===========================================')
for nk in range(nkmax):
    print('%d & %4.4g & %3.3e & %4.4g' % (nn[nk], hh[nk], eu[nk], ru[nk]))
