from __future__ import print_function
from dolfin import *

fileO = XDMFFile("outputs/z-mixed.xdmf")
fileO.parameters["functions_share_mesh"] = True

mesh = UnitSquareMesh(25,25)

# ******* Mechanical parameters ******* #

E     = Constant(100.0)
nu    = Constant(0.33)
lmbda = E*nu/((1.0+nu)*(1.0-2.0*nu))
mu    = E/(2.0*(1.0+nu))
alpha = Constant(100.0)
dt    = 1/alpha**2

Cinv  = lambda ten: 0.5/mu*ten - lmbda/(4*mu*(lmbda+mu))*tr(ten)*Identity(2)

print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

#  ********* Defintion of function spaces  ******* #

P0   = FiniteElement('DG', triangle, 0)
P1   = FiniteElement('CG', triangle, 1)
BDMt = VectorElement('BDM', triangle, 1)
P0v  = VectorElement('DG', triangle, 0)

# Rigid motions: in 2D, only 2 translations and 1 rotation
Rv   = VectorElement('R', triangle, 0, dim=3)

#                                     stress, rm,   u, rho
Hh   = FunctionSpace(mesh, MixedElement(BDMt, Rv, P0v, P0))
sol  = Function(Hh)

sigma, s_chi, u, rho12 = TrialFunctions(Hh)
tau  ,  s_xi, v, eta12 = TestFunctions(Hh)

nullspace=[Constant((1,0)), Constant((0,1)),\
           Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

rho = as_tensor(((0,rho12),(-rho12,0)))

eta = as_tensor(((0,eta12),(-eta12,0)))

# ******** Pure traction BCs are essential for mixed ************* #

bcS = DirichletBC(Hh.sub(0), [[0,0],[0,0]], 'on_boundary')

# ******** Reference image on RHS ************* #

R = Expression("sin(2*DOLFIN_PI*x[0])*sin(2*DOLFIN_PI*x[1])", degree = 3)
R_h = interpolate(R,FunctionSpace(mesh,P1))
u_n = Function(FunctionSpace(mesh,P0v))

# ******** Weak form of the problem ************* #

calA = -1.0/dt*dot(u,v)*dx \
       + inner(Cinv(sigma),tau)*dx \
       + dot(u,div(tau))*dx + inner(rho,tau)*dx \
       + dot(v,div(sigma))*dx + inner(eta,sigma) *dx

for i, ns_i in enumerate(nullspace):
    chi = s_chi[i]
    xi  = s_xi[i]
    calA += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx
   
calF = -alpha*R_h*dot(grad(R_h), v)*dx \
       -1.0/dt*dot(u_n,v)*dx

LHS = assemble(calA)
solver = LUSolver(LHS)
RHS = None

# ******** Time iterations **** #
res = 1.0; inc = 0; tol = 1.0E-5; maxiter = 100

while res > tol and inc < maxiter:
    RHS = assemble(calF, tensor=RHS)
    bcS.apply(LHS, RHS)
    solver.solve(sol.vector(), RHS)
    
    sigma_h, chi_h, u_h, rho12_h = sol.split(True)
    res = pow(assemble(dot(u_h-u_n,u_h-u_n)*dx),0.5)
    
    assign(u_n, u_h)
    
    if inc%1==0:
        print("iter {}, res = {}".format(inc, res))
        u_h.rename("u","u"); fileO.write(u_h,inc*1.0)
        R_h.rename("R","R"); fileO.write(R_h,inc*1.0)

    inc += 1
