from dolfin import *
import numpy

parameters["form_compiler"]["cpp_optimize"] = True

degree = 1

# Exact solution
ue = Expression('(1.0/pi) * atan2(x[1], x[0])',degree=degree+3)

# Initial mesh
n = 10
mesh = RectangleMesh(Point(-0.5, 0), Point(+0.5, 1.0), n, n)

# Number of refinement steps
nstep= 5

# Fraction of cells to refine
REFINE_FRACTION=0.1

# Refinement type: 'uniform' or 'adaptive'
refine_type = 'adaptive'



fileO = XDMFFile("outputs/test_adapt.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

conv = []

for j in range(nstep):
    V = FunctionSpace(mesh, 'CG', degree)

    u = TrialFunction(V)
    v = TestFunction(V)

    # Bilinear form
    A = inner(grad(u), grad(v))*dx

    # Linear functional
    f = Constant(0.0)
    L = f*v*dx

    # Dirichlet bc
    bc = DirichletBC(V, ue, 'on_boundary')

    # Solution variable
    u = Function(V)
    solve(A == L, u, bc)
    u.rename("u","u"); fileO.write(u,j*1.0)

    # Compute error in solution
    err = interpolate(ue, V); err.rename("Error","Error")
    err.vector()[:] -= u.vector().array()
    err.rename("err","err"); fileO.write(err,j*1.0)
   
    error_L2 = errornorm(ue, u, norm_type='L2', degree_rise=3)
    error_H1 = errornorm(ue, u, norm_type='H10', degree_rise=3)
    conv.append([V.dim(), mesh.hmax(), error_L2, error_H1])

    n = FacetNormal(mesh)
    dudn = dot( grad(u), n)
    Z = FunctionSpace(mesh, 'DG', 0)
    z = TestFunction(Z)
    ETA = assemble(2*avg(z)*jump(dudn)**2*dS)
    eta = numpy.array([0.5*numpy.sqrt(c.h()*ETA[c.index()]) \
                       for c in cells(mesh)])
    gamma = sorted(eta, reverse=True)[int(len(eta)*REFINE_FRACTION)]
    flag = MeshFunction('bool', mesh, mesh.topology().dim())
    for c in cells(mesh):
        flag[c] = eta[c.index()] > gamma

    # refine the mesh
    if j < nstep-1:
        if refine_type == 'adaptive':
            mesh_new = adapt(mesh, flag)
        else:
            mesh_new = adapt(mesh)

    #V = adapt(V, mesh_new)
    #u = adapt(u, mesh_new)
    #A = adapt(Form(A), mesh_new)
    #L = adapt(Form(L), mesh_new)
    #bc= adapt(bc, mesh_new, V)
    mesh = mesh_new


for j in range(nstep):
   fmt='{0:6d} {1:14.6e} {2:14.6e} {3:14.6e}'
   print fmt.format(conv[j][0], conv[j][1], conv[j][2], conv[j][3])   
