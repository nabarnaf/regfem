from __future__ import print_function
from dolfin import *
import sympy2fenics as sf

parameters["form_compiler"]["cpp_optimize"] = True
parameters["allow_extrapolation"]= True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"
fileO = XDMFFile("outputs/primal-reg-aposteriori-sing.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

xpr_file = open( "composition.cpp" , 'r' )  
displ_compos = xpr_file.read()

# ********* Model coefficients and parameters ********* #

E     = Constant(1.0e3)
nu    = Constant(0.4)
lmbda = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu    = Constant(E/(2.*(1.+nu)))
alpha = Constant(100.0)
dt    = 1/alpha**2

C     = lambda ten: lmbda*tr(ten)*Identity(2)+2*mu*ten
eps   = lambda vec: sym(grad(vec))
print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #
Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
uu = sf.str2sympy('(0.1*cos(2*pi*x)*sin(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**2, -0.1*sin(2*pi*x)*cos(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**3)')

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)
gradu = sf.grad(uu)
gradu_code = sf.sympy2exp(gradu)
gradu_ex = Expression(gradu_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('x*y*(x-1)*(y-1)/((x+0.02)**4+(y+0.02)**4)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)

targ0 = sf.str2sympy('exp(-50*((x-0.2)**2+(y-0.2)**2))')
targ0_code = sf.sympy2exp(targ0)
T0 = Expression(targ0_code, degree=6, cell=triangle)

rhorho   = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex  =  Expression(rho_code, degree=6, cell=triangle, lmbda=lmbda)

sigmasigma   = 2.0*mu*sf.epsilon(uu) + lmbda*sf.div(uu)*Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=6, cell=triangle,lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=5, cell=triangle, lmbda=lmbda)

load = 1/dt*uu - divsig
load_code = sf.sympy2exp(load)
load_ex  = Expression(load_code, degree=6, cell=triangle, lmbda=lmbda)

# ********* Mesh refinement loop ********* #

hh = []; nn = []; eu = []; ru = [];
it =[]; eff = []; ru.append(0.0); 

nkmax = 6
mesh = UnitSquareMesh(2, 2)
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)

for nk in range(nkmax):
    print( "......................Refinement level : nk = ", nk)
    n = FacetNormal(mesh); hh.append(mesh.hmin())
    t = as_vector((-n[1],n[0]))

    # ********* Finite dimensional spaces ********* #
    
    vectorP1 = VectorElement("CG", mesh.ufl_cell(), 2)
    RM        = VectorElement('R', triangle, 0, dim=3)
    
    # Mixed space 
    Hh = FunctionSpace(mesh, MixedElement([vectorP1,RM]))
    nn.append(Hh.dim())
    print("h_min = {}, dim(H_h) = {}".format(hh[nk], nn[nk]))

    v, s_xi = TestFunctions(Hh)
    u, s_chi = TrialFunctions(Hh)

    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]


    # ******** Pure traction BCs are natural for primal ************* #

    # nothing to do

    # ******** Reference and target images ************* #

    R_h = interpolate(R,FunctionSpace(mesh,'CG',1))
    T = Expression(displ_compos, domain=mesh, degree=2)
    T.f = T0
    
    u_n = Function(FunctionSpace(mesh,vectorP1))

    # ******** Exact target for error computation ******* #
    
    u_ex_h = interpolate(u_ex,FunctionSpace(mesh,vectorP1))
    T_ex = Expression(displ_compos,domain=mesh, degree=2)
    T_ex.f = T0; T_ex.u = u_ex_h
        
    # ******** Weak forms ************* #

    a  = inner(C(eps(u)),eps(v))*dx

    calA = 1.0/dt*dot(u,v)*dx + a + dot(s_chi,s_xi)*dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi  = s_xi[i]
        calA += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx

    calF = -alpha*(T-R_h)*dot(grad(T),v)*dx + dot(load_ex,v)*dx \
           +alpha*(T_ex-R)*dot(grad(T_ex),v)*dx

    Sol_h = Function(Hh)
    LHS = assemble(calA)
    solver = LUSolver(LHS)
    #solver = PETScKrylovSolver("gmres", "ilu")
    RHS = None

    # ******** Picard iterations **** #
    res = 1.0; inc = 0; tol = 4.0E-5; maxiter = 30
    
    while res > tol and inc < maxiter:
        print("iter {}, res = {}".format(inc, res))
        T.u = u_n
        RHS = assemble(calF, tensor=RHS)
        solver.solve(Sol_h.vector(), RHS)
        u_h,chi_h = Sol_h.split(True)
    
        res = pow(assemble((u_h-u_n)**2*dx),0.5)
        assign(u_n,u_h)
        inc += 1

    it.append(inc)
    
    # ******** Exporting solutions **** #
    u_h.rename("u","u"); fileO.write(u_h,nk*1.0)
    R_h.rename("R","R"); fileO.write(R_h,nk*1.0)
    T_h = interpolate(T,FunctionSpace(mesh,'CG',1))
    T_h.rename("T","T"); fileO.write(T_h,nk*1.0)
        
    # *** Computing errors
    eu.append(errornorm(u_ex,u_h,'H1'))
   
    if(nk>0):
        ru.append(ln(eu[nk]/eu[nk-1])/(-0.5*ln(float(nn[nk])/nn[nk-1])))
        
    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, 'DG', 0)
    w  = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)

    PsiK = hK**2*(1.0/dt*u_h \
                  - div(C(eps(u_h))) - load_ex)**2*w*dx \
                  + avg(he)*jump(C(eps(u_h)),n)**2*avg(w)*dS \
                  + he*(C(eps(u_h))*n)**2*w*ds
    
    globalPsi=assemble(PsiK)
    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))    

    eff.append(errornorm(u_ex,u_h,'H1')*lmbda/Psi)

    # ********* Mesh adaptivity ******** #
    ref_ratio = 0.01

    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    cell_markers.set_all(True)
    for c in cells (mesh):
        cell_markers[c] = globalPsi[c.index()] > (ref_ratio * globalPsi_max)

    adapt(mesh, cell_markers)
    
    mesh.smooth(50); mesh.smooth_boundary(50)
    mesh = mesh.child()
    
    adapt(bdry, mesh); bdry = bdry.child()


# ******* Convergence history ********** *
print('DoF   &  h_min  &   e(u)  &   r(u)  & eff  & iter ')
print('==================================================')
for nk in range(nkmax):
    print('%d & %4.4g & %3.3e & %4.4g & %4.4g & %d' % (nn[nk], hh[nk], eu[nk], ru[nk], eff[nk], it[nk]))
