from __future__ import print_function
from dolfin import *
import dolfin as df
import time
from AndersonAcceleration import AndersonAcceleration

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 6
parameters["allow_extrapolation"] = True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"


def get_time():
    return time.perf_counter()


start = get_time()
mesh = UnitSquareMesh(40, 40)  # 20x20 default

doPlot = False

# ******* Mechanical parameters ******* #

E = Constant(15.0)
nu = Constant(0.3)
lmbda = E * nu / ((1.0 + nu) * (1.0 - 2.0 * nu))
mu = E / (2.0 * (1.0 + nu))

alpha = Constant(1000)
order_anderson = 1


def C(ten): return lmbda * tr(ten) * Identity(2) + 2 * mu * ten


def eps(vec): return sym(grad(vec))


# ********* Mesh refinement loop ********* #

# Set to 1 if called by mixed solver
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)

R_h = Expression(
    "pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2) < pow(0.64/2,2) && pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2) > pow(0.33/2,2) && !(x[0]>0.5&&x[1]<0.6&&x[1]>0.4)  ? 1 : 0", degree=6)


vectorP1 = VectorElement("CG", mesh.ufl_cell(), 1)
u_n = Function(FunctionSpace(mesh, vectorP1))

t_loading = get_time()
print("loading time ", t_loading - start)

if __name__ == '__main__':
    nkmax = 5  # 5 default
    fileO = XDMFFile("outputs/primal-oc-adaptive.xdmf")
    fileO.parameters["functions_share_mesh"] = True
    fileO.parameters["flush_output"] = True
else:
    nkmax = 1
    mesh = UnitSquareMesh(20 * 2**2, 20 * 2**2)


def export(i, mesh, u_n, R_h, T, T0):
    if __name__ != '__main__':
        return i+1
    t_export = time.perf_counter()
    Vscalar = FunctionSpace(mesh, 'CG', 1)
    Vvector = VectorFunctionSpace(mesh, 'CG', 1)
    u_out = Function(Vvector)
    u_out.interpolate(u_n)
    u_out.rename("u", "u")
    fileO.write(u_out, i)
    R_out = Function(Vscalar)
    T_out = Function(Vscalar)
    T0_out = Function(Vscalar)
    fileO.write(u_n, i)
    R_out.interpolate(R_h)
    R_out.rename("R", "R")
    fileO.write(R_out, i)
    # R_h.rename("R","R"); fileO.write(R_h,i)
    # T_h = interpolate(T,Vscalar)
    T_out.interpolate(T)
    T_out.rename("T", "T")
    fileO.write(T_out, i)
    # fileO.write(T,i)
    T0_out.interpolate(T0)
    T0_out.rename("T0", "T0")
    fileO.write(T0_out, i)
    # T0.rename("T0","T0"); fileO.write(T0,i)
    print("Saved step {} in {}s".format(i, time.perf_counter() - t_export))
    return i+1


ii = 0


def solve_iteration(ii, mesh, nk, u_prev):
    hmin = mesh.hmin()
    anderson = AndersonAcceleration(order_anderson)
    # dt = Constant(hmin**2 / alpha)
    dt = Constant(0.1 * hmin / alpha)
    t_loop_init = get_time()
    print("......................Refinement level : nk = ", nk)
    n = FacetNormal(mesh)
    t = as_vector((-n[1], n[0]))
    T = Expression("pow(x[0] + u[0] - 0.5, 2) + pow(x[1] + u[1] - 0.5, 2) < pow(0.5/2,2) ? 1 : 0",
                   degree=6, u=Constant((0, 0)), domain=mesh)
    T0 = Expression("pow(x[0] + u[0] - 0.5, 2) + pow(x[1] + u[1] - 0.5, 2) < pow(0.5/2,2) ? 1 : 0",
                    degree=6, u=Constant((0, 0)), domain=mesh)

    #  ********* Defintion of function spaces  ******* #

    vectorP1 = VectorElement("CG", mesh.ufl_cell(), 1)
    RM = VectorElement('R', triangle, 0, dim=3)

    Hh = FunctionSpace(mesh, MixedElement([vectorP1, RM]))

    print("h_min = {}, DoF = {}".format(mesh.hmin(), Hh.dim()))

    v, s_xi = TestFunctions(Hh)
    u, s_chi = TrialFunctions(Hh)

    nullspace = [Constant((1, 0)), Constant((0, 1)),
                 Expression(('-x[1]+0.5', 'x[0]+0.5'), degree=1)]

    # ******** Pure traction BCs are natural for primal ************* #

    # nothing to do

    # ******** Reference and target images ************* #

    u_n = interpolate(u_prev, FunctionSpace(mesh, vectorP1))

    # ******** Weak form of the linearised problem ************* #

    dx = df.dx(mesh)
    ds = df.ds(mesh)

    a = inner(C(eps(u)), eps(v)) * dx

    calA = dot(u, v) * dx + dt * a + dt * dot(s_chi, s_xi) * dx
    calA_err = a + dot(s_chi, s_xi)*dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi = s_xi[i]
        calA -= dt * chi * dot(v, ns_i) * dx + dt * xi * dot(u, ns_i) * dx
        calA_err -= chi*dot(v, ns_i)*dx + xi*dot(u, ns_i)*dx

    calF = - alpha * dt * (T - R_h) * dot(grad(T), v) * dx + dot(u_n, v) * dx
    calF_err = -alpha*(T-R_h)*dot(grad(T), v)*dx

    Sol_h = Function(Hh)
    LHS = PETScMatrix()
    LHS_err = PETScMatrix()
    assemble(calA, tensor=LHS)
    assemble(calA_err, tensor=LHS_err)
    solver = PETScKrylovSolver("bicgstab", "ilu")
    PETScOptions.set("pc_factor_levels", 3)
    PETScOptions.set("ksp_atol", 1e-12)
    PETScOptions.set("ksp_rtol", 1e-10)
    solver.set_from_options()
    solver.set_operator(LHS)
    RHS = PETScVector()
    RHS_err = PETScVector()
    T.u = u_n
    assemble(calF, tensor=RHS)
    assemble(calF_err, tensor=RHS_err)
    res0 = RHS_err.vec().norm()

    t_assembly = get_time()
    print("assembling time ", t_assembly - t_loop_init)

    # ******** Picard iterations **** #
    res = 1.0
    inc = 0
    tol = 1.0E-4
    if nk == 0:
        maxiter = 200
    else:
        maxiter = 50
    miniter = 0

    tot_time = 0.0
    ii = export(ii, mesh, u_n, R_h, T, T0)
    while res > tol and inc < maxiter or inc < miniter:  # Always do some iterations
        t_picard_init = get_time()
        # print("iter {}, res = {}".format(inc, res))

        print("iter {}, res = {}".format(inc, res), flush=True)
        solver.solve(LHS, Sol_h.vector(), RHS)

        u_h, chi_h = Sol_h.split(True)

        assign(u_n, u_h)
        anderson.get_next_vector(u_n.vector().vec())
        T.u = u_n
        assemble(calF, tensor=RHS)
        assemble(calF_err, tensor=RHS_err)
        ii = export(ii, mesh, u_n, R_h, T, T0)
        res = (LHS_err * Sol_h.vector() - RHS_err).vec().norm()/res0
        inc += 1
        print("solution time ", time.perf_counter()-t_picard_init)

        t_solve = get_time()
        tot_time += t_solve - t_picard_init
    print("Average solution time {:.3f} in {} iterations".format(
        tot_time / inc, inc))

    if doPlot:
        import pylab as plt
        plt.clf()
        plt.subplot(1, 2, 1)
        plot(T, mesh=mesh)
        plt.subplot(1, 2, 2)
        plot(mesh)
        plt.show()

    t_picard_end = get_time()
    if __name__ == "__main__":
        ii = export(ii, mesh, u_n, R_h, T, T0)

    t_io = get_time()
    print("IO time ", t_io - t_picard_end)

    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, 'DG', 0)
    w = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)

    PsiK = hK**2 * (1. / dt * u_h + alpha * (T - R_h) * grad(T) - div(C(eps(u_h))))**2 * w * dx \
        + chi_h**2 * w * dx \
        + avg(he) * (jump(C(eps(u_h)) * n))**2 * avg(w) * dS \
        + he * (C(eps(u_h)) * n)**2 * w * ds

    globalPsi = assemble(PsiK)
    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))

    t_estimator = get_time()
    print("compute estimator time ", t_estimator - t_io)

    # ********* Mesh adaptivity ******** #
    tolAdapt = 1.0E-5
    ref_ratio = 0.075

    if (Psi < tolAdapt and nk == nkmax):
        print("\nEstimated error < tolerance: %s < %s" % (Psi, tolAdapt))
        return None

    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    cell_markers.set_all(True)
    for c in cells(mesh):
        cell_markers[c] = globalPsi[c.index()] > (ref_ratio * globalPsi_max)

    mesh = refine(mesh, cell_markers)
    # mesh.smooth(50)#; mesh.smooth_boundary(50)
    # mesh = mesh.child()
    # adapt(bdry, mesh); bdry = bdry.child()

    t_refine = get_time()
    print("marking and ref time ", t_refine - t_estimator)
    return mesh, u_n, R_h, T, T0, ii


    # ************* End **************** #
if __name__ == "__main__":
    for nk in range(nkmax):
        mesh, u_n, R, T, T0, ii_ = solve_iteration(ii, mesh, nk, u_n)
        ii = ii_
        if not mesh:
            break
    fileO.close()

if __name__ == '__main__':
    mesh = UnitSquareMesh(512, 512)
    ii = export(ii, mesh, u_n, R_h, T, T0)
print("Total time: ", get_time() - start)
