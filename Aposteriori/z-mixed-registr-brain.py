from __future__ import print_function
from dolfin import *
parameters["form_compiler"]["cpp_optimize"] = True

mesh = Mesh("../Meshes/brainwebSmall_meshR.xml")
fileO = XDMFFile("outputs/mixed-reg-brain-unif.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

xpr_file = open( "compositionFunction.cpp" , 'r' )  
displ_compos = xpr_file.read()

# ******* Mechanical parameters ******* #

E     = Constant(15.0)
nu    = Constant(0.25)
lmbda = E*nu/((1.0+nu)*(1.0-2.0*nu))
mu    = E/(2.0*(1.0+nu))

#lmbda, mu = 10.0, 10.0

alpha = Constant(50.0) # 20
dt    = 0.01/alpha     #0.005

Cinv  = lambda ten: 0.5/mu*ten - lmbda/(4*mu*(lmbda+mu))*tr(ten)*Identity(2)

print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

#  ********* Defintion of function spaces  ******* #

vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), 1)
vectorP0  = VectorElement("DG", mesh.ufl_cell(), 0)
P0        = FiniteElement("DG", mesh.ufl_cell(), 0)
RM        = VectorElement('R', triangle, 0, dim=3)

Hh = FunctionSpace(mesh, MixedElement([vectorBDM,vectorBDM,RM,vectorP0,P0]))
print( ".----. DoFs = ", Hh.dim())

tau1,tau2, s_xi,  v, eta12 = TestFunctions(Hh)
sig1,sig2, s_chi, u, rho12 = TrialFunctions(Hh)

sigma = as_tensor((sig1,sig2))
tau = as_tensor((tau1,tau2))

nullspace=[Constant((1,0)), Constant((0,1)),\
           Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

rho = as_tensor(((0,rho12),(-rho12,0)))
eta = as_tensor(((0,eta12),(-eta12,0)))
    
# ******** Pure traction BCs are essential for mixed ************* #

# will do it with Nitsche, encoded in calA

# ******** Reference and target images ************* #

R_h = Function(FunctionSpace(mesh,'CG',1))
T0  = Function(FunctionSpace(mesh,'CG',1))

input_file = HDF5File(mesh.mpi_comm(),"../HDF5/brainwebSmall.h5", "r")
input_file.read(R_h, "R")
input_file.read(T0, "T")

u_n = Function(FunctionSpace(mesh,vectorP0))
sig1_n = Function(FunctionSpace(mesh,vectorBDM))
sig2_n = Function(FunctionSpace(mesh,vectorBDM))
sigma_n = as_tensor((sig1_n,sig2_n))

T = Expression(displ_compos, domain=mesh, degree=1)
T.f = T0

n = FacetNormal(mesh)

# ******** Weak form of the linearised problem ************* #

a  = inner(Cinv(sigma),tau)*dx
bt = dot(u,div(tau))*dx + inner(rho,tau) *dx
b  = dot(v,div(sigma))*dx + inner(eta,sigma) *dx

calA = -1.0/dt*dot(u,v)*dx + a + bt + b + dot(s_chi,s_xi)*dx \
       + dot(sigma*n,tau*n)*ds

for i, ns_i in enumerate(nullspace):
    chi = s_chi[i]
    xi  = s_xi[i]
    calA -= chi*dot(v, ns_i)*dx + xi*dot(u, ns_i)*dx
   
calF = alpha*(T-R_h)*dot(grad(T),v)*dx \
       -1.0/dt*dot(u_n,v)*dx

LHS = assemble(calA)
solver = PETScKrylovSolver("bicgstab", "ilu")
RHS = None
Sol_h = Function(Hh)

# ******** Picard iterations **** #
res = 1.0; inc = 0; tol = 1.0E-4; maxiter = 20

while res > tol and inc < maxiter:
    print("iter {}, res = {}".format(inc, res))
    T.u = u_n
    RHS = assemble(calF, tensor=RHS)
    solver.solve(LHS, Sol_h.vector(), RHS)
    sig1_h,sig2_h,chi_h,u_h,rho12_h = Sol_h.split(True)

    res = pow(assemble((u_h-u_n)**2*dx),0.5)
    assign(u_n,u_h)
    
    u_h.rename("u","u"); fileO.write(u_h,inc*1.0)
    sig1_h.rename("sig1","sig1"); fileO.write(sig1_h,inc*1.0)
    sig2_h.rename("sig2","sig2"); fileO.write(sig2_h,inc*1.0)
    rho12_h.rename("rho12","rho12"); fileO.write(rho12_h,inc*1.0)
    R_h.rename("R","R"); fileO.write(R_h,inc*1.0)
    T_h = interpolate(T,FunctionSpace(mesh,'CG',1))
    T_h.rename("T","T"); fileO.write(T_h,inc*1.0)
    inc += 1
