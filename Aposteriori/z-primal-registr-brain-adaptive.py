from __future__ import print_function
import numpy as np
from PIL.Image import open as PilOpen
from dolfin import *
from RegFem import Images
import time
from petsc4py import PETSc
from AndersonAcceleration import AndersonAcceleration

start = time.perf_counter()

parameters["form_compiler"]["cpp_optimize"] = True
parameters["allow_extrapolation"] = True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"

# mesh = Mesh("../Meshes/brainwebSmallCoarse_meshR.xml")
fileO = XDMFFile("outputs/primal-brain-adaptive-coarse-0p-noise.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# xpr_file = open( "compositionFunction.cpp" , 'r' )
# displ_compos = xpr_file.read()

# ******* Mechanical parameters ******* #

E = Constant(15.0)
nu = Constant(0.3)
lmbda = E*nu/((1.0+nu)*(1.0-2.0*nu))
mu = E/(2.0*(1.0+nu))

alpha = Constant(50.0)
dt = 0.01/alpha
maxiter = 20


def C(ten): return lmbda*tr(ten)*Identity(2)+2*mu*ten


def eps(vec): return sym(grad(vec))


print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ********* Mesh refinement loop ********* #

nkmax = 5
order_anderson = 5
# bdry = MeshFunction("size_t", mesh, 1)
# bdry.set_all(0)

# Load images
sigma = 0.0
Rimg = np.array(PilOpen("brainR.gif"))/255
Timg = np.array(PilOpen("brainT.gif"))/255

# Add noise
np.random.seed(0)
Rimg = Rimg + np.random.normal(0, sigma, Rimg.shape)
Timg = Timg + np.random.normal(0, sigma, Timg.shape)

t_loading = time.perf_counter()


print("Loading images")
mesh_ref = UnitSquareMesh(258, 258)
mesh = UnitSquareMesh(64, 64)
images = Images.NPYImages(mesh_ref, Rimg, Timg, generate_gradients=True)
R_h = images.R

u_nn = None
T = images.T
T0 = images.T.f
print("Loading time ", time.perf_counter()-start)
ii = 0

for nk in range(nkmax):

    def export(i, mesh, u_n, R_h, T, T0):
        t_export = time.perf_counter()
        Vscalar = FunctionSpace(mesh, 'CG', 1)
        Vvector = VectorFunctionSpace(mesh, 'CG', 1)
        u_out = Function(Vvector)
        u_out.interpolate(u_n)
        u_out.rename("u", "u")
        fileO.write(u_out, i)
        R_out = Function(Vscalar)
        T_out = Function(Vscalar)
        T0_out = Function(Vscalar)
        fileO.write(u_n, i)
        R_out.interpolate(R_h)
        R_out.rename("R", "R")
        fileO.write(R_out, i)
        # R_h.rename("R","R"); fileO.write(R_h,i)
        # T_h = interpolate(T,Vscalar)
        T_out.interpolate(T)
        T_out.rename("T", "T")
        fileO.write(T_out, i)
        # fileO.write(T,i)
        T0_out.interpolate(T0)
        T0_out.rename("T0", "T0")
        fileO.write(T0_out, i)
        # T0.rename("T0","T0"); fileO.write(T0,i)
        print("Saved step {} in {}s".format(i, time.perf_counter() - t_export))
        return i+1
    anderson = AndersonAcceleration(order_anderson)
    dt = Constant(min(0.01, mesh.hmin())/alpha)
    t_loop_init = time.perf_counter()
    print("......................Refinement level : nk = ", nk, flush=True)
    n = FacetNormal(mesh)
    t = as_vector((-n[1], n[0]))

    #  ********* Defintion of function spaces  ******* #

    vectorP1 = VectorElement("CG", mesh.ufl_cell(), 1)
    RM = VectorElement('R', triangle, 0, dim=3)

    Hh = FunctionSpace(mesh, MixedElement([vectorP1, RM]))

    print("h_min = {}, DoF = {}".format(mesh.hmin(), Hh.dim()))

    v, s_xi = TestFunctions(Hh)
    u, s_chi = TrialFunctions(Hh)

    nullspace = [Constant((1, 0)), Constant((0, 1)),
                 Expression(('-x[1]+0.5', 'x[0]+0.5'), degree=1)]

    # ******** Pure traction BCs are natural for primal ************* #

    # nothing to do

    # ******** Reference and target images ************* #

    Vu = FunctionSpace(mesh, vectorP1)
    u_n = Function(Vu)
    if u_nn:
        u_n.interpolate(u_nn)
    images.setDisplacement(u_n.cpp_object())

    # ******** Weak form of the linearised problem ************* #

    dx = dx(mesh)
    ds = ds(mesh)

    a = inner(C(eps(u)), eps(v))*dx

    calA = 1.0/dt*dot(u, v)*dx + a + dot(s_chi, s_xi)*dx
    calA_err = a + dot(s_chi, s_xi)*dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi = s_xi[i]
        calA -= chi*dot(v, ns_i)*dx + xi*dot(u, ns_i)*dx
        calA_err -= chi*dot(v, ns_i)*dx + xi*dot(u, ns_i)*dx

    calF = -alpha*(T-R_h)*dot(images.dT, v)*dx \
        + 1.0/dt*dot(u_n, v)*dx
    calF_err = -alpha*(T-R_h)*dot(images.dT, v)*dx

    Sol_h = Function(Hh)

    LHS = PETScMatrix()
    LHS_err = PETScMatrix()
    assemble(calA, tensor=LHS)
    assemble(calA_err, tensor=LHS_err)
    solver = PETScKrylovSolver("gmres", "ilu")
    PETScOptions.set("pc_factor_levels", 3)
    PETScOptions.set("ksp_atol", 1e-12)
    PETScOptions.set("ksp_rtol", 1e-10)
    solver.set_from_options()
    solver.set_operator(LHS)
    t_assembly = time.perf_counter()

    # ******** Picard iterations **** #
    res = 1.0
    inc = 0
    tol = 1.0E-4
    RHS = PETScVector()
    RHS_err = PETScVector()
    images.setDisplacement(u_n.cpp_object())
    assemble(calF, tensor=RHS)
    assemble(calF_err, tensor=RHS_err)
    res0 = RHS_err.vec().norm()
    print("assembling time ", time.perf_counter()-t_loop_init)
    ii = export(ii, mesh, u_n, R_h, T, T0)
    while res > tol and inc < maxiter:
        t_picard_init = time.perf_counter()

        print("iter {}, res = {}".format(inc, res), flush=True)
        solver.solve(Sol_h.vector(), RHS)

        t_solve = time.perf_counter()

        u_h, chi_h = Sol_h.split(True)

        # res = pow(assemble((u_h-u_n)**2*dx),0.5)
        assign(u_n, u_h)
        anderson.get_next_vector(u_n.vector().vec())
        images.setDisplacement(u_n.cpp_object())
        assemble(calF, tensor=RHS)
        assemble(calF_err, tensor=RHS_err)
        ii = export(ii, mesh, u_n, R_h, T, T0)
        res = (LHS_err * Sol_h.vector() - RHS_err).vec().norm()/res0
        inc += 1
        print("solution time ", time.perf_counter()-t_picard_init)

    u_nn = u_n.copy(True)

    t_picard_end = time.perf_counter()

    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, 'DG', 0)
    w = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)

    t_io = time.perf_counter()
    print("IO time ", time.perf_counter()-t_picard_end, flush=True)

    PsiK = hK**2*(1./dt*u_h+alpha*(T-R_h)*images.dT-div(C(eps(u_h))))**2*w*dx \
        + chi_h**2*w*dx \
        + avg(he)*(jump(C(eps(u_h))*n))**2*avg(w)*dS \
        + he*(C(eps(u_h))*n)**2*w*ds

    globalPsi = assemble(PsiK)
    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))

    t_estimator = time.perf_counter()
    print("compute estimator time ", time.perf_counter()-t_io)

    # ********* Mesh adaptivity ******** #
    tolAdapt = 1.0E-5
    ref_ratio = 0.05

    if (Psi < tolAdapt and nk == nkmax):
        print("\nEstimated error < tolerance: %s < %s" % (Psi, tolAdapt))
        break

    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    cell_markers.set_all(0)
    for c in cells(mesh):
        cell_markers[c] = globalPsi[c.index()] > (ref_ratio * globalPsi_max)

    mesh = refine(mesh, cell_markers)
    # adapt(cell_markers, mesh)
    # mesh.smooth(50) #; mesh.smooth_boundary(50)
    # mesh = mesh.child()
    # adapt(bdry, mesh); bdry = bdry.child()

    t_refine = time.perf_counter()
    print("marking and ref time ", time.perf_counter()-t_estimator)

# ************* End **************** #
# Final export
ii = export(ii, mesh_ref, u_n, R_h, T, T0)
print("Total time: ", time.perf_counter()-start)
