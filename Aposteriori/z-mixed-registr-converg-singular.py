from __future__ import print_function
from dolfin import *
import sympy2fenics as sf

parameters["form_compiler"]["cpp_optimize"] = True
fileO = XDMFFile("outputs/mixed-reg-conv-sing.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

xpr_file = open( "composition.cpp" , 'r' )  
displ_compos = xpr_file.read()

# ********* Model coefficients and parameters ********* #

E     = Constant(1.0e3)
nu    = Constant(0.4)
lmbda = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu    = Constant(E/(2.*(1.+nu)))
alpha = Constant(100.0)
dt    = 1/alpha**2

Cinv  = lambda ten: 0.5/mu*ten - lmbda/(4*mu*(lmbda+mu))*tr(ten)*Identity(2)

print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #
Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
uu = sf.str2sympy('(0.1*cos(2*pi*x)*sin(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**2, -0.1*sin(2*pi*x)*cos(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**3)')

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)
gradu = sf.grad(uu)
gradu_code = sf.sympy2exp(gradu)
gradu_ex = Expression(gradu_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('x*y*(x-1)*(y-1)/((x+0.02)**4+(y+0.02)**4)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)

targ0 = sf.str2sympy('exp(-50*((x-0.2)**2+(y-0.2)**2))')
targ0_code = sf.sympy2exp(targ0)
T0 = Expression(targ0_code, degree=6, cell=triangle)

# setting T0(x + u) with u(0) and u(1) from above
#targex = sf.str2sympy('sin(2*pi*(x+0.1*cos(pi*x)*sin(pi*y)+0.5*(x*(1-x)*y*(1-y))**2/lmbda))*cos(2*pi*(y-0.1*sin(pi*x)*cos(pi*y)+0.5*(x*(1-x)*y*(1-y))**3/lmbda))')
#ff = alpha * (targex-ref) * sf.grad(targex).transpose()

rhorho   = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex  =  Expression(rho_code, degree=6, cell=triangle, lmbda=lmbda)

sigmasigma   = 2.0*mu*sf.epsilon(uu) + lmbda*sf.div(uu)*Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=6, cell=triangle,lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=5, cell=triangle, lmbda=lmbda)

load = 1/dt*uu - divsig #+ ff <- this term will be inlcuded explicitely in calF
load_code = sf.sympy2exp(load)
load_ex  = Expression(load_code, degree=6, cell=triangle, lmbda=lmbda)

# ********* Mesh refinement loop ********* #

hh = []; nn = []; eu = []; ru = [];
er = []; rr = []; es = []; rs = []; it =[];
rs.append(0.0); ru.append(0.0); rr.append(0.0); 

k=0; nkmax = 6;

for nk in range(nkmax):
    print( "......................Refinement level : nk = ", nk)
    nps = pow(2,nk+1)
    mesh = UnitSquareMesh(nps,nps)
    n = FacetNormal(mesh)
    hh.append(mesh.hmax())

    # ********* Finite dimensional spaces ********* #
    
    vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), k+1)
    vectorP0  = VectorElement("DG", mesh.ufl_cell(), k)
    P0        = FiniteElement("DG", mesh.ufl_cell(), k)
    RM        = VectorElement('R', triangle, 0, dim=3)
    
    # Mixed space 
    Hh = FunctionSpace(mesh, MixedElement([vectorBDM,vectorBDM,RM,vectorP0,P0]))

    tau1,tau2, s_xi,  v, eta12 = TestFunctions(Hh)
    sig1,sig2, s_chi, u, rho12 = TrialFunctions(Hh)

    sigma = as_tensor((sig1,sig2))
    tau = as_tensor((tau1,tau2))

    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

    rho = as_tensor(((0,rho12),(-rho12,0)))
    eta = as_tensor(((0,eta12),(-eta12,0)))

    nn.append(Hh.dim())

    # ******** Pure traction BCs are essential for mixed ****** #

    #bcs1 = DirichletBC(Hh.sub(0), [0,0], 'on_boundary')
    #bcs2 = DirichletBC(Hh.sub(1), [0,0], 'on_boundary')
    #bcS = [bcs1,bcs2]

    #will use Nitsche in calA
    
    # ******** Reference and target images ************* #

    R_h = interpolate(R,FunctionSpace(mesh,'CG',1))
    T = Expression(displ_compos, domain=mesh, degree=2)
    T.f = T0
    
    u_n = Function(FunctionSpace(mesh,vectorP0))
    sig1_n = Function(FunctionSpace(mesh,vectorBDM))
    sig2_n = Function(FunctionSpace(mesh,vectorBDM))
    sigma_n = as_tensor((sig1_n,sig2_n))

    # ******** Exact target for error computation ******* #
    
    u_ex_h = interpolate(u_ex,FunctionSpace(mesh,vectorP0))
    T_ex = Expression(displ_compos,domain=mesh, degree=2)
    T_ex.f = T0; T_ex.u = u_ex_h
        
    # ******** Weak forms ************* #

    a  = inner(Cinv(sigma),tau)*dx
    bt = dot(u,div(tau))*dx + inner(rho,tau) *dx
    b  = dot(v,div(sigma))*dx + inner(eta,sigma) *dx

    calA = -1.0/dt*dot(u,v)*dx + a + bt + b + dot(s_chi,s_xi)*dx \
           + dot(sigma*n,tau*n)*ds

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi  = s_xi[i]
        calA -= chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx

    calF = alpha*(T-R_h)*dot(grad(T),v)*dx - dot(load_ex,v)*dx \
           -alpha*(T_ex-R)*dot(grad(T_ex),v)*dx

    #Because of load_ex, I need to remove this term: -1.0/dt*dot(u_n,v)*dx

    LHS = assemble(calA)
    solver = LUSolver(LHS)
    #solver = PETScKrylovSolver("gmres", "ilu")
    RHS = None
    Sol_h = Function(Hh)

    # ******** Picard iterations **** #
    res = 1.0; inc = 0; tol = 4.0E-5; maxiter = 50

    Sol_h = Function(Hh)
    
    while res > tol and inc < maxiter:
        print("iter {}, res = {}".format(inc, res))
        T.u = u_n
        RHS = assemble(calF, tensor=RHS)
        #[bc.apply(LHS, RHS) for bc in bcS]
        solver.solve(Sol_h.vector(), RHS)
        sig1_h,sig2_h,chi_h,u_h,rho12_h = Sol_h.split(True)
        sigma_h = as_tensor((sig1_h,sig2_h))
        rho_h = as_tensor(((0,rho12_h),(-rho12_h,0)))
    
        res = pow(assemble((u_h-u_n)**2*dx),0.5) \
              + pow(assemble(inner(sigma_n-sigma_h,sigma_n-sigma_h)*dx \
                             + dot(div(sigma_h)-div(sigma_n),div(sigma_h)-div(sigma_n))*dx), 0.5)
        assign(u_n,u_h);
        sigma_n = as_tensor((sig1_h,sig2_h))
        
        inc += 1

    it.append(inc)
    

    # ******** Exporting solutions **** #
    u_h.rename("u","u"); fileO.write(u_h,nk*1.0)
    sig1_h.rename("sig1","sig1"); fileO.write(sig1_h,nk*1.0)
    sig2_h.rename("sig2","sig2"); fileO.write(sig2_h,nk*1.0)
    rho12_h.rename("rho12","rho12"); fileO.write(rho12_h,nk*1.0)
    R_h.rename("R","R"); fileO.write(R_h,nk*1.0)
    T_h = interpolate(T,FunctionSpace(mesh,'CG',1))
    T_h.rename("T","T"); fileO.write(T_h,nk*1.0)
        
    # *** Computing errors. sigmaex and rhoex are not "expressions"
    E_s = assemble(inner(sigma_ex-sigma_h,sigma_ex-sigma_h)*dx \
                   + dot(div(sigma_h)-divsigma_ex,div(sigma_h)-divsigma_ex)*dx)
    E_rho = assemble(inner(rho_h-rho_ex,rho_h-rho_ex)*dx)
    
    es.append(pow(E_s,0.5))
    eu.append(errornorm(u_ex,u_h,'L2'))
    er.append(pow(E_rho,0.5))
   
    if(nk>0):
        ru.append(ln(eu[nk]/eu[nk-1])/ln(hh[nk]/hh[nk-1]))
        rs.append(ln(es[nk]/es[nk-1])/ln(hh[nk]/hh[nk-1]))
        rr.append(ln(er[nk]/er[nk-1])/ln(hh[nk]/hh[nk-1]))

# Generating convergence history
print('DoF   &  hh  &   e(s)  &   r(s)  &  e(u)  &   r(u)  &  e(r)  &  r(r)  & iter')
print('===========================================================================')
for nk in range(nkmax):
    print('%d & %4.4g & %g & %4.4g & %3.3e & %4.4g & %3.3e & %4.4g & %d' % (nn[nk], hh[nk], es[nk], rs[nk], eu[nk], ru[nk], er[nk], rr[nk], it[nk]))
