from __future__ import print_function
from dolfin import *
import sympy2fenics as sf

parameters["form_compiler"]["cpp_optimize"] = True
fileO = XDMFFile("outputs/mixed-reg-conv-RM.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

xpr_file = open("composition.cpp", 'r')
displ_compos = xpr_file.read()

# ********* Model coefficients and parameters ********* #

E = Constant(1.0e3)
nu = Constant(0.49999)
lmbda = Constant(E * nu / ((1. + nu) * (1. - 2. * nu)))
mu = Constant(E / (2. * (1. + nu)))
alpha = Constant(100.0)
dt = Constant(0.1 / alpha)

def Cinv(ten): return 0.5 / mu * ten - lmbda / (4 * mu * (lmbda + mu)) * tr(ten) * Identity(2)


print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #
Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
uu = sf.str2sympy(
    '(0.1*cos(2*pi*x)*sin(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**2/2/{lamb}, -0.1*sin(2*pi*x)*cos(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**3/2/{lamb})'.format(lamb=lmbda(0)))

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)
gradu = sf.grad(uu)
gradu_code = sf.sympy2exp(gradu)
gradu_ex = Expression(gradu_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('sin(2*pi*x)*sin(2*pi*y)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)

targ0 = sf.str2sympy('sin(2*pi*x)*sin(2*pi*y)')
x, y = sf.symbols('x y')
targ0 = targ0.subs({'x': x - uu[0], 'y': y - uu[1]})

# Create exact Expression and enable evaluation of u
targ0_code = sf.sympy2exp(targ0)
T0 = Expression(targ0_code,
                degree=8,
                cell=triangle,
                u=Constant((0, 0)))
targ0_code = targ0_code.replace('x[0]', 'x[0] + u[0]').replace('x[1]', 'x[1] + u[1]')


# setting T0(x + u) with u(0) and u(1) from above
#targex = sf.str2sympy('sin(2*pi*(x+0.1*cos(pi*x)*sin(pi*y)+0.5*(x*(1-x)*y*(1-y))**2/lmbda))*cos(2*pi*(y-0.1*sin(pi*x)*cos(pi*y)+0.5*(x*(1-x)*y*(1-y))**3/lmbda))')
#ff = alpha * (targex-ref) * sf.grad(targex).transpose()

rhorho = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex = Expression(rho_code, degree=8, cell=triangle, lmbda=lmbda)

sigmasigma = 2.0 * mu(0) * sf.epsilon(uu) + lmbda(0) * sf.div(uu) * Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=8, cell=triangle, lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=8, cell=triangle, lmbda=lmbda)

load = 1 / dt(0) * uu - divsig  # + ff <- this term will be inlcuded explicitely in calF
load_code = sf.sympy2exp(load)
load_ex = Expression(load_code, degree=8, cell=triangle, lmbda=lmbda)

# ********* Mesh refinement loop ********* #

hh = []
nn = []
eu = []
ru = []
er = []
rr = []
es = []
rs = []
it = []
rs.append(0.0)
ru.append(0.0)
rr.append(0.0)

k = 0
nkmax = 7

for ii, nk in enumerate(range(2, nkmax)):
    print("......................Refinement level : nk = ", nk)
    nps = pow(2, nk + 1)
    mesh = UnitSquareMesh(nps, nps)
    hh.append(mesh.hmax())

    # Create functions again with a domain so that they can be differentiated
    T = Expression(targ0_code,
                   degree=8,
                   cell=triangle,
                   domain=mesh,
                   u=Constant((0, 0)))
    T_ex = Expression(targ0_code,
                      degree=8,
                      cell=triangle,
                      domain=mesh,
                      u=Constant((0, 0)))

    # ********* Finite dimensional spaces ********* #

    vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), k + 1)
    vectorP0 = VectorElement("DG", mesh.ufl_cell(), k)
    P0 = FiniteElement("DG", mesh.ufl_cell(), k)
    RM = VectorElement('R', triangle, 0, dim=3)

    # Mixed space
    Hh = FunctionSpace(mesh, MixedElement([vectorBDM, vectorBDM, RM, vectorP0, P0]))

    tau1, tau2, s_xi, v, eta12 = TestFunctions(Hh)
    sig1, sig2, s_chi, u, rho12 = TrialFunctions(Hh)

    sigma = as_tensor((sig1, sig2))
    tau = as_tensor((tau1, tau2))

    nullspace = [Constant((1, 0)), Constant((0, 1)),
                 Expression(('-x[1]+0.5', 'x[0]+0.5'), degree=1)]

    rho = as_tensor(((0, rho12), (-rho12, 0)))
    eta = as_tensor(((0, eta12), (-eta12, 0)))

    nn.append(Hh.dim())

    # ******** Pure traction BCs are essential for mixed ****** #

    bcs1 = DirichletBC(Hh.sub(0), [0, 0], 'on_boundary')
    bcs2 = DirichletBC(Hh.sub(1), [0, 0], 'on_boundary')
    bcS = [bcs1, bcs2]

    # ******** Reference and target images ************* #

    R_h = interpolate(R, FunctionSpace(mesh, 'CG', 1))
    # T = Expression(displ_compos, domain=mesh, degree=2)
    # T.f = T0

    u_n = Function(FunctionSpace(mesh, vectorP0))
    sig1_n = Function(FunctionSpace(mesh, vectorBDM))
    sig2_n = Function(FunctionSpace(mesh, vectorBDM))
    sigma_n = as_tensor((sig1_n, sig2_n))

    # ******** Exact target for error computation ******* #

    u_ex_h = interpolate(u_ex, FunctionSpace(mesh, vectorP0))
    # T_ex = Expression(displ_compos,domain=mesh, degree=2)
    # T_ex.f = T0; T_ex.u = u_ex_h
    T_ex.u = u_ex_h

    # ******** Weak forms ************* #

    a = inner(Cinv(sigma), tau) * dx
    bt = dot(u, div(tau)) * dx + inner(rho, tau) * dx
    b = dot(v, div(sigma)) * dx + inner(eta, sigma) * dx

    calA = -1.0 / dt * dot(u, v) * dx + a + bt + b + dot(s_chi, s_xi) * dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi = s_xi[i]
        calA -= chi * inner(v, ns_i) * dx + xi * inner(u, ns_i) * dx

    calF = alpha * (T - R_h) * dot(grad(T), v) * dx - dot(load_ex, v) * dx \
        - alpha * (T_ex - R) * dot(grad(T_ex), v) * dx

    # Because of load_ex, I need to remove this term: -1.0/dt*dot(u_n,v)*dx

    LHS = PETScMatrix()
    assemble(calA, tensor=LHS)
    [bc.apply(LHS) for bc in bcS]
    # solver = LUSolver(LHS)
    solver = PETScKrylovSolver()
    solver.set_operator(LHS)
    PETScOptions.set("ksp_type", "gmres")
    PETScOptions.set("ksp_atol", 1e-10)
    PETScOptions.set("ksp_rtol", 1e-8)
    # PETScOptions.set("ksp_monitor_cancel")
    # PETScOptions.set("ksp_monitor")
    # PETScOptions.set("ksp_monitor_true_residual")
    # PETScOptions.set("pc_type", "lu")
    PETScOptions.set("pc_type", "ilu")
    PETScOptions.set("pc_factor_levels", 7)
    solver.set_from_options()
    RHS = None
    Sol_h = Function(Hh)

    # ******** Picard iterations **** #
    res = 1.0
    inc = 0
    tol = 4.0E-5
    maxiter = 20

    Sol_h = Function(Hh)

    while res > tol and inc < maxiter:
        print("iter {}, res = {}".format(inc, res))
        T.u = u_n
        RHS = assemble(calF, tensor=RHS)
        [bc.apply(LHS, RHS) for bc in bcS]
        solver.solve(Sol_h.vector(), RHS)
        sig1_h, sig2_h, chi_h, u_h, rho12_h = Sol_h.split(True)
        sigma_h = as_tensor((sig1_h, sig2_h))
        rho_h = as_tensor(((0, rho12_h), (-rho12_h, 0)))

        res = pow(assemble((u_h - u_n)**2 * dx), 0.5) \
            + pow(assemble(inner(sigma_n - sigma_h, sigma_n - sigma_h) * dx
                           + dot(div(sigma_h) - div(sigma_n), div(sigma_h) - div(sigma_n)) * dx), 0.5)
        assign(u_n, u_h)
        sigma_n = as_tensor((sig1_h, sig2_h))

        inc += 1

    it.append(inc)

    # ******** Exporting solutions **** #
    u_h.rename("u", "u")
    fileO.write(u_h, nk * 1.0)
    sig1_h.rename("sig1", "sig1")
    fileO.write(sig1_h, nk * 1.0)
    sig2_h.rename("sig2", "sig2")
    fileO.write(sig2_h, nk * 1.0)
    rho12_h.rename("rho12", "rho12")
    fileO.write(rho12_h, nk * 1.0)
    R_h.rename("R", "R")
    fileO.write(R_h, nk * 1.0)
    T_h = interpolate(T, FunctionSpace(mesh, 'CG', 1))
    T0_h = interpolate(T0, FunctionSpace(mesh, 'CG', 1))
    T_h.rename("T", "T")
    T0_h.rename("T0", "T0")
    fileO.write(T_h, nk * 1.0)
    fileO.write(T0_h, nk * 1.0)

    # *** Computing errors. sigmaex and rhoex are not "expressions"
    E_s = assemble(inner(sigma_ex - sigma_h, sigma_ex - sigma_h) * dx
                   + dot(div(sigma_h) - divsigma_ex, div(sigma_h) - divsigma_ex) * dx)
    E_rho = assemble(inner(rho_h - rho_ex, rho_h - rho_ex) * dx)

    es.append(pow(E_s, 0.5))
    eu.append(errornorm(u_ex, u_h, 'L2'))
    er.append(pow(E_rho, 0.5))

    if(ii > 0):
        ru.append(ln(eu[ii] / eu[ii - 1]) / ln(hh[ii] / hh[ii - 1]))
        rs.append(ln(es[ii] / es[ii - 1]) / ln(hh[ii] / hh[ii - 1]))
        rr.append(ln(er[ii] / er[ii - 1]) / ln(hh[ii] / hh[ii - 1]))

# Generating convergence history
print('DoF   &  hh  &   e(s)  &   r(s)  &  e(u)  &   r(u)  &  e(r)  &  r(r)  & iter')
print('===========================================================================')
for ii, nk in enumerate(range(2, nkmax)):
    print('%d & %4.4g & %g & %4.4g & %3.3e & %4.4g & %3.3e & %4.4g & %d' %
          (nn[ii], hh[ii], es[ii], rs[ii], eu[ii], ru[ii], er[ii], rr[ii], it[ii]))
