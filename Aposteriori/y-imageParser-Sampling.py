#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from dolfin import *
import numpy as np

def case_brainweb(sigmas):
    """
    Return image fields for itk brain scenario. Template image's mesh is 
    extended to allow evaluations outside the reference domain    
    """
    R_dir  = "brainRsmall.gif"
    T_dir = "brainTsmall.gif"
    FILENAME = "brainwebSmallCoarse"
    
    from skimage.io import imread
    
    if sigmas > 0:
        FILENAME += str(sigmas)
    print("Loading images")
    
    # Thought for one image at a time!!!! #
    
    # Load image arrays #

    Rarr = imread(R_dir)[:,:,0]
    Rarr = Rarr/float(Rarr.max())
    Tarr = imread(T_dir)[:,:,0]
    Tarr = Tarr/float(Tarr.max())

    
    
    from scipy.ndimage import gaussian_filter as gf
    for i in range(sigmas):
        Rarr = gf(Rarr, sigma = 1)
        Tarr = gf(Tarr, sigma = 1)

    # Generate interpolation

    # First R
    from scipy.interpolate import RectBivariateSpline as spline
    nx, ny = Rarr.shape
    lx, ly = 1./nx, 1./ny
    y, x = (np.linspace(0,1-lx, nx) + lx/2), (np.linspace(0,1-ly, ny) + ly/2)
    IR = spline(x, y, Rarr.T)

    #Now T
    nx, ny = Tarr.shape
    lx, ly = 1./nx, 1./ny
    y, x = np.linspace(0,1-lx, nx) + lx/2, (np.linspace(0,1-ly, ny) + ly/2)
    IT = spline(x, y, Tarr.T)


    # Modify them so that they receive a tuple

    R = lambda x: IR(x[0], x[1])[0,0]
    T = lambda x: IT(x[0], x[1])[0,0]
    
    print("Set mesh")

    #Rarr = Rarr[0:nx:4, 0:ny:4]
    #Tarr = Tarr[0:nx:4, 0:ny:4]
    
    # Setting mesh #
    
    a,b = Rarr.shape[0], Rarr.shape[1] 
    mesh = UnitSquareMesh(a,b)
    
    a,b = Tarr.shape[0], Tarr.shape[1] 
    meshT = UnitSquareMesh(a,b)
    
    File("../Meshes/{0}_meshR.xml".format(FILENAME)) << mesh
    return FILENAME, R, T, mesh,  meshT


def generate_HDF5(FILENAME, R, T, mesh,  meshT):

    print("Set spaces")
    
    # Function spaces #
    
    FR = FunctionSpace(mesh, "CG", 1)
    FT = FunctionSpace(meshT, "CG", 1)
    
    # Generate interpolated placeholders
    
    IR = Function(FR)
    IT = Function(FT)
    IR.vector()[:] = 0.
    IT.vector()[:] = 0.
    
    coords = FR.tabulate_dof_coordinates().reshape((-1, 2))
    coordsT = FT.tabulate_dof_coordinates().reshape((-1, 2))
    
    
    print("Setting dolfin::Functions")
    
    coords_list = [coords, coordsT]
    functions = [IR, IT]
    fields = [R, T]
    
    for coords, f, field in zip(coords_list, functions, fields):
        print(f.name(), coords[0,:])
        f.vector()[:] = np.array([field(coords[i,:]) for i in range(len(coords[:,0]))])
    
    
    print("Saving ",)
    print("{0}, ".format(FILENAME))
    
    
    names = ["R", "T"]
    
    functions = [IR, IT] 
    
    writer = HDF5File(mesh.mpi_comm(), "../HDF5/{0}.h5".format(FILENAME), "w")
    for name, function in zip(names, functions):
        print(name)
        writer.write(function, name)
    writer.close()
    
    
FILENAME, R, T, mesh,  meshT = case_brainweb(0)
generate_HDF5(FILENAME, R, T, mesh,  meshT)
