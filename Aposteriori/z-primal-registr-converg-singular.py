from __future__ import print_function
from dolfin import *
import sympy2fenics as sf


parameters["form_compiler"]["cpp_optimize"] = True
fileO = XDMFFile("outputs/primal-reg-conv-sing.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True


# ********* Model coefficients and parameters ********* #

E = Constant(1.0e3)
nu = Constant(0.4999)
lmbda = Constant(E * nu / ((1. + nu) * (1. - 2. * nu)))
mu = Constant(E / (2. * (1. + nu)))
alpha = Constant(100.0)
dt = Constant(1 / alpha**2)
u_degree = 1

def C(ten): return lmbda * tr(ten) * Identity(2) + 2 * mu * ten
def eps(vec): return sym(grad(vec))


print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #
Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
uu = sf.str2sympy(
    '(0.1*cos(2*pi*x)*sin(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**2/{lamb}, -0.1*sin(2*pi*x)*cos(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**3/{lamb})'.format(lamb=lmbda(0)))

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)
gradu = sf.grad(uu)
gradu_code = sf.sympy2exp(gradu)
gradu_ex = Expression(gradu_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('x*y*(x-1)*(y-1)/((x+0.02)**4+(y+0.02)**4)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)

# targ0 = sf.str2sympy('exp(-50*((x-0.2)**2+(y-0.2)**2))')
# targ0_code = sf.sympy2exp(targ0)
T = Expression('exp(-50*(pow(x[0] + u[0]-0.2,2)+pow(x[1] + u[1] -0.2,2)))',
               degree=8,
               cell=triangle,
               u=Constant((0, 0)))
T_ex = Expression('exp(-50*(pow(x[0] + u[0]-0.2,2)+pow(x[1] + u[1] -0.2,2)))',
                  degree=8,
                  cell=triangle,
                  u=Constant((0, 0)))

rhorho = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex = Expression(rho_code, degree=6, cell=triangle, lmbda=lmbda)

sigmasigma = 2.0 * mu(0) * sf.epsilon(uu) + lmbda(0) * sf.div(uu) * Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=8, cell=triangle, lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=8, cell=triangle, lmbda=lmbda)

load = 1 / dt(0) * uu - divsig
load_code = sf.sympy2exp(load)
load_ex = Expression(load_code, degree=8, cell=triangle, lmbda=lmbda)

# ********* Mesh refinement loop ********* #

hh = []
nn = []
eu = []
ru = []
it = []
ru.append(0.0)

nkmax = 6

for nk in range(nkmax):
    print("......................Refinement level : nk = ", nk)
    nps = pow(2, nk + 1)
    mesh = UnitSquareMesh(nps, nps)
    hh.append(mesh.hmax())

    # Create functions again with a domain so that they can be differentiated
    T = Expression('exp(-50*(pow(x[0] + u[0]-0.2,2)+pow(x[1] + u[1] -0.2,2)))',
               degree=8,
               cell=triangle,
               domain=mesh,
               u=Constant((0, 0)))
    T_ex = Expression('exp(-50*(pow(x[0] + u[0]-0.2,2)+pow(x[1] + u[1] -0.2,2)))',
                  degree=8,
                  cell=triangle,
                  domain=mesh,
                  u=Constant((0, 0)))

    # ********* Finite dimensional spaces ********* #

    vectorP1 = VectorElement("CG", mesh.ufl_cell(), u_degree)
    RM = VectorElement('R', triangle, 0, dim=3)

    # Mixed space
    Hh = FunctionSpace(mesh, MixedElement([vectorP1, RM]))
    v, s_xi = TestFunctions(Hh)
    u, s_chi = TrialFunctions(Hh)

    nullspace = [Constant((1, 0)), Constant((0, 1)),
                 Expression(('-x[1]+0.5', 'x[0]+0.5'), degree=1)]

    nn.append(Hh.dim())

    # ******** Pure traction BCs are natural for primal ************* #

    # nothing to do

    # ******** Reference and target images ************* #

    R_h = interpolate(R, FunctionSpace(mesh, 'CG', 1))
    # T = Expression(displ_compos, domain=mesh, degree=2)
    # T.f = T0

    u_n = Function(FunctionSpace(mesh, vectorP1))

    # ******** Exact target for error computation ******* #

    u_ex_h = interpolate(u_ex, FunctionSpace(mesh, vectorP1))
    # T_ex = Expression(displ_compos,domain=mesh, degree=2)
    # T_ex.f = T0; T_ex.u = u_ex_h
    T_ex.u = u_ex_h

    # ******** Weak forms ************* #

    a = inner(C(eps(u)), eps(v)) * dx

    calA = 1.0 / dt * dot(u, v) * dx + a + dot(s_chi, s_xi) * dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi = s_xi[i]
        calA -= chi * inner(v, ns_i) * dx + xi * inner(u, ns_i) * dx

    calF = -alpha * (T - R_h) * dot(grad(T), v) * dx + dot(load_ex, v) * dx \
        + alpha * (T_ex - R) * dot(grad(T_ex), v) * dx

    # Because of load_ex, I need to remove this term: -1.0/dt*dot(u_n,v)*dx

    LHS = PETScMatrix()
    assemble(calA, tensor=LHS)
    # solver = LUSolver(LHS)
    solver = PETScKrylovSolver()
    solver.set_operator(LHS)
    solver.set_from_options()
    PETScOptions.set("ksp_type", "gmres")
    # PETScOptions.set("ksp_monitor_cancel")
    # PETScOptions.set("ksp_monitor")
    # PETScOptions.set("ksp_monitor_true_residual")
    PETScOptions.set("pc_type", "lu")
    # PETScOptions.set("pc_type", "ilu")
    # PETScOptions.set("pc_factor_levels", 6)
    RHS = None
    Sol_h = Function(Hh)

    # ******** Picard iterations **** #
    res = 1.0
    inc = 0
    tol = 1.0E-6
    maxiter = 30

    Sol_h = Function(Hh)

    while res > tol and inc < maxiter:
        print("iter {}, res = {}".format(inc, res))
        T.u = u_n
        RHS = assemble(calF, tensor=RHS)
        solver.solve(Sol_h.vector(), RHS)
        u_h, chi_h = Sol_h.split(True)

        res = pow(assemble((u_h - u_n)**2 * dx), 0.5)
        assign(u_n, u_h)

        inc += 1

    it.append(inc)

    # ******** Exporting solutions **** #
    u_h.rename("u", "u")
    fileO.write(u_h, nk * 1.0)
    R_h.rename("R", "R")
    fileO.write(R_h, nk * 1.0)
    T_h = interpolate(T, FunctionSpace(mesh, 'CG', 1))
    T_h.rename("T", "T")
    fileO.write(T_h, nk * 1.0)

    # *** Computing errors
    eu.append(errornorm(u_ex, u_h, 'H1'))

    if(nk > 0):
        ru.append(ln(eu[nk] / eu[nk - 1]) / ln(hh[nk] / hh[nk - 1]))


# Generating convergence history
print('DoF   &  hh  e(u)  &  r(u)  & iter')
print('==========================================')
for nk in range(nkmax):
    print('%d & %4.4g & %3.3e & %4.4g & %d' % (nn[nk], hh[nk], eu[nk], ru[nk], it[nk]))
