from __future__ import print_function
from dolfin import *
import sympy2fenics as sf

parameters["form_compiler"]["cpp_optimize"] = True
parameters["allow_extrapolation"]= True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"
fileO = XDMFFile("outputs/primal-elast-aposteriori.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# ********* Model coefficients and parameters ********* #

#E     = Constant(1.0e3)
#nu    = Constant(0.4)
#lmbda = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
#mu    = Constant(E/(2.*(1.+nu)))

lmbda, mu = 1.0, 1.0

C     = lambda ten: lmbda*tr(ten)*Identity(2)+2*mu*ten
eps   = lambda vec: sym(grad(vec))

print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #

Id = sf.str2sympy('((1,0),(0,1))')

uu = sf.str2sympy('(cos(pi*x)*sin(pi*y)+(x*(1-x)*y*(1-y))**2/lmbda, -sin(pi*x)*cos(pi*y)+(x*(1-x)*y*(1-y))**3/lmbda)')

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=5, cell=triangle, mu=mu, lmbda=lmbda)

sigmasigma   = 2*mu*sf.epsilon(uu) + lmbda*sf.div(uu)*Id
divsig = sf.div(sigmasigma)

ff =  - divsig
ff_code = sf.sympy2exp(ff)
ff_ex  = Expression(ff_code, degree=4, cell=triangle, mu=mu, lmbda=lmbda)

# ********* Mesh refinement loop ********* #

hh = []; nn = []; eu = []; ru = [];
ru.append(0.0); eff = []
nkmax = 5

mesh = UnitSquareMesh(2, 2)
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)

for nk in range(nkmax):
    print( "......................Refinement level : nk = ", nk)
    n = FacetNormal(mesh); hh.append(mesh.hmin())

    # ********* Finite dimensional spaces ********* #
    
    vectorP1 = VectorElement("CG", mesh.ufl_cell(), 2)
    RM       = VectorElement('R', triangle, 0, dim=3)
    
    Hh = FunctionSpace(mesh, MixedElement([vectorP1,RM]))
    nn.append(Hh.dim())
    print("h_min = {}, dim(H_h) = {}".format(hh[nk], nn[nk]))
    
    v, s_xi = TestFunctions(Hh)
    u, s_chi = TrialFunctions(Hh)

    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]+0.5','x[0]+0.5'),degree = 1)]

    # ******** Pure traction BCs are natural for primal ************* #

    # nothing to do

    # ******** Weak forms ************* #

    calA = inner(C(eps(u)),eps(v))*dx + dot(s_chi,s_xi)*dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi  = s_xi[i]
        calA += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx

    calF = dot(ff_ex,v)*dx

    Sol_h = Function(Hh)
    solve(calA == calF, Sol_h)
    u_h,chi_h = Sol_h.split(True)
    u_h.rename("u","u"); fileO.write(u_h,nk*1.0)
    eu.append(errornorm(u_ex,u_h,'H1'))
   
    if(nk>0):
        ru.append(ln(eu[nk]/eu[nk-1])/ln(hh[nk]/hh[nk-1]))

    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, 'DG', 0)
    w  = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)
    
    globalPsi = assemble(hK**2*w*(div(C(eps(u_h))) + ff_ex)**2*dx \
                         + avg(he*w)*jump(C(eps(u_h)),n)**2*dS \
                         + he*w*(C(eps(u_h))*n)**2*ds)

    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))
    eff.append(errornorm(u_ex,u_h,'H1')/Psi)

    # ********* Mesh adaptivity ******** #
    ref_ratio = 0.1
    
    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    for c in cells (mesh):
        cell_markers[c] = globalPsi[c.index()] > ref_ratio * globalPsi_max

    adapt(mesh, cell_markers)
    
    mesh.smooth(50); mesh.smooth_boundary(50)
    mesh = mesh.child()
    
    adapt(bdry, mesh); bdry = bdry.child()
        
    
# ******* Convergence history ********** *
print('DoF   &  h_min  &   e(u)  &   r(u)  & eff ')
print('=============================================')
for nk in range(nkmax):
    print('%d & %4.4g & %3.3e & %4.4g & %4.4g' % (nn[nk], hh[nk], eu[nk], ru[nk], eff[nk]))
