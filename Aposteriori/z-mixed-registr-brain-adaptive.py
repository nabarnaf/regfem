from __future__ import print_function
import numpy as np
from PIL.Image import open as PilOpen
from dolfin import *
from RegFem import Images
import time
from AndersonAcceleration import AndersonAcceleration
start = time.perf_counter()

parameters["form_compiler"]["cpp_optimize"] = True
parameters["allow_extrapolation"] = True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"

# mesh = Mesh("../Meshes/brainwebSmallCoarse_meshR.xml")
fileO = XDMFFile("outputs/mixed-reg-brain-adaptive-10p-noise.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# xpr_file = open( "compositionFunction.cpp" , 'r' )
# displ_compos = xpr_file.read()

# ******* Mechanical parameters ******* #

E = Constant(15.0)
nu = Constant(0.3)
lmbda = E*nu/((1.0+nu)*(1.0-2.0*nu))
mu = E/(2.0*(1.0+nu))

alpha = Constant(50.0)
dt = 0.01/alpha
maxiter = 20


def Cinv(ten): return 0.5/mu*ten - lmbda/(4*mu*(lmbda+mu))*tr(ten)*Identity(2)


def Tcurl(ten): return as_vector(
    (Dx(ten[0, 1], 0)-Dx(ten[0, 0], 1), Dx(ten[1, 1], 0)-Dx(ten[1, 0], 1)))


print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ********* Mesh refinement loop ********* #

nkmax = 5
order_anderson = 5
# Load images
sigma = 0.1
Rimg = np.array(PilOpen("brainR.gif"))/255
Timg = np.array(PilOpen("brainT.gif"))/255

# Add noise
np.random.seed(0)
Rimg = Rimg + np.random.normal(0, sigma, Rimg.shape)
Timg = Timg + np.random.normal(0, sigma, Timg.shape)

t_loading = time.perf_counter()

print("Loading images")
mesh_ref = UnitSquareMesh(258, 258)
mesh = UnitSquareMesh(64, 64)
images = Images.NPYImages(mesh_ref, Rimg, Timg, generate_gradients=True)
R_h = images.R
u_nn = None
T = images.T
T0 = images.T.f
print("loading time ", time.perf_counter()-start)
ii = 0
for nk in range(nkmax):
    def export(i, mesh, u_n, R_h, T, T0):
        t_export = time.perf_counter()
        Vscalar = FunctionSpace(mesh, 'CG', 1)
        Vvector = VectorFunctionSpace(mesh, 'CG', 1)
        u_out = Function(Vvector)
        u_out.interpolate(u_n)
        u_out.rename("u", "u")
        fileO.write(u_out, i)
        Rout = Function(Vscalar)
        Rout.interpolate(R_h)
        Rout.rename("R", "R")
        fileO.write(Rout, i)
        # R_h.rename("R","R"); fileO.write(R_h,i)
        T_h = Function(Vscalar)
        # T_h = interpolate(T,Vscalar)
        T_h.interpolate(T)
        T_h.rename("T", "T")
        fileO.write(T_h, i)
        # fileO.write(T,i)
        T0_h = Function(Vscalar)
        T0_h.interpolate(T0)
        T0_h.rename("T0", "T0")
        fileO.write(T0_h, i)
        # T0.rename("T0","T0"); fileO.write(T0,i)
        print("Saved step {} in {}s".format(i, time.perf_counter() - t_export))
        return i+1
    anderson = AndersonAcceleration(order_anderson)
    dt = Constant(min(0.01, mesh.hmin())/alpha)
    t_loop_init = time.perf_counter()
    # dt    = Constant(mesh.hmin()/alpha)
    print("......................Refinement level : nk = ", nk)
    n = FacetNormal(mesh)
    t = as_vector((-n[1], n[0]))

    #  ********* Defintion of function spaces  ******* #

    vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), 1)
    vectorP0 = VectorElement("DG", mesh.ufl_cell(), 0)
    P0 = FiniteElement("DG", mesh.ufl_cell(), 0)
    RM = VectorElement('R', triangle, 0, dim=3)

    Hh = FunctionSpace(mesh, MixedElement([vectorBDM, vectorBDM, RM, vectorP0, P0]))
    print("h_min = {}, DoF = {}".format(mesh.hmin(), Hh.dim()))

    tau1, tau2, s_xi,  v, eta12 = TestFunctions(Hh)
    sig1, sig2, s_chi, u, rho12 = TrialFunctions(Hh)

    sigma = as_tensor((sig1, sig2))
    tau = as_tensor((tau1, tau2))

    nullspace = [Constant((1, 0)), Constant((0, 1)),
                 Expression(('-x[1]+0.5', 'x[0]+0.5'), degree=1)]

    rho = as_tensor(((0, rho12), (-rho12, 0)))
    eta = as_tensor(((0, eta12), (-eta12, 0)))

    # ******** Pure traction BCs are essential for mixed ************* #

    bcs1 = DirichletBC(Hh.sub(0), [0, 0], 'on_boundary')
    bcs2 = DirichletBC(Hh.sub(1), [0, 0], 'on_boundary')
    bcS = [bcs1, bcs2]

    # ******** Reference and target images ************* #

    u_n = Function(FunctionSpace(mesh, vectorP0))
    sig1_n = Function(FunctionSpace(mesh, vectorBDM))
    sig2_n = Function(FunctionSpace(mesh, vectorBDM))
    sigma_n = as_tensor((sig1_n, sig2_n))

    # T = Expression(displ_compos, domain=mesh, degree=1)
    # T.f = T0
    Vu = FunctionSpace(mesh, vectorP0)
    u_n = Function(Vu)
    if u_nn:
        u_n.interpolate(u_nn)
    else:
        u_nn = u_n.copy(True)
    images.setDisplacement(u_n.cpp_object())

    # ******** Weak form of the linearised problem ************* #

    dx = dx(mesh)
    ds = ds(mesh)

    a = inner(Cinv(sigma), tau)*dx
    bt = dot(u, div(tau))*dx + inner(rho, tau) * dx
    b = dot(v, div(sigma))*dx + inner(eta, sigma) * dx

    calA = -1.0/dt*dot(u, v)*dx + a + bt + b + dot(s_chi, s_xi)*dx  # + dot(sigma*n,tau*n)*ds
    calA_err = a + bt + b + dot(s_chi, s_xi)*dx  # + dot(sigma*n,tau*n)*ds

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi = s_xi[i]
        calA -= chi*dot(v, ns_i)*dx + xi*dot(u, ns_i)*dx
        calA_err -= chi*dot(v, ns_i)*dx + xi*dot(u, ns_i)*dx

    calF = alpha*(T-R_h)*dot(images.dT, v)*dx \
        - 1.0/dt*dot(u_n, v)*dx
    calF_err = alpha*(T-R_h)*dot(images.dT, v)*dx

    LHS = PETScMatrix()
    LHS_err = PETScMatrix()
    assemble(calA, tensor=LHS)
    assemble(calA_err, tensor=LHS_err)
    [bc.apply(LHS) for bc in bcS]
    [bc.apply(LHS_err) for bc in bcS]
    # solver = PETScKrylovSolver("bicgstab", "ilu")
    solver = PETScKrylovSolver("gmres", "ilu")
    PETScOptions.set("pc_type", "ilu")
    PETScOptions.set("pc_factor_levels", 4)
    PETScOptions.set("ksp_atol", 1e-12)
    PETScOptions.set("ksp_rtol", 1e-8)
    solver.set_from_options()
    solver.set_operator(LHS)
    RHS = PETScVector()
    RHS_err = PETScVector()
    Sol_h = Function(Hh)
    images.setDisplacement(u_n.cpp_object())
    RHS = assemble(calF, tensor=RHS)
    RHS_err = assemble(calF_err, tensor=RHS_err)
    res0 = RHS_err.vec().norm()
    t_assembly = time.perf_counter()
    print("assembling time ", time.perf_counter()-t_loop_init)

    # ******** Picard iterations **** #
    res = 1.0
    inc = 0
    tol = 1.0E-4
    ii = export(ii, mesh, u_n, R_h, T, T0)
    while res > tol and inc < maxiter:
        t_picard_init = time.perf_counter()
        print("iter {}, res = {}".format(inc, res))
        solver.solve(Sol_h.vector(), RHS)
        sig1_h, sig2_h, chi_h, u_h, rho12_h = Sol_h.split(True)
        sigma_h = as_tensor((sig1_h, sig2_h))
        sigma_n = as_tensor((sig1_h, sig2_h))
        rho_h = as_tensor(((0, rho12_h), (-rho12_h, 0)))

        assign(u_n, u_h)
        anderson.get_next_vector(u_n.vector().vec())
        images.setDisplacement(u_n.cpp_object())
        assemble(calF, tensor=RHS)
        assemble(calF_err, tensor=RHS_err)
        [bc.apply(RHS) for bc in bcS]
        [bc.apply(RHS_err) for bc in bcS]
        ii = export(ii, mesh, u_n, R_h, T, T0)
        res = (LHS_err * Sol_h.vector() - RHS_err).vec().norm()/res0

        t_solve = time.perf_counter()
        print("solution time ", time.perf_counter()-t_picard_init)
        # res = pow(assemble((u_h-u_n)**2*dx),0.5) #\
        #      + pow(assemble(inner(sigma_n-sigma_h,sigma_n-sigma_h)*dx \
        #                     + dot(div(sigma_h)-div(sigma_n), \
        #                           div(sigma_h)-div(sigma_n))*dx), 0.5)
        inc += 1

    u_nn.assign(u_n)

    t_picard_end = time.perf_counter()
    u_h.rename("u", "u")
    fileO.write(u_h, nk*1.0)
    sig1_h.rename("sig1", "sig1")
    fileO.write(sig1_h, nk*1.0)
    sig2_h.rename("sig2", "sig2")
    fileO.write(sig2_h, nk*1.0)
    rho12_h.rename("rho12", "rho12")
    fileO.write(rho12_h, nk*1.0)

    Vscalar = FunctionSpace(mesh, 'CG', 1)
    Rout = Function(Vscalar)
    Rout.interpolate(R_h)
    Rout.rename("R", "R")
    fileO.write(Rout, nk*1.0)
    T_h = interpolate(T, Vscalar)
    T_h.rename("T", "T")
    fileO.write(T_h, nk*1.0)
    T0_h = Function(Vscalar)
    T0_h.interpolate(T0)
    T0_h.rename("T0", "T0")
    fileO.write(T0_h, nk*1.0)

    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, P0)
    w = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)

    t_io = time.perf_counter()
    print("IO time ", time.perf_counter()-t_picard_end)

    PsiK = (alpha*(T-R_h)*images.dT - div(sigma_h))**2*w*dx \
        + (sigma_h - sigma_h.T)**2*w*dx  \
        + chi_h**2*w*dx \
        + hK**2*(Tcurl(Cinv(sigma_h)+rho_h))**2*w*dx \
        + hK**2*(Cinv(sigma_h)+rho_h)**2*w*dx \
        + avg(he)*(jump((Cinv(sigma_h)+rho_h)*t))**2*avg(w)*dS \
        + he*((Cinv(sigma_h)+rho_h)*t)**2*w*ds

    globalPsi = assemble(PsiK)
    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))

    t_estimator = time.perf_counter()
    print("compute estimator time ", time.perf_counter()-t_io)

    # ********* Mesh adaptivity ******** #
    tolAdapt = 1.0E-5
    ref_ratio = 0.05

    if (Psi < tolAdapt and nk == nkmax):
        print("\nEstimated error < tolerance: %s < %s" % (Psi, tolAdapt))
        break

    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    #cell_coarsen = MeshFunction('bool', mesh, mesh.topology().dim())
    cell_markers.set_all(True)
    # cell_coarsen.set_all(True)
    for c in cells(mesh):
        cell_markers[c] = globalPsi[c.index()] > (ref_ratio * globalPsi_max)
        #cell_coarsen[c] = globalPsi[c.index()] < (0.5*ref_ratio * globalPsi_max)

    # adapt(mesh, cell_markers)
    # coarsen_mesh_by_edge_collapse(mesh, cell_coarsen) # currently only in CPP
    mesh = refine(mesh, cell_markers)
    mesh.smooth(50)  # ; mesh.smooth_boundary(50)
    # mesh = mesh.child()
    # adapt(bdry, mesh); bdry = bdry.child()

    t_refine = time.perf_counter()
    print("marking and ref time ", time.perf_counter()-t_estimator)

# ************* End **************** #
ii = export(ii, mesh_ref, u_n, R_h, T, T0)
print("Total time: ", time.perf_counter()-start)
