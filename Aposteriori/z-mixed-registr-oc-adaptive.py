from __future__ import print_function
import os
from AndersonAcceleration import AndersonAcceleration
import sympy2fenics as sf
import time
from dolfin import *

primal_oc = __import__("z-primal-registr-oc-adaptive")

parameters["form_compiler"]["cpp_optimize"] = True
# parameters["form_compiler"]["quadrature_degree"] = 6
parameters["allow_extrapolation"] = True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"


def get_time():
    return time.perf_counter()


start = get_time()

# Take values from primal example.
mesh = UnitSquareMesh(40, 40)
fileO = XDMFFile("outputs/mixed-oc-adaptive.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True

# ********* Model coefficients and parameters ********* #

# Take values from primal example.
E = primal_oc.E
nu = primal_oc.nu
lmbda = Constant(E * nu / ((1. + nu) * (1. - 2. * nu)))
mu = Constant(E / (2. * (1. + nu)))

alpha = Constant(1000)
tol = 1e-3
maxiter = 10
miniter = 0
nkmax = 5
order_anderson = 1


def Cinv(ten): return 0.5 / mu * ten - lmbda / \
    (4 * mu * (lmbda + mu)) * tr(ten) * Identity(2)


def Tcurl(ten): return as_vector(
    (Dx(ten[0, 1], 0) - Dx(ten[0, 0], 1), Dx(ten[1, 1], 0) - Dx(ten[1, 0], 1)))


# bdry = MeshFunction("size_t", mesh, 1)
# bdry.set_all(0)

R = Expression(
    "pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2) < pow(0.64/2,2) && pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2) > pow(0.33/2,2) && !(x[0]>0.5&&x[1]<0.6&&x[1]>0.4)  ? 1 : 0", degree=6)


vectorP0 = VectorElement("DG", mesh.ufl_cell(), 0)
u_n = Function(FunctionSpace(mesh, vectorP0))
# u_n = interpolate(u_n, FunctionSpace(mesh, vectorP0))

t_loading = get_time()
print("loading time ", t_loading - start)


ii = 0

for nk in range(nkmax):
    def export(i, mesh, u_n, R_h, T, T0):
        t_export = time.perf_counter()
        Vscalar = FunctionSpace(mesh, 'CG', 1)
        Vvector = VectorFunctionSpace(mesh, 'CG', 1)
        u_out = Function(Vvector)
        u_out.interpolate(u_n)
        u_out.rename("u", "u")
        fileO.write(u_out, i)
        Rout = Function(Vscalar)
        Rout.interpolate(R_h)
        Rout.rename("R", "R")
        fileO.write(Rout, i)
        # R_h.rename("R","R"); fileO.write(R_h,i)
        T_h = Function(Vscalar)
        # T_h = interpolate(T,Vscalar)
        T_h.interpolate(T)
        T_h.rename("T", "T")
        fileO.write(T_h, i)
        # fileO.write(T,i)
        T0_h = Function(Vscalar)
        T0_h.interpolate(T0)
        T0_h.rename("T0", "T0")
        fileO.write(T0_h, i)
        # T0.rename("T0","T0"); fileO.write(T0,i)
        print("Saved step {} in {}s".format(i, time.perf_counter() - t_export))
        return i+1
    hmin = mesh.hmin()
    anderson = AndersonAcceleration(order_anderson)
    dt = Constant(0.1 * hmin / alpha)

    # ******** Reference and target images ************* #
    mesh, u_new, R_h, T, T0, _ = primal_oc.solve_iteration(ii, mesh, nk, u_n)

    u_n = interpolate(u_new, FunctionSpace(mesh, vectorP0))

    T = Expression("pow(x[0] + u[0] - 0.5, 2) + pow(x[1] + u[1] - 0.5, 2) < pow(0.5/2,2) ? 1 : 0",
                   degree=6, u=Constant((0, 0)), domain=mesh)
    T.u = u_n

    t_loop_init = get_time()
    print("......................Refinement level : nk = ", nk)
    n = FacetNormal(mesh)
    t = as_vector((-n[1], n[0]))

    # ********* Finite dimensional spaces ********* #

    vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), 1)
    vectorP0 = VectorElement("DG", mesh.ufl_cell(), 0)
    P0 = FiniteElement("DG", mesh.ufl_cell(), 0)
    RM = VectorElement('R', triangle, 0, dim=3)

    # Mixed space
    Hh = FunctionSpace(mesh, MixedElement(
        [vectorBDM, vectorBDM, RM, vectorP0, P0]))

    tau1, tau2, s_xi, v, eta12 = TestFunctions(Hh)
    sig1, sig2, s_chi, u, rho12 = TrialFunctions(Hh)

    sigma = as_tensor((sig1, sig2))
    tau = as_tensor((tau1, tau2))

    nullspace = [Constant((1, 0)), Constant((0, 1)),
                 Expression(('-x[1]+0.5', 'x[0]-0.5'), degree=1)]

    rho = as_tensor(((0, rho12), (-rho12, 0)))
    eta = as_tensor(((0, eta12), (-eta12, 0)))

    # ******** Pure traction BCs are essential for mixed ****** #

    bcs1 = DirichletBC(Hh.sub(0), [0, 0], 'on_boundary')
    bcs2 = DirichletBC(Hh.sub(1), [0, 0], 'on_boundary')
    bcS = [bcs1, bcs2]

    sig1_n = Function(FunctionSpace(mesh, vectorBDM))
    sig2_n = Function(FunctionSpace(mesh, vectorBDM))
    sigma_n = as_tensor((sig1_n, sig2_n))

    # ******** Weak forms ************* #

    dx = dx(mesh)
    ds = ds(mesh)
    dS = dS(mesh)

    a = inner(Cinv(sigma), tau) * dx
    bt = dot(u, div(tau)) * dx + inner(rho, tau) * dx
    b = dot(v, div(sigma)) * dx + inner(eta, sigma) * dx

    a_target = a + bt + b + dot(s_chi, s_xi) * dx
    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi = s_xi[i]
        a_target -= chi * inner(v, ns_i) * dx + xi * inner(u, ns_i) * dx
    calA = -1.0 / dt * dot(u, v) * dx + a_target
    calA_err = a_target  # + dot(sigma*n,tau*n)*ds

    F_target = alpha * (T - R) * dot(grad(T), v) * dx
    calF = F_target - 1 / dt * dot(u_n, v) * dx
    calF_err = F_target

    if nk == 0:
        Sol_h = Function(Hh)
    else:
        Sol_h = interpolate(Sol_h, Hh)
    LHS = PETScMatrix()
    LHS_err = PETScMatrix()
    assemble(calA, tensor=LHS)
    assemble(calA_err, tensor=LHS_err)
    [bc.apply(LHS) for bc in bcS]
    [bc.apply(LHS_err) for bc in bcS]
    # solver = PETScKrylovSolver("bicgstab", "ilu")
    solver = PETScKrylovSolver("gmres", "ilu")
    PETScOptions.set("pc_type", "ilu")
    PETScOptions.set("pc_factor_levels", 4)
    PETScOptions.set("ksp_atol", 1e-12)
    PETScOptions.set("ksp_rtol", 1e-8)
    solver.set_from_options()
    solver.set_operator(LHS)
    RHS = PETScVector()
    RHS_err = PETScVector()
    Sol_h = Function(Hh)
    T.u = u_n
    RHS = assemble(calF, tensor=RHS)
    RHS_err = assemble(calF_err, tensor=RHS_err)
    res0 = RHS_err.vec().norm()
    t_assembly = time.perf_counter()
    print("assembling time ", time.perf_counter()-t_loop_init)

    # ******** Picard iterations **** #
    res = 1.0
    inc = 0
    tol = 1.0E-4
    ii = export(ii, mesh, u_n, R_h, T, T0)
    while res > tol and inc < maxiter or inc < miniter:
        t_picard_init = time.perf_counter()
        print("iter {}, res = {}".format(inc, res))
        solver.solve(Sol_h.vector(), RHS)
        sig1_h, sig2_h, chi_h, u_h, rho12_h = Sol_h.split(True)
        sigma_h = as_tensor((sig1_h, sig2_h))
        sigma_n = as_tensor((sig1_h, sig2_h))
        rho_h = as_tensor(((0, rho12_h), (-rho12_h, 0)))

        assign(u_n, u_h)
        anderson.get_next_vector(u_n.vector().vec())
        T.u = u_n
        assemble(calF, tensor=RHS)
        assemble(calF_err, tensor=RHS_err)
        [bc.apply(RHS) for bc in bcS]
        [bc.apply(RHS_err) for bc in bcS]
        ii = export(ii, mesh, u_n, R_h, T, T0)
        res = (LHS_err * Sol_h.vector() - RHS_err).vec().norm()/res0

        t_solve = time.perf_counter()
        print("solution time ", time.perf_counter()-t_picard_init)
        inc += 1
        t_solve = get_time()

    # print("Average solution time {:.3f} in {} iterations".format(
        # tot_time / inc, inc))
    # import pylab as plt
    # plt.subplot(1, 2, 1)
    # plot(T, mesh=mesh)
    # plt.subplot(1, 2, 2)
    # plot(mesh)
    # plt.show()

    # ******** Exporting solutions **** #
    t_picard_end = get_time()
    u_h.rename("u", "u")
    fileO.write(u_h, nk * 1.0)
    sig1_h.rename("sig1", "sig1")
    fileO.write(sig1_h, nk * 1.0)
    sig2_h.rename("sig2", "sig2")
    fileO.write(sig2_h, nk * 1.0)
    rho12_h.rename("rho12", "rho12")
    fileO.write(rho12_h, nk * 1.0)
    R_out = interpolate(R, FunctionSpace(mesh, 'CG', 1))
    R_out.rename("R", "R")
    fileO.write(R_out, nk * 1.0)
    T_out = interpolate(T, FunctionSpace(mesh, 'CG', 1))
    T_out.rename("T", "T")
    fileO.write(T_out, nk * 1.0)
    T.u = Constant((0, 0))
    T_out = interpolate(T, FunctionSpace(mesh, 'CG', 1))
    T_out.rename("T0", "T0")
    fileO.write(T_out, nk * 1.0)

    t_io = get_time()
    print("IO time ", t_io - t_picard_end)

    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, 'DG', 0)
    w = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)

    #chih = as_vector((chi_h.sub(0),chi_h.sub(2)))

    PsiK = (1. / dt * u_h + alpha * (T - R) * grad(T)
            - div(sigma_h))**2 * w * dx \
        + (sigma_h - sigma_h.T)**2 * w * dx  \
        + chi_h**2 * w * dx \
        + hK**2 * (Tcurl(Cinv(sigma_h) + rho_h))**2 * w * dx \
        + hK**2 * (Cinv(sigma_h) + rho_h)**2 * w * dx \
        + avg(he) * (jump((Cinv(sigma_h) + rho_h) * t))**2 * avg(w) * dS \
        + he * ((Cinv(sigma_h) + rho_h) * t)**2 * w * ds

    globalPsi = assemble(PsiK)
    globalPsi = globalPsi.get_local()
    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))

    t_estimator = get_time()
    print("compute estimator time ", t_estimator - t_io)

    # eff.append(
    # (pow(E_s, 0.5) + errornorm(u_ex, u_h, 'L2') + pow(E_rho, 0.5)) / Psi)

    # ********* Mesh adaptivity ******** #
    tolAdapt = 1.0E-5
    ref_ratio = 0.075

    if (Psi < tolAdapt and nk == nkmax):
        print("\nEstimated error < tolerance: %s < %s" % (Psi, tolAdapt))
        break

    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    cell_markers.set_all(True)
    for c in cells(mesh):
        cell_markers[c] = globalPsi[c.index()] > (ref_ratio * globalPsi_max)

    mesh = refine(mesh, cell_markers)

    # mesh.smooth(50)
    # mesh.smooth_boundary(50)
    # mesh = mesh.child()

    # adapt(bdry, mesh)
    # bdry = bdry.child()
    t_refine = get_time()
    print("marking and ref time ", t_refine - t_estimator)


# Generating convergence history
# print('DoF   & min(h)  &   e(s)  &   r(s)  &  e(u)  &   r(u)  &  e(r)  &  r(r) &  eff  & iter')
# print('===========================================================================')
# for nk in range(nkmax):
#     print('%d & %4.4g & %g & %4.4g & %3.3e & %4.4g & %3.3e & %4.4g & %4.4g & %d' % (
#         nn[nk], hh[nk], es[nk], rs[nk], eu[nk], ru[nk], er[nk], rr[nk], eff[nk], it[nk]))

print("Total time: ", get_time() - start)

fileO.close()
