from __future__ import print_function
from dolfin import *
import sympy2fenics as sf
from AndersonAcceleration import AndersonAcceleration

parameters["form_compiler"]["cpp_optimize"] = True
parameters["allow_extrapolation"] = True
parameters["refinement_algorithm"] = "plaza_with_parent_facets"
fileO = XDMFFile("outputs/mixed-reg-adaptive.xdmf")
fileO.parameters["functions_share_mesh"] = True
fileO.parameters["flush_output"] = True


# ********* Model coefficients and parameters ********* #

E = Constant(1.0e3)
nu = Constant(0.49999)
lmbda = Constant(E * nu / ((1. + nu) * (1. - 2. * nu)))
mu = Constant(E / (2. * (1. + nu)))
alpha = Constant(100)
dt = Constant(0.1 / alpha)
order_anderson = 10


def C(ten): return lmbda * tr(ten) * Identity(2) + 2 * mu * ten


def Cinv(ten): return 0.5 / mu * ten - lmbda / (4 * mu * (lmbda + mu)) * tr(ten) * Identity(2)


def Tcurl(ten): return as_vector(
    (Dx(ten[0, 1], 0)-Dx(ten[0, 0], 1), Dx(ten[1, 1], 0)-Dx(ten[1, 0], 1)))


def eps(vec): return sym(grad(vec))


print("lmbda = %g, mu = %g" % (float(lmbda), float(mu)))

# ******* Exact solutions and forcing terms for error analysis ****** #
Id = sf.str2sympy('((1,0),(0,1))')

# solution such that zero traction everywhere on the boundary
# uu = sf.str2sympy('(0.1*cos(pi*x)*sin(pi*y)+0.5*(x*(1-x)*y*(1-y))**2/lmbda, -0.1*sin(pi*x)*cos(pi*y)+0.5*(x*(1-x)*y*(1-y))**3/lmbda)')
uu = sf.str2sympy(
    '(0.1*cos(2*pi*x)*sin(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**2/{lamb}, -0.1*sin(2*pi*x)*cos(2*pi*y)+0.5*(x*(1-x)*y*(1-y))**3/{lamb})'.format(lamb=lmbda(0)))

u_code = sf.sympy2exp(uu)
u_ex = Expression(u_code, degree=6, cell=triangle, lmbda=lmbda)
gradu = sf.grad(uu)
gradu_code = sf.sympy2exp(gradu)
gradu_ex = Expression(gradu_code, degree=6, cell=triangle, lmbda=lmbda)

ref = sf.str2sympy('sin(2*pi*x)*sin(2*pi*y)')
ref_code = sf.sympy2exp(ref)
R = Expression(ref_code, degree=6, cell=triangle)

targ0 = sf.str2sympy('sin(2*pi*x)*sin(2*pi*y)')
x, y = sf.symbols('x y')
targ0 = targ0.subs({'x': x - uu[0], 'y': y - uu[1]})

# Create exact Expression and enable evaluation of u
targ0_code = sf.sympy2exp(targ0)
T0 = Expression(targ0_code,
                degree=6,
                cell=triangle,
                u=Constant((0, 0)))
targ0_code = targ0_code.replace('x[0]', 'x[0] + u[0]').replace('x[1]', 'x[1] + u[1]')

T = Expression(targ0_code,
               degree=6,
               cell=triangle,
               u=Constant((0, 0)))
T_ex = Expression(targ0_code,
                  degree=6,
                  cell=triangle,
                  u=Constant((0, 0)))

rhorho = sf.omega(uu)
rho_code = sf.sympy2exp(rhorho)
rho_ex = Expression(rho_code, degree=6, cell=triangle, lmbda=lmbda)

# sigmasigma = 2.0 * mu * sf.epsilon(uu) + lmbda * sf.div(uu) * Id
sigmasigma = 2.0 * mu(0) * sf.epsilon(uu) + lmbda(0) * sf.div(uu) * Id
sigma_code = sf.sympy2exp(sigmasigma)
sigma_ex = Expression(sigma_code, degree=6, cell=triangle, lmbda=lmbda)
divsig = sf.div(sigmasigma)
divsigma_code = sf.sympy2exp(divsig)
divsigma_ex = Expression(divsigma_code, degree=6, cell=triangle, lmbda=lmbda)

load = 1 / dt(0) * uu - divsig
# load = - divsig
load_code = sf.sympy2exp(load)
load_ex = Expression(load_code, degree=6, cell=triangle, lmbda=lmbda)
# ********* Mesh refinement loop ********* #

hh = []
nn = []
eu = []
ru = []
er = []
rr = []
es = []
rs = []
it = []
eff = []
rs.append(0.0)
ru.append(0.0)
rr.append(0.0)

nkmax = 6
mesh = UnitSquareMesh(2, 2)
bdry = MeshFunction("size_t", mesh, 1)
bdry.set_all(0)
u_nn = None

for nk in range(nkmax):
    print("......................Refinement level : nk = ", nk)
    n = FacetNormal(mesh)
    hh.append(mesh.hmin())
    t = as_vector((-n[1], n[0]))

    T = Expression(targ0_code,
                   degree=6,
                   cell=triangle,
                   domain=mesh,
                   u=Constant((0, 0)))
    T_ex = Expression(targ0_code,
                      degree=6,
                      cell=triangle,
                      domain=mesh,
                      u=Constant((0, 0)))
    anderson = AndersonAcceleration(order_anderson)

    # ********* Finite dimensional spaces ********* #

    vectorBDM = FiniteElement("BDM", mesh.ufl_cell(), 1)
    vectorP0 = VectorElement("DG", mesh.ufl_cell(), 0)
    P0 = FiniteElement("DG", mesh.ufl_cell(), 0)
    RM = VectorElement('R', triangle, 0, dim=3)

    # Mixed space
    Hh = FunctionSpace(mesh, MixedElement([vectorBDM, vectorBDM, RM, vectorP0, P0]))
    nn.append(Hh.dim())
    print("h_min = {}, dim(H_h) = {}".format(hh[nk], nn[nk]))

    tau1, tau2, s_xi,  v, eta12 = TestFunctions(Hh)
    sig1, sig2, s_chi, u, rho12 = TrialFunctions(Hh)

    sigma = as_tensor((sig1, sig2))
    tau = as_tensor((tau1, tau2))

    nullspace = [Constant((1, 0)), Constant((0, 1)),
                 Expression(('-x[1]+0.5', 'x[0]+0.5'), degree=1)]

    rho = as_tensor(((0, rho12), (-rho12, 0)))
    eta = as_tensor(((0, eta12), (-eta12, 0)))

    # ******** Pure traction BCs are essential for mixed ****** #

    bcs1 = DirichletBC(Hh.sub(0), [0, 0], 'on_boundary')
    bcs2 = DirichletBC(Hh.sub(1), [0, 0], 'on_boundary')
    bcS = [bcs1, bcs2]

    # ******** Reference and target images ************* #

    R_h = interpolate(R, FunctionSpace(mesh, 'CG', 1))
    # T = Expression(displ_compos, domain=mesh, degree=2)
    # T.f = T0

    u_n = Function(FunctionSpace(mesh, vectorP0))
    if u_nn:
        u_n.interpolate(u_nn)
    else:
        u_nn = u_n.copy(True)
    sig1_n = Function(FunctionSpace(mesh, vectorBDM))
    sig2_n = Function(FunctionSpace(mesh, vectorBDM))
    sigma_n = as_tensor((sig1_n, sig2_n))

    # ******** Exact target for error computation ******* #

    u_ex_h = interpolate(u_ex, FunctionSpace(mesh, vectorP0))
    # T_ex = Expression(displ_compos,domain=mesh, degree=2)
    # T_ex.f = T0
    T_ex.u = u_ex_h

    # ******** Weak forms ************* #

    a = inner(Cinv(sigma), tau)*dx
    bt = dot(u, div(tau))*dx + inner(rho, tau) * dx
    b = dot(v, div(sigma))*dx + inner(eta, sigma) * dx

    calA = -1.0/dt*dot(u, v)*dx + a + bt + b + dot(s_chi, s_xi)*dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi = s_xi[i]
        calA -= chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx

    calF = alpha*(T-R_h)*dot(grad(T), v)*dx - dot(load_ex, v)*dx \
        - alpha*(T_ex-R)*dot(grad(T_ex), v)*dx
    # calF = alpha*(T-R_h)*dot(grad(T),v)*dx - dot(load_ex,v)*dx

    # Sol_h = Function(Hh)
    # LHS = assemble(calA)
    # solver = LUSolver(LHS)
    # #solver = PETScKrylovSolver("gmres", "ilu")
    # RHS = None

    LHS = PETScMatrix()
    assemble(calA, tensor=LHS)
    # solver = LUSolver(LHS)
    solver = PETScKrylovSolver()
    solver.set_operator(LHS)
    # PETScOptions.set("ksp_type", "preonly")
    PETScOptions.set("ksp_type", "gmres")
    PETScOptions.set("ksp_atol", 1e-12)
    PETScOptions.set("ksp_rtol", 1e-10)
    # PETScOptions.set("ksp_monitor_cancel")
    # PETScOptions.set("ksp_monitor")
    # PETScOptions.set("ksp_monitor_true_residual")
    PETScOptions.set("pc_type", "lu")
    PETScOptions.set("pc_factor_mat_solver_type", "mumps")
    # PETScOptions.set("pc_type", "ilu")
    # PETScOptions.set("pc_factor_levels", 7)
    solver.set_from_options()
    RHS = PETScVector()
    RHS_err = PETScVector()
    Sol_h = Function(Hh)
    RHS = assemble(calF, tensor=RHS)
    [bc.apply(LHS, RHS) for bc in bcS]
    res0 = RHS.vec().norm()

    # ******** Picard iterations **** #
    res = 1.0
    inc = 0
    tol = 1.0E-8
    maxiter = 20

    while res > tol and inc < maxiter:
        print("iter {}, res = {}".format(inc, res))
        T.u = u_n
        solver.solve(Sol_h.vector(), RHS)
        sig1_h, sig2_h, chi_h, u_h, rho12_h = Sol_h.split(True)
        sigma_h = as_tensor((sig1_h, sig2_h))
        rho_h = as_tensor(((0, rho12_h), (-rho12_h, 0)))

        # res = pow(assemble((u_h-u_n)**2*dx),0.5) \
        #       + pow(assemble(inner(sigma_n-sigma_h,sigma_n-sigma_h)*dx \
        #                      + dot(div(sigma_h)-div(sigma_n),div(sigma_h)-div(sigma_n))*dx), 0.5)
        assign(u_n, u_h)
        anderson.get_next_vector(u_n.vector().vec())
        assemble(calF, tensor=RHS)
        [bc.apply(LHS, RHS) for bc in bcS]
        sigma_n = as_tensor((sig1_h, sig2_h))
        res = (LHS * Sol_h.vector() - RHS).vec().norm()/res0

        inc += 1

    it.append(inc)
    u_nn = u_n.copy(True)

    # ******** Exporting solutions **** #
    u_h.rename("u", "u")
    fileO.write(u_h, nk*1.0)
    sig1_h.rename("sig1", "sig1")
    fileO.write(sig1_h, nk*1.0)
    sig2_h.rename("sig2", "sig2")
    fileO.write(sig2_h, nk*1.0)
    rho12_h.rename("rho12", "rho12")
    fileO.write(rho12_h, nk*1.0)
    R_h.rename("R", "R")
    fileO.write(R_h, nk*1.0)
    T_h = interpolate(T, FunctionSpace(mesh, 'CG', 1))
    T_h.rename("T", "T")
    fileO.write(T_h, nk*1.0)
    T0_h = interpolate(T0, FunctionSpace(mesh, 'CG', 1))
    T0_h.rename("T0", "T0")
    fileO.write(T0_h, nk * 1.0)

    # *** Computing errors. sigmaex and rhoex are not "expressions"
    E_s = assemble(inner(sigma_ex-sigma_h, sigma_ex-sigma_h)*dx
                   + dot(div(sigma_h)-divsigma_ex, div(sigma_h)-divsigma_ex)*dx)
    E_rho = assemble(inner(rho_h-rho_ex, rho_h-rho_ex)*dx)

    es.append(pow(E_s, 0.5))
    eu.append(errornorm(u_ex, u_h, 'L2'))
    er.append(pow(E_rho, 0.5))

    if(nk > 0):
        ru.append(ln(eu[nk]/eu[nk-1])/(-0.5*ln(float(nn[nk])/nn[nk-1])))
        rs.append(ln(es[nk]/es[nk-1])/(-0.5*ln(float(nn[nk])/nn[nk-1])))
        rr.append(ln(er[nk]/er[nk-1])/(-0.5*ln(float(nn[nk])/nn[nk-1])))

    # ***** Error indicator **************** #

    Mh = FunctionSpace(mesh, 'DG', 0)
    w = TestFunction(Mh)
    hK = CellDiameter(mesh)
    he = FacetArea(mesh)

    #chih = as_vector((chi_h.sub(0),chi_h.sub(2)))

    PsiK = (1./dt*u_h + alpha*(T_h-R_h)*grad(T_h)
            - alpha*(T_ex-R)*grad(T_ex)
            - div(sigma_h) - load_ex)**2*w*dx \
        + (sigma_h - sigma_h.T)**2*w*dx  \
        + chi_h**2*w*dx \
        + hK**2*(Tcurl(Cinv(sigma_h)+rho_h))**2*w*dx \
        + hK**2*(Cinv(sigma_h)+rho_h)**2*w*dx \
        + avg(he)*(jump((Cinv(sigma_h)+rho_h)*t))**2*avg(w)*dS \
        + he*((Cinv(sigma_h)+rho_h)*t)**2*w*ds

    globalPsi = assemble(PsiK)
    globalPsi = globalPsi.get_local()
    globalPsi_max = max(globalPsi)
    Psi = sqrt(sum(globalPsi))

    eff.append((pow(E_s, 0.5)+errornorm(u_ex, u_h, 'L2')+pow(E_rho, 0.5))/Psi)

    # ********* Mesh adaptivity ******** #
    tolAdapt = 1.0E-5
    ref_ratio = 0.1

    if (Psi < tolAdapt and nk == nkmax):
        print("\nEstimated error < tolerance: %s < %s" % (Psi, tolAdapt))
        break

    cell_markers = MeshFunction('bool', mesh, mesh.topology().dim())
    cell_markers.set_all(True)
    for c in cells(mesh):
        cell_markers[c] = globalPsi[c.index()] > (ref_ratio * globalPsi_max)

    # adapt(mesh, cell_markers)

    # mesh.smooth(50); mesh.smooth_boundary(50)
    # mesh = mesh.child()
    # mesh = refine(mesh, cell_markers)
    mesh = refine(mesh)  # Uniform
    # mesh.smooth(50)#; mesh.smooth_boundary(50)

    # adapt(bdry, mesh); bdry = bdry.child()

# Generating convergence history
print('DoF   & min(h)  &   e(s)  &   r(s)  &  e(u)  &   r(u)  &  e(r)  &  r(r) &  eff  & iter')
print('===========================================================================')
for nk in range(nkmax):
    print('%d & %4.4g & %g & %4.4g & %3.3e & %4.4g & %3.3e & %4.4g & %4.4g & %d' %
          (nn[nk], hh[nk], es[nk], rs[nk], eu[nk], ru[nk], er[nk], rr[nk], eff[nk], it[nk]))
