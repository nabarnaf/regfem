"""
Translation example for stable article
"""

from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
import matplotlib.pyplot as plt

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True
name = "rotation"
doPlot = True
# Solver parameters
max_iters = 1000
tolerance = 1e-8

# Physical paramters
E = Constant(15)
nu = Constant(0.49999)
lamb = E * nu / ((1.0 + nu) * (1.0 - 2.0 * nu))
mu = E / (2.0 * (1.0 + nu))
beta = 1
alpha = 500

# Mesh
N = 20
mesh = UnitSquareMesh(N, N)
dt =  0.1 / alpha

images = Images.RotationImages(mesh, C=20, a=0.4)
reg = Regularizer.ElasticPrimalRegularizer(mu, lamb)
sim = Similarity.SimilaritySSD(images)
params = Parameters.ElasticRegistrationParamters()
params.mu = mu
params.lamb = lamb
params.alpha = alpha
params.beta = beta
params.dt = dt
params.u_degree = 1
params.do_extended = True
params.do_time_regularization = True
params.time_regularization_norm = "L2"
params.bc_type = "Neumann"

solverParameters = Parameters.solverParameters()
solverParameters.max_iters = 10
solverParameters.tolerance = tolerance
solverParameters.stopping_criterion = "residual"

# Primal problem solution
RP = RegistrationProblem.ElasticRegistration(sim, reg, images, params, mesh)
RP.setup()
sol, its = RP.solve(solverParameters, printEvery=1)
RP.export(name, "primal")


if doPlot:
    plt.subplot(221)
    plot(RP.images.R, mesh=mesh)
    plt.subplot(222)
    plot(RP.images.T, mesh=mesh)
    plt.subplot(223)
    plot(abs((RP.images.R - RP.images.T)), mesh=mesh)
    plt.subplot(224)
    RP.images.T.u = Constant((0, 0))
    plot(RP.images.T, mesh=mesh)
    plt.show()
    plot(RP.u_n)

rmPrimal = sol.sub(2)(0.5, 0.5)


# Mixed problem solution
params.s_degree = 1
params.u_degree = 0
# params.dt = mesh.hmin()**2 * dt
# solverParameters.tolerance = 0.1 * tolerance
# solverParameters.max_iters = 1
RPmixed = RegistrationProblem.MixedElasticRegistration(
    sim, reg, images, params, mesh)
RPmixed.setup()
# RPmixed.u_n.interpolate(RP.u_n)

solverParameters.max_iters = max_iters
sol, its = RPmixed.solve(solverParameters, printEvery=1)
RPmixed.export(name, "mixed")

if doPlot:
    plt.subplot(221)
    plot(RPmixed.images.R, mesh=mesh)
    plt.subplot(222)
    plot(RPmixed.images.T, mesh=mesh)
    plt.subplot(223)
    plot(abs((RPmixed.images.R - RPmixed.images.T)), mesh=mesh)
    plt.subplot(224)
    RPmixed.images.T.u = Constant((0, 0))
    plot(RPmixed.images.T, mesh=mesh)
    plt.show()

rmMixed = sol.sub(5)(0.5, 0.5)

s1 = "Primal rigid motion components:{}\n".format(rmPrimal)
s2 = "Mixed rigid motion components:{}".format(rmMixed)
print(s1)
print(s2)


with open("rotation.log", "w") as f:
    f.writelines([s1, s2])
