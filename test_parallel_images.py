from dolfin import *
import numpy as np

# Problem definition
N = 50
mesh = UnitSquareMesh(N, N)
print("Divided mesh lenth=", len(mesh.coordinates()), flush=True)
V    = FunctionSpace(mesh, 'CG', 1)
Vv   = VectorFunctionSpace(mesh, 'CG', 1)
F    = Function(V)  # Resulting displaced images
# MPI params
RANK = MPI.comm_world.rank

if RANK == 0:
    # Create Function
    meshIm = UnitSquareMesh(comm=MPI.comm_self, nx=N, ny=N)
    print("Local mesh length=", len(meshIm.coordinates()), flush=True)
    VIm = FunctionSpace(meshIm, 'CG', 1)
    Vvloc= VectorFunctionSpace(meshIm, 'CG', 1)

def generateImageFunction(arr, U=None):
    """
    Code extract from fenics-diffeomorphic
    """
    if not U:
        U = Function(VectorFunctionSpace(meshIm, 'CG', 1))    
    I   = Function(VIm)
    
    
    # Array to store vertex values in
    import numpy as np
    vertex_values = np.zeros(meshIm.num_vertices())

    print("Starting image loop", flush=True)
    # Loop through array
    if arr.ndim == 2:
        for vertex in vertices(meshIm):
            vx = vertex.x(0)
            vy = vertex.x(1)
            Lx, Ly = arr.shape
            x = vx + U(vx, vy)[0]
            y = vy + U(vx, vy)[1]
            i = int(np.trunc((Lx-1)*x))
            j = int(np.trunc((Ly-1)*y))
            vertex_values[vertex.index()] = arr[i,j].item() if i<N and j<N else 0
    elif arr.ndim == 3:
        for vertex in vertices(meshIm):
            Lx, Ly, Lz = arr.shape
            i = int(np.trunc((Lx - 1)*vertex.x(0)))
            j = int(np.trunc((Ly - 1)*vertex.x(1)))
            k = int(np.trunc((Lz - 1)*vertex.x(2)))
            vertex_values[vertex.index()] = arr[i,j,k].item() if i<N and j<N and k<N else 0

    # Assign vertex values at correct dofs of I
    I.vector().set_local( vertex_values[dof_to_vertex_map(VIm)] )
    return I

# Create numpy image
if RANK == 0:
    im = np.zeros((N,N))
    x, y = np.meshgrid(np.linspace(0,1, N), np.linspace(0,1,N))
    im = (x + y)/2
    print("Creating image function...", flush = True)
    imF = generateImageFunction(im) 

# Create displacement vector
u = Function(Vv)
u.interpolate(Expression(("pow(x[0], 1)", "pow(x[1], 1)"), degree = 3))

# Gather resulting vector and mesh coords
data   = MPI.comm_world.gather(F.vector().get_local(), root=0)
coords = MPI.comm_world.gather(mesh.coordinates())

# Gather displacement
ucopy = u.copy(True)
ucopy_vec = ucopy.vector().gather_on_zero()
print("u copy vec:", ucopy_vec, flush=True)
if RANK == 0:
    im = np.zeros((N,N))
    x, y = np.meshgrid(np.linspace(0,1, N), np.linspace(0,1,N))
    im = (x + y)/2
    U = Function(Vvloc, comm=MPI.comm_self) 
    print("Tests, U size=", U.vector().size(), "ucopy_vec size=", ucopy_vec.size, flush=True)
    U.vector().set_local(ucopy_vec)
    print("Assigned vector", flush=True)
    print("Test value,", U(0.2, 0.2), flush=True)
    imF = generateImageFunction(im, U=U).vector() 
else:
    imF = Vector(MPI.comm_self)

print("bcast", flush=True)
Fvals = MPI.comm_world.bcast(imF.get_local())
print("Create vec", flush=True)
Fvec  = Vector(MPI.comm_self, V.dim())
print("Set local", flush=True)
Fvec.set_local(Fvals)
print("Fvec:", Fvec.get_local(), flush=True)
print("Create function", flush=True)
F = Function(V, Fvec)
xdmf=XDMFFile("parallel_image_test.xdmf")
xdmf.write(F, 0)
