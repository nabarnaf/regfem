#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 00:41:38 2019

@author: nico
"""


from dolfin import *
from RegFem import Images, Regularizer, Similarity, Parameters, RegistrationProblem
set_log_active(False)
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

name = "convergence_mixed_RRB"

# Solver parameters
max_iters = 10000
tolerance = 1e-10
Nreference = 6

# Physical paramters
mu    = 1
lamb  = 1
alpha = 1e-2
beta  = 1

# Methods required to handle rigid motions
def RigidMotionBase(mesh):
    """
    Generates the base for the rigid motions space. 
    """
    area = assemble(1*dx(mesh))
    e1   = Constant((1,0), cell=triangle)/area
    e2   = Constant((0,1), cell=triangle)/area
    
    e3   = Expression(['-x[1]', 'x[0]'], degree=1, domain=mesh, cell=triangle)
    e3norm = norm(e3)
    e3 = e3/e3norm
    
    e2e3 = assemble(dot(e2,e3)*dx)
    e1e3 = assemble(dot(e1,e3)*dx)
    return e1, e2, (e3 - e2e3*e2 - e1e3*e1)
    
    
def RigidMotionize(vec, mesh):
    """
    Takes a scalar vector of RM coefficients and converts it to the 
    corresponding rigid motioin
    """
    RM  = RigidMotionBase(mesh)
    
    out = 0*RM[0]  # The compiler later optimizes zero terms and doesn't add them
    for val, rm in zip(vec, RM):
        out += val*rm
    return out

def getSolutionPrimal(mesh):
    images = Images.TranslationImages(mesh)
    reg    = Regularizer.ElasticPrimalRegularizer(mu, lamb)
    sim    = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    params.mu      = mu
    params.lamb    = lamb
    params.alpha   = alpha
    params.bc_type = "Neumann"
    params.do_time_regularization = False

    RP = RegistrationProblem.ElasticRegistration(sim, reg, images, params, mesh)

    solverParameters =  Parameters.solverParameters()
    solverParameters.max_iters = 1
    solverParameters.tolerance = 1 
    RP.setup()
    u_n = Function(RP.getDisplacementSpace())
    images = Images.TranslationImagesUFL(mesh, u_n)

    a  = RP.a

    error, it = 1, 0
    while error > tolerance and it < max_iters:
        tt = TestFunctions(RP.V)
        f  = - (images.T - images.R) * as_vector([images.dTx, images.dTy])
        F = Constant(alpha) * dot(f, tt[RP.u_index]) * dx(mesh)

        sol = Function(RP.V)
        solve(a == F, sol)

        u = sol.sub(RP.u_index)
        error = errornorm(u, u_n)
        it += 1
        assign(u_n, u)
        print("it {}, error = {}".format(it, error))
    return u_n, RP.V.dim()

def getSolutionMixed(mesh):
    
    # Generate function space
    el_u  = VectorElement("DG", mesh.ufl_cell(), 0)  # Displacement
    el_ub = FiniteElement("R", mesh.ufl_cell(), 0, 1)  # Added dof
    el_s  = VectorElement("BDM", mesh.ufl_cell(), 1)  # Stress
    el_k  = VectorElement("R", mesh.ufl_cell(), 0, 3)  # Element for RM
    el_skew = FiniteElement("DG", mesh.ufl_cell(), 0)   # skew tensor
    V = FunctionSpace(mesh, MixedElement(el_s, el_k, el_u, el_ub, el_skew, el_k))
    
    u_index  = 2
    ub_index = 3
    
    # Create boundary conditions
    class Sides(SubDomain):
            def inside(self,x,on_boundary):
                return on_boundary and (near(x[0],0.0) or near(x[0], 1.0))
    class Covers(SubDomain):
        def inside(self,x,on_boundary):
            return on_boundary and (near(x[1],0.0) or near(x[1], 1.0))
    Sides = Sides(); Covers = Covers()
    bc_side = DirichletBC(V.sub(0).sub(0), Constant((0,0)), Sides)
    bc_cover = DirichletBC(V.sub(0).sub(1), Constant((0,0)), Covers)
    bcs = [bc_side, bc_cover]
    newbc = DirichletBC(V.sub(0), Constant(((0,0), (0,0))), "on_boundary")
    bcs = [newbc]


    ### Setup bilinear forms
    def eps(u):
        return sym(grad(u))
    def Cinv(e):
        mm = 2*mu
        return e/mm - lamb/(mm*(mm+2*lamb))*tr(e)*Identity(2)
    s, __rho, __u, __ub,  __phi, __lamb = TrialFunctions(V)
    tau, __eta, __v, __vb,  __psi, __xi  = TestFunctions(V)
    
    # Expand functions with the missing component in RM
    e2 = RigidMotionBase(mesh)[2]
    u    = __u + __ub * e2
    v    = __v + __vb * e2

    # Extend vectors in R3 of RM dofs to sum of vectors wieghted by unknowns
    rho   = RigidMotionize(__rho, mesh)
    lmbda = RigidMotionize(__lamb, mesh)
    xi    = RigidMotionize(__xi, mesh)
    eta   = RigidMotionize(__eta, mesh)
    
    Phi  = as_matrix([[0, __phi], [-__phi, 0]])
    Psi   = as_matrix([[0, __psi], [-__psi, 0]])
        
    a  = inner(Cinv(s), tau)
    B  = lambda _t, _e,_v, _X, _xi: dot(_v, div(_t))+inner(_t, _X)+dot(_xi - _v, _e) 
    bt = B(tau, eta, u, Phi,lmbda)
    b  = B(s, rho, v, Psi, xi)
    c  = - beta * dot(lmbda, xi)
    
    A = (a + bt + b + c) * dx
    

    u0_n = Function(V.sub(u_index).collapse())
    ub_n = Function(V.sub(ub_index).collapse())

    error, it = 1, 0
    while error > tolerance and it < max_iters:
        u_n  = u0_n + ub_n * e2
        images = Images.TranslationImagesUFL(mesh, u_n)
        f  = (images.T - images.R) * as_vector([images.dTx, images.dTy])
        F = Constant(alpha) * dot(f, v) * dx

        sol = Function(V)
        solve(A == F, sol, bcs)

        u0 = sol.sub(u_index)
        ub = sol.sub(ub_index)
        u  = u0 + ub * e2

        error = sqrt(assemble(dot(u - u_n, u - u_n) * dx ) )
        it += 1

        assign(u0_n, u0)
        assign(ub_n, ub)

        print("it {}, error = {}".format(it, error))
    sig, u, ub, phi = sol.sub(0), sol.sub(2), sol.sub(3), sol.sub(4)
    
    return sig, u, ub, phi, V.dim()

mesh = UnitSquareMesh(10,10)
print("PRIMAL TEST")
getSolutionPrimal(mesh)

print("MIXED TEST")
getSolutionMixed(mesh)

import sys
sys.exit()
#######################
### Start algorithm ###
#######################
    
print("------- Compute reference solution")
meshRef = UnitSquareMesh(2**Nreference, 2**Nreference)
sigRef, uRef, ubRef, phiRef, dimRef = getSolution(meshRef)

phiRef = project(phiRef, FunctionSpace(meshRef, 'CG', 1))  # Project so as to comply with regularity requirements of reference solution
dofs = []
hs   = []
eus   = []
esigs = []
es0s  = []
ephis = []

for n in range(1, Nreference):
    dxRef = dx(meshRef)
    print("Solving for {} elements per side".format(2**n))
    meshCur = UnitSquareMesh(2**n, 2**n)
    sig, u, ub, phi, dof = getSolution(meshCur)
    deg_rise = 2

    # Displacement error
    e2 = RigidMotionBase(meshCur)[2]
    uuRef = uRef + ubRef * e2
    uu    = u + ub * e2
    erru = sqrt(assemble((uuRef - uu)**2*dxRef))
    eus.append(erru)

    # Stress error
    esigs.append(errornorm(sigRef, sig, "Hdiv", degree_rise=deg_rise))
    es0s.append(errornorm(sigRef, sig, "L2", degree_rise=deg_rise))
     
    # Other variables
    ephis.append(errornorm(phiRef, phi, "L2", degree_rise=deg_rise))
    dofs.append(dof)
    hs.append(meshCur.hmax())
    
# Finally calculate errors
rsigs = []
rs0s  = []
rus   = []
rphis = []
Nsteps = len(dofs)
for i in range(1, Nsteps):
    lnhh = ln(hs[i]/hs[i-1])
    rsigs.append(ln(esigs[i]/esigs[i-1])/lnhh)
    rs0s.append(ln(es0s[i]/es0s[i-1])/lnhh)
    rus.append(ln(eus[i]/eus[i-1])/lnhh)
    rphis.append(ln(ephis[i]/ephis[i-1])/lnhh)
    
print("dofs & hh  &  esigL2  &  rsigL2  &  esig  &  rsig  &  eu  &  ru & ephi & rphi  \\")
for i in range(Nsteps):
    if i ==0:
        print(" {} & {:.3e} &  {:.3e}  & -- & {:.3e}  & --  & {:.3e}  &  -- & {:.3e} & -- \\".format(dofs[i], hs[i], es0s[i], esigs[i], eus[i], ephis[i]))
    else:
        print(" {} & {:.3e} &  {:.3e}  & {:.3e}  &  {:.3e}  & {:.3e}  & {:.3e}  &  {:.3e} & {:.3e}  &  {:.3e} \\".format(dofs[i], hs[i], es0s[i], rs0s[i-1], esigs[i], rsigs[i-1], eus[i], rus[i-1], ephis[i], rphis[i-1]))
