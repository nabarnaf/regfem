from dolfin import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 2

# ********* Model coefficients and parameters ********* #
d = 2

alpha = Constant(1.e4)

E         = Constant(1.e4)
nu        = Constant(0.4)
lmbda     = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu        = Constant(E/(2.*(1.+nu)))

strain = lambda v: sym(grad(v))

f = lambda v: f1 + (1.+dot(v,v))**2 * v

# ******* Fine mesh generation ****** #

nps =  120
meshFine = UnitSquareMesh(nps,nps) 
fileO = HDF5File(meshFine.mpi_comm(),"Rigid-ref-primal.h5","w")
n = FacetNormal(meshFine)

f1 = Expression(('-sin(x[0]*x[1])','x[0]+x[1]'),degree = 1)

# ********* Finite dimensional spaces ********* #
P1v = VectorElement("CG", meshFine.ufl_cell(), 1)
RM  = VectorElement('R', triangle, 0, dim=3)

Hh = FunctionSpace(meshFine,MixedElement([P1v,RM]))


# ******************** RHS ******************** #

# test and trial functions for product space

sol  = Function(Hh)
dsol = TrialFunction(Hh)
v,s_xi = TestFunctions(Hh)
u,s_chi = split(sol)

nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]','x[0]'),degree = 1)]

# ********* Boundaries and boundary conditions ********* #


# for sigma, only naturally imposed

# ******** Weak forms ************* #

aa  = inner(2.*mu*strain(u)+lmbda*div(u)*Identity(d),strain(v))*dx

FF = aa - alpha*dot(f(u),v) * dx


for i, ns_i in enumerate(nullspace):
    chi = s_chi[i]
    xi  = s_xi[i]
    FF += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx
    
Tang = derivative(FF, sol, dsol)
solve(FF==0, sol, J = Tang)    
uh,aux = sol.split()

fileO.write(uh,"uf")
fileO.close()

file1 = XDMFFile(meshFine.mpi_comm(), "viewingFine.xdmf")
uh.rename("u","u")
file1.write(uh,0)
