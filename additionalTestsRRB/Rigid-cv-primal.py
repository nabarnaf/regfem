from dolfin import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 2

# ********* Model coefficients and parameters ********* #
d = 2

alpha = Constant(1.e4)

E         = Constant(1.0e4)
nu        = Constant(0.4)
lmbda     = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu        = Constant(E/(2.*(1.+nu)))

strain = lambda v: sym(grad(v))

f = lambda v: f1 + (1.+dot(v,v))**2 * v

meshFine = UnitSquareMesh(120,120)
VhFine = VectorFunctionSpace(meshFine, "CG", 1)

# Load Pre-computed fine solution
u_hf = Function(VhFine); 
input_file = HDF5File(meshFine.mpi_comm(),"Rigid-ref-primal.h5","r")
input_file.read(u_hf,"uf")
input_file.close()



f1 = Expression(('-sin(x[0]*x[1])','x[0]+x[1]'),degree = 1)

# ******* Error analysis ****** #

hh = []; nn = []; stepslevel = [];
eu = []; ru = []; 

ru.append(0.0); 

maxsteps = 5

for steps in range(maxsteps):
    nps = pow(2,steps)+1
    mesh = UnitSquareMesh(nps,nps) 
    n = FacetNormal(mesh); hh.append(mesh.hmax())

    # ********* Finite dimensional spaces ********* #
    
    P1v = VectorElement("CG", mesh.ufl_cell(), 1)
    RM  = VectorElement('R', triangle, 0, dim=3)
    
    Vh = FunctionSpace(mesh, P1v)

    Hh = FunctionSpace(mesh,MixedElement([P1v,RM]))
    
    # exact solutions interpolated to the local spaces
    u_ex = interpolate(u_hf, Vh)

    # ******************** RHS ******************** #

    # test and trial functions for product space

    sol  = Function(Hh)
    dsol = TrialFunction(Hh)
    v,s_xi = TestFunctions(Hh)
    u,s_chi = split(sol)

    nn.append(Hh.dim())

    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]','x[0]'),degree = 1)]

    # ********* Boundaries and boundary conditions ********* #

    # for sigma, only naturally imposed
    
    # ******** Weak forms ************* #

    aa  = inner(2.*mu*strain(u)+lmbda*div(u)*Identity(d),strain(v))*dx

    FF = aa - alpha*dot(f(u),v) * dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi  = s_xi[i]
        FF += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx
    
    Tang = derivative(FF, sol, dsol)
    solve(FF==0, sol, J = Tang)
    uh,aux = sol.split()

    # Computing errors
    E_u = assemble((uh-u_ex)**2*dx)
    eu.append(pow(E_u,0.5))
    

    if(steps>0):
        ru.append(ln(eu[steps]/eu[steps-1])/ln(hh[steps]/hh[steps-1]))
        
#Generating table of convergences
print('hh   &   e(u)  &   r(u) ')
print('=====================================================================================================')
for steps in range(maxsteps):
    print('%4.4g & %3.3e & %3.3g' % (hh[steps], eu[steps], ru[steps]))
