from dolfin import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 2

# ********* Model coefficients and parameters ********* #
d = 2; k = 0

alpha = Constant(1.e4)

E         = Constant(1.e4)
nu        = Constant(0.4)
lmbda     = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu        = Constant(E/(2.*(1.+nu)))

CinvTimes = lambda s: 0.5/mu * s - lmbda/(2.*mu*(d*lmbda+2.*mu))*tr(s)*Identity(d)

f = lambda v: f1 + (1.+dot(v,v))**2 * v


# ******* Fine mesh generation ****** #

meshFine = UnitSquareMesh(120,120) 
fileO = HDF5File(meshFine.mpi_comm(),"Rigid-ref-mixed.h5","w")
n = FacetNormal(meshFine)

f1 = Expression(('-sin(x[0]*x[1])','x[0]+x[1]'),degree = 1)

# ********* Finite dimensional spaces ********* #
BDM = FiniteElement("BDM", meshFine.ufl_cell(), k+1)
P0v = VectorElement("DG", meshFine.ufl_cell(), k)
P0 = FiniteElement("DG", meshFine.ufl_cell(), k)
RM  = VectorElement('R', triangle, 0, dim=3)

# Mixed space for the elasticity and diffusion parts 
Hh   = FunctionSpace(meshFine,MixedElement([BDM,BDM,P0v,P0,RM]))

# ******************** RHS ******************** #

# test and trial functions for product space

Sol = Function(Hh)
dSol = TrialFunction(Hh)
sigma1, sigma2, u, rho12, s_chi = split(Sol)
tau1, tau2,   v, eta12, s_xi = TestFunctions(Hh)

sigma = as_tensor((sigma1,
                   sigma2))

tau = as_tensor((tau1,
                 tau2))

eta=as_tensor(((0.0,eta12),
               (-eta12,0.0)))

rho=as_tensor(((0.0,rho12),
               (-rho12,0.0)))


nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]','x[0]'),degree = 1)]

# ********* Boundaries and boundary conditions ********* #

'''
# sigma*n = 0 will be imposed by Nitsche
'''

# ******** Weak forms ************* #

aa  = inner(CinvTimes(sigma),tau)*dx    
bbt = dot(u,div(tau))*dx + inner(rho,tau) *dx 
bb  = dot(v,div(sigma))*dx + inner(eta,sigma) *dx

FF = aa + bbt + bb \
     + alpha*dot(sigma*n,tau*n)*ds \
     + alpha*dot(f(u),v) * dx #- dot(tau*n,uD)*ds(32) 

for i, ns_i in enumerate(nullspace):
    chi = s_chi[i]
    xi  = s_xi[i]
    FF += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx

    
Tang = derivative(FF, Sol, dSol)
solve(FF==0, Sol, J = Tang)

sigma1_hf,sigma2_hf,u_hf,rho12_hf,s_hchi = Sol.split()

fileO.write(u_hf,"uf")
fileO.write(sigma1_hf,"sigma1f")
fileO.write(sigma2_hf,"sigma2f")
fileO.write(rho12_hf,"rho12f")
fileO.close()

file1 = XDMFFile(meshFine.mpi_comm(), "viewingFineMixed.xdmf")
u_hf.rename("u","u")
file1.write(u_hf,0)
