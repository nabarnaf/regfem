from dolfin import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 2

# ********* Model coefficients and parameters ********* #
d = 2

alpha = Constant(1.e4)

beta  = Constant(1.1)


E         = Constant(1.e4)
nu        = Constant(0.4)
lmbda     = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu        = Constant(E/(2.*(1.+nu)))

strain = lambda v: sym(grad(v))

f = lambda v: f1 + (1.+dot(v,v))**2 * v

# ******* Fine mesh generation ****** #

nps =  120
meshFine = UnitSquareMesh(nps,nps) 
fileO = HDF5File(meshFine.mpi_comm(),"Rigid-ref-primal-XX.h5","w")
n = FacetNormal(meshFine)

f1 = Expression(('-sin(x[0]*x[1])','x[0]+x[1]'),degree = 1)

# ********* Finite dimensional spaces ********* #
P1v = VectorElement("CG", meshFine.ufl_cell(), 1)
RM  = VectorElement('R', triangle, 0, dim=3)

Hh = FunctionSpace(meshFine,MixedElement([P1v,RM,RM]))


# ******************** RHS ******************** #

# test and trial functions for product space

sol  = Function(Hh)
dsol = TrialFunction(Hh)
v,m_eta,m_xi = TestFunctions(Hh)
u,m_lam,m_rho = split(sol)

nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]','x[0]'),degree = 1)]

e0, e1, e2 = nullspace
new = e2 - assemble(dot(e2, e0) * dx(meshFine)) * e0 - assemble(dot(e2, e1) * dx(meshFine)) * e1

def as_RM(vals):
    """
    Convert a vector of 3 dofs into the span of nullspace
    """
    return vals[0] * nullspace[0] + vals[1] * nullspace[1] + vals[2] * nullspace[2]

# Convert RM dofs into RM vectors
m_eta = as_RM(m_eta)
m_xi = as_RM(m_xi)
m_lam = as_RM(m_lam)
m_rho = as_RM(m_rho)

# ********* Boundaries and boundary conditions ********* #

# for sigma, only naturally imposed

# ******** Weak forms ************* #

aa  = inner(2.*mu*strain(u)+lmbda*div(u)*Identity(d),strain(v))*dx

FF = aa - alpha*dot(f(u),v) * dx + beta*dot(m_lam,m_eta)*dx \
     + dot(v - m_eta,m_rho)*dx - dot(u - m_lam,m_xi)*dx

Tang = derivative(FF, sol, dsol)
solve(FF==0, sol, J = Tang)    
uh,aux1,aux2 = sol.split()

fileO.write(uh,"uf")
fileO.close()

file1 = XDMFFile(meshFine.mpi_comm(), "viewingFineExtendedX.xdmf")
uh.rename("u","u")
file1.write(uh,0)
