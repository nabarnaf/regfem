from dolfin import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 2

# ********* Model coefficients and parameters ********* #
d = 2; k = 0

alpha = Constant(1.e4)

E         = Constant(1.0e4)
nu        = Constant(0.4)
lmbda     = Constant(E*nu/((1.+nu)*(1.-2.*nu)))
mu        = Constant(E/(2.*(1.+nu)))

CinvTimes = lambda s: 0.5/mu * s - lmbda/(2.*mu*(d*lmbda+2.*mu))*tr(s)*Identity(d)


f = lambda v: f1 + (1.+dot(v,v))**2 * v

meshFine = UnitSquareMesh(120,120)
ShFine = FunctionSpace(meshFine, "BDM", k+1)
VhFine = VectorFunctionSpace(meshFine, "DG", k)
RhFine = FunctionSpace(meshFine, "DG", k)

f1 = Expression(('-sin(x[0]*x[1])','x[0]+x[1]'),degree = 1)

# Load Pre-computed fine solution
sigma1_hf = Function(ShFine); sigma2_hf = Function(ShFine)
u_hf = Function(VhFine); rho12_hf = Function(RhFine); 
input_file = HDF5File(meshFine.mpi_comm(),"Rigid-ref-mixed.h5","r")
input_file.read(sigma1_hf,"sigma1f")
input_file.read(sigma2_hf,"sigma2f")
input_file.read(u_hf,"uf")
input_file.read(rho12_hf,"rho12f")
input_file.close()

# ******* Error analysis ****** #

hh = []; nn = []; stepslevel = [];
eu = []; ru = []; er = []; rr = []; es = []; rs = []

rs.append(0.0); ru.append(0.0); rr.append(0.0); 

maxsteps = 5

for steps in range(maxsteps):
    nps = pow(2,steps)+1
    mesh = UnitSquareMesh(nps,nps) 
    n = FacetNormal(mesh); hh.append(mesh.hmax())

    # ********* Finite dimensional spaces ********* #
    BDM = FiniteElement("BDM", mesh.ufl_cell(), k+1)
    P0v = VectorElement("DG", mesh.ufl_cell(), k)
    P0 = FiniteElement("DG", mesh.ufl_cell(), k)

    RM  = VectorElement('R', triangle, 0, dim=3)
    
    Sh = FunctionSpace(mesh, BDM)
    Vh = FunctionSpace(mesh, P0v)
    Rh = FunctionSpace(mesh, P0)
         
    # Mixed space for the elasticity and diffusion parts 
    Hh   = FunctionSpace(mesh,MixedElement([BDM,BDM,P0v,P0,RM]))

    # exact solutions interpolated to the local spaces
    sigma1_fine = interpolate(sigma1_hf, Sh)
    sigma2_fine = interpolate(sigma2_hf, Sh)
    sigma_fine = as_tensor((sigma1_fine,
                            sigma2_fine))
    u_fine = interpolate(u_hf, Vh)
    rho12_fine = interpolate(rho12_hf, Rh)

    # ** RHS, not sure why this is needed!! ************* #

    rhs_fine = - div(sigma_fine) - alpha*f(u_fine)

    # test and trial functions for product space

    Sol = Function(Hh)
    dSol = TrialFunction(Hh)
    sigma1, sigma2, u, rho12, s_chi = split(Sol)
    tau1, tau2,   v, eta12, s_xi = TestFunctions(Hh)

    sigma = as_tensor((sigma1,
                       sigma2))

    tau = as_tensor((tau1,
                     tau2))

    eta=as_tensor(((0.0,eta12),
                   (-eta12,0.0)))

    rho=as_tensor(((0.0,rho12),
                   (-rho12,0.0)))

    nullspace=[Constant((1,0)), Constant((0,1)),\
               Expression(('-x[1]','x[0]'),degree = 1)]

    nn.append(Hh.dim())

    # ********* Boundaries and boundary conditions ********* #

    '''
    # sigma*n = 0 will be imposed by Nitsche
    '''


    # ******** Weak forms ************* #

    aa  = inner(CinvTimes(sigma),tau)*dx    
    bbt = dot(u,div(tau))*dx + inner(rho,tau) *dx 
    bb  = dot(v,div(sigma))*dx + inner(eta,sigma) *dx

    FF = aa + bbt + bb \
         + alpha*dot(sigma*n,tau*n)*ds \
         + dot(alpha*f(u)+rhs_fine,v) * dx

    for i, ns_i in enumerate(nullspace):
        chi = s_chi[i]
        xi  = s_xi[i]
        FF += chi*inner(v, ns_i)*dx + xi*inner(u, ns_i)*dx

    Tang = derivative(FF, Sol, dSol)
    solve(FF==0, Sol, J = Tang)

    sigma1_h,sigma2_h,u_h,rho12_h, s_hchi = Sol.split(True)

    sigma_h = as_tensor((sigma1_h,
                         sigma2_h))

    rho_h = as_tensor(((0,rho12_h),(-rho12_h,0)))

    # Computing errors

    E_s = assemble((sigma_fine-sigma_h)**2*dx \
                   +(div(sigma_fine)-div(sigma_h))**2*dx)
    E_u = assemble((u_fine-u_h)**2*dx)
    E_rho = assemble((rho12_fine-rho12_h)**2*dx)
      
    es.append(pow(E_s,0.5))
    eu.append(pow(E_u,0.5))
    er.append(pow(E_rho,0.5))

    if(steps>0):
        ru.append(ln(eu[steps]/eu[steps-1])/ln(hh[steps]/hh[steps-1]))
        rs.append(ln(es[steps]/es[steps-1])/ln(hh[steps]/hh[steps-1]))
        rr.append(ln(er[steps]/er[steps-1])/ln(hh[steps]/hh[steps-1]))

#Generating table of convergences
print 'hh   &   e(s)  &   r(s)  &  e(u)  &   r(u)  &  e(r)  &  r(r) '
print '====================================================================================================='
for steps in range(maxsteps):
    print '%4.4g & %g & %4.4g & %3.3e & %4.4g & %3.3e &  %4.4g ' % (hh[steps], es[steps], rs[steps], eu[steps], ru[steps], er[steps], rr[steps])
