#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 22:49:30 2019

@author: Nicolas Barnafi
"""

from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
import matplotlib.pyplot as plt
from time import time

name = "nuemann_vs_dirichlet"

# Solver parameters
max_iters = 1000
tolerance = 0.01  # Relative tolerance w.r.t initial similarity
doPlot = False
# reduction_factor = 1000

# Physical paramters
E = 1e3
nu = 0.2
lamb = E * nu / ((1. + nu) * (1. - 2. * nu))
mu = E / (2. * (1. + nu))
alpha = 1e4

# Mesh
N = 20
mesh = UnitSquareMesh(N, N)
dt = mesh.hmin() / alpha

# Setup registration components and solver parameters

reg = Regularizer.ElasticPrimalRegularizer(mu, lamb)

solverParameters = Parameters.solverParameters()
solverParameters.max_iters = max_iters
solverParameters.stopping_criterion = "similarity"


def solve_translation(bc_type):
    images = Images.TranslationImages(mesh)
    sim = Similarity.SimilaritySSD(images)

    params = Parameters.ElasticRegistrationParamters()
    params.mu = mu
    params.lamb = lamb
    params.alpha = alpha
    params.dt = dt
    params.bc_type = bc_type

    RP = RegistrationProblem.ElasticRegistration(
        sim, reg, images, params, mesh)
    RP.setup()

    solverParameters.tolerance = tolerance * assemble(sim.F * dx)
    solverParameters.similarity_threshold = 0
    _, its = RP.solve(solverParameters)
    RP.export(name, "translation_{}".format(bc_type))

    # Visualization
    if doPlot:
        plt.subplot(131)
        plot(RP.images.R, mesh=mesh)
        plt.subplot(132)
        plot(RP.images.T, mesh=mesh)
        plt.subplot(133)
        RP.images.T.u = Constant((0, 0))
        plot(RP.images.T, mesh=mesh)
        plt.show()

    return its


def solve_rotation(bc_type):
    images = Images.RotationImages(mesh)
    sim = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    params.mu = mu
    params.lamb = lamb
    params.alpha = alpha
    params.dt = dt
    params.bc_type = bc_type

    RP = RegistrationProblem.ElasticRegistration(
        sim, reg, images, params, mesh)
    RP.setup()
    solverParameters.tolerance = tolerance * assemble(sim.F * dx)
    solverParameters.similarity_threshold = 0
    _, its = RP.solve(solverParameters)
    RP.export(name, "rotation_{}".format(bc_type))

    # Visualization
    if doPlot:
        plt.subplot(131)
        plot(RP.images.R, mesh=mesh)
        plt.subplot(132)
        plot(RP.images.T, mesh=mesh)
        plt.subplot(133)
        RP.images.T.u = Constant((0, 0))
        plot(RP.images.T, mesh=mesh)
        plt.show()

    return its


# Translation with Neumann boundary conditions
t = time()
its_TN = solve_translation("Neumann")
time_TN = time() - t

# Rotation with Neumann boundary conditions
t = time()
its_RN = solve_rotation("Neumann")
time_RN = time() - t

# Translation with Dirichlet boundary conditions
t = time()
its_TD = solve_translation("Dirichlet")
time_TD = time() - t

# Rotation with Dirichlet boundary conditions
t = time()
its_RD = solve_rotation("Dirichlet")
time_RD = time() - t
print("Tranlation iterations: \n\tDirichlet={} iterations in {}s, \n\tNeumann={} in {}s".format(
    its_TD, time_TD, its_TN, time_TN))
print("Rotation iterations: \n\tDirichlet={} in {}s, \n\tNeumann={} in {}s".format(
    its_RD, time_RD, its_RN, time_RN))
