#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 23:16:26 2019

@author: nico
"""

from dolfin import *
from time import time

t = time()
#First load gif 
from PIL import Image as im

imageR = im.open("images/brainR.gif")
imageT = im.open("images/brainT.gif")

# Convert to numpy array
import numpy as np
Rarr = np.array(imageR)
Tarr = np.array(imageT)

# Read R image
from RegFem.Images import NPYImages
from dolfin import UnitSquareMesh

mesh = UnitSquareMesh(*Rarr.shape)
brainImages = NPYImages(mesh, Rarr.T, Tarr.T)

t = time() - t

# Plot images
import matplotlib.pyplot as plt
plt.subplot(121)
plot(brainImages.R)
plt.subplot(122)
plot(brainImages.T)
plt.show()

print("Time used to load images = {} s".format(t))