#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for smallest eigenvalue in both formulations
"""


from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
import numpy as np
from numpy.linalg import eigvalsh
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

Nmax = 50

def asScipySparse(A):
    from scipy.sparse import csr_matrix
    # Can't get CSR of unassembled matrix
    assert(A.mat().isAssembled())
    CSR = A.mat().getValuesCSR()
    csr = csr_matrix(CSR[::-1], shape=(A.size(0), A.size(1)))
    # Filtering is strange in csr
    return csr

def getEigenvalues(matA, sigma=0):
    A = asScipySparse(matA)
    A = A.T.dot(A)
    from scipy.sparse.linalg import eigsh, lobpcg, LinearOperator
    from scipy.sparse import spdiags

#    out = eigsh(A, k=1, sigma=sigma, which='LM', return_eigenvectors=False, tol=1e-4)  # maxiter=1e14, 
#    return abs(out.min())
    N = A.shape[0]
    X = np.random.rand(N,1)
    D = A.diagonal()
    invA = spdiags([1./D], 0, N, N)
    def precond( x ):
        return invA  * x
    M = LinearOperator(matvec=precond, shape=(N, N), dtype=float)
    
    valmin, _ = lobpcg(A, X, M=M,
                 largest=False,
                 verbosityLevel=0, 
                 tol=1e-10)
    
    valmax, _ = lobpcg(A, X, M=M,
                 largest=True,
                 verbosityLevel=0, 
                 tol=1e-10)
    return sqrt(min(valmin)), sqrt(max(valmax))


def getEigenvaluePrimal(mesh, valprimal):
    images = Images.TranslationImages(mesh)
    reg    = Regularizer.ElasticPrimalRegularizer(mu=1, lamb=1)
    sim    = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    RP = RegistrationProblem.ElasticRegistration(sim, reg, images, params, mesh)
    RP.setup()
    
    A = PETScMatrix()
    assemble(RP.a, tensor=A)
    
    return getEigenvalues(A, valprimal)
    # AA = A.array().T.dot(A.array())
    # val = eigvalsh(AA).min()
    # return sqrt(abs(val))


def getEigenvalueMixed(mesh, valmixed):
    images = Images.TranslationImages(mesh)
    reg    = Regularizer.ElasticPrimalRegularizer(mu=1, lamb=1)
    sim    = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    RP = RegistrationProblem.MixedElasticRegistration(sim, reg, images, params, mesh)
    RP.setup()
    
    A = PETScMatrix()
    assemble(RP.a, tensor=A)
    for b in RP.bc: b.apply(A)
    
    return getEigenvalues(A, valmixed)
    # AA = A.array().T.dot(A.array())
    # val = eigvalsh(AA).min()
    # return sqrt(abs(val))




outlines_primal = []
outlines_mixed = []
tags = "h & min & max & max\min \\\\"
outlines_primal.append(tags)
outlines_mixed.append(tags)
#valprimal = valmixed = 0
print(tags)
for n in range(5, Nmax+1, 5):
    print("Solving for {} elements per side".format(n))
    mesh = UnitSquareMesh(n, n)
    valsprimal = getEigenvaluePrimal(mesh, 0)
    min_primal, max_primal = valsprimal
    valsmixed  = getEigenvalueMixed(mesh, 0)
    min_mixed, max_mixed = valsmixed
    
    line_primal = "{} & {} & {} & {} \\\\".format(mesh.hmax(), min_primal, max_primal, max_primal/min_primal)
    line_mixed  = "{} & {} & {} & {} \\\\".format(mesh.hmax(), min_mixed , max_mixed,  max_mixed/min_mixed)
    print("Primal:", line_primal)
    print("Mixed:",  line_mixed)
    outlines_primal.append(line_primal)
    outlines_mixed.append(line_mixed)
    
with open('eigenvalues_primal', 'w') as f:
    f.writelines(outlines_primal)
    
with open('eigenvalues_mixed', 'w') as f:
    f.writelines(outlines_mixed)
    
    
