#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for convergence of primal registration
"""


from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

name = "convergence_primal"

# Solver parameters

Nreference = 7
degree_exact = 8

# Physical paramters
E     = 1.0e3
nu    = 0.45
lmbda = E*nu/((1.+nu)*(1.-2.*nu))
mu    = E/(2.*(1.+nu))
beta = 1e4


# Get reference solution
print("------- Compute reference solution")
#meshRef = UnitSquareMesh(2**Nreference, 2**Nreference)
#uRef, _ = getSolution(meshRef)
# Get reference solution


def generateExactTerms():
    import sympy as sp
    from time import time
    from sympy.abc import x, y, t
    from sympy.functions import Max, Min
    import RegFem.sympy2fenics as sf

    current_t = time()
    Id = sf.str2sympy('((1,0),(0,1))')

    # First create target image composition
    u_ex =  1e-1 * sp.Matrix([0.1 * sp.cos(sp.pi * x) * sp.sin(sp.pi * y) + 0.5 * (x * (1 - x) * y * (1 - y))
                             ** 2 / lmbda, -0.1 * sp.sin(sp.pi * x) * sp.cos(sp.pi * y) + 0.5 * (x * (1 - x) * y * (1 - y))**3 / lmbda])

    # Then generate differential operators
    sigma = 2.0*mu*sf.epsilon(u_ex) + lmbda*sf.div(u_ex)*Id

    # Finally create the rigid motion multipliers
    def integrate_x(f): return sp.integrate(f, (x, 0, 1))
    def integrate_y(f): return sp.integrate(f, (y, 0, 1))
    def integrate_2d(f): return integrate_x(integrate_y(f))

    def rm_projection(vec_exp, RM):
        p0 = integrate_2d(vec_exp.dot(RM[0])) * RM[0]
        p1 = integrate_2d(vec_exp.dot(RM[1])) * RM[1]
        p2 = integrate_2d(vec_exp.dot(RM[2])) * RM[2]
        return p0 + p1 + p2

    RMs = [sp.Matrix([1, 0]), sp.Matrix([0, 1]),
           sp.Matrix([-y+0.5, x-0.5])]
    lamb_rm = rm_projection(u_ex, RMs)
    rho = beta * lamb_rm

    # Now create load
    f_exp = -sf.div(sigma) + rho
    # Must be compatible
    # f_exp = f_exp - rm_projection(f_exp, RMs)

    phi_ex = sf.omega(u_ex)
    phi_ex = Expression(sf.sympy2exp(phi_ex[0, 1]), degree=degree_exact, cell=triangle)
    u_ex = Expression(sf.sympy2exp(u_ex), degree=degree_exact, cell=triangle)
    sigma_ex = Expression(sf.sympy2exp(sigma), degree=degree_exact, cell=triangle)
    div_sigma_ex = Expression(sf.sympy2exp(sf.div(sigma)), degree=degree_exact, cell=triangle)

    # Generate relevant objects
    f = Expression(sf.sympy2exp(f_exp), degree=6, cell=triangle)
    return div_sigma_ex, sigma_ex, u_ex, phi_ex, f


div_sigma_ex, sigma_ex, u_ex, phi_ex, f_ex = generateExactTerms()

# Methods required to handle rigid motions


def RigidMotionBase(mesh):
    """
    Generates the base for the rigid motions space. 
    """
    area = assemble(1 * dx(mesh))
    e1 = Constant((1, 0), cell=triangle) 
    e2 = Constant((0, 1), cell=triangle)

    e3 = Expression(['-x[1]+0.5', 'x[0]-0.5'], degree=1, domain=mesh, cell=triangle)
    return e1, e2, e3


def RigidMotionize(vec, mesh):
    """
    Takes a scalar vector of RM coefficients and converts it to the 
    corresponding rigid motioin
    """
    RM = RigidMotionBase(mesh)

    # The compiler later optimizes zero terms and doesn't add them
    out = 0 * RM[0]
    for val, rm in zip(vec, RM):
        out += val * rm
    return out


def getSolution(mesh):

    # Generate function space
    el_u = VectorElement("DG", mesh.ufl_cell(), 0)  # Displacement
    el_ub = FiniteElement("R", mesh.ufl_cell(), 0, 1)  # Added dof
    el_s = VectorElement("BDM", mesh.ufl_cell(), 1)  # Stress
    el_k = VectorElement("R", mesh.ufl_cell(), 0, 3)  # Element for RM
    el_skew = FiniteElement("DG", mesh.ufl_cell(), 0)   # skew tensor
    V = FunctionSpace(mesh, MixedElement(
        el_s, el_k, el_u, el_ub, el_skew, el_k))

    # Create boundary conditions
    bc = DirichletBC(V.sub(0), Constant(((0, 0), (0, 0))), "on_boundary")

    # Setup bilinear forms
    def eps(u):
        return sym(grad(u))

    mu_c = Constant(mu)
    lmbda_c = Constant(lmbda)

    def Cinv(e):
        mm = 2 * mu_c
        return 1 / mm * e - lmbda_c / (mm * (mm + 2 * lmbda_c)) * tr(e) * Identity(2)
    s, __rho, __u, __ub, __phi, __lamb = TrialFunctions(V)
    tau, __eta, __v, __vb, __psi, __xi = TestFunctions(V)

    # Expand functions with the missing component in RM
    e2 = RigidMotionBase(mesh)[2]
    u = __u + __ub * e2
    v = __v + __vb * e2

    # Extend vectors in R3 of RM dofs to sum of vectors wieghted by unknowns
    rho = RigidMotionize(__rho, mesh)
    lamb = RigidMotionize(__lamb, mesh)
    xi = RigidMotionize(__xi, mesh)
    eta = RigidMotionize(__eta, mesh)

    Phi = as_matrix([[0, __phi], [-__phi, 0]])
    Psi = as_matrix([[0, __psi], [-__psi, 0]])

    a = inner(Cinv(s), tau)
    def B(_t, _e, _v, _X, _xi): return dot(
        _v, div(_t)) + inner(_t, _X) + dot(_xi - _v, _e)
    bt = B(tau, eta, u, Phi, lamb)
    b = B(s, rho, v, Psi, xi)
    c = - beta * dot(lamb, xi)
    A = (a + bt + b + c) * dx
    F = -dot(f_ex, v) * dx  # Minus due to sign change in mixed formulation

    # Solve problem
    sol = Function(V)
    solver = PETScKrylovSolver('bicgstab', 'ilu')
    # solver = LUSolver()
    Amat = assemble(A)
    Fvec = assemble(F)
    bc.apply(Amat, Fvec)
    # bc.apply(Fvec)
    solver.solve(Amat, sol.vector(), Fvec)
    # s0, s1, u, ub, phi = sol.sub(0), sol.sub(1),  sol.sub(3), sol.sub(4), sol.sub(5)
    s, u, ub, phi = sol.sub(0), sol.sub(2), sol.sub(3), sol.sub(4)
    return s, u, ub, phi, V.dim()
    # return s0, s1, u, ub, phi, V.dim()


#######################
### Start algorithm ###
#######################

# print("------- Compute reference solution")
# meshRef = UnitSquareMesh(2**Nreference, 2**Nreference)
# sigRef, uRef, ubRef, phiRef, dimRef = getSolution(meshRef)

# Project so as to comply with regularity requirements of reference solution
# phiRef = project(phiRef, FunctionSpace(meshRef, 'CG', 1))
dofs = []
hs = []
eus = []
esigs = []
es0s = []
ephis = []

for n in range(2, Nreference):
    # dxRef = dx(meshRef)
    print("Solving for {} elements per side".format(2**n))
    meshCur = UnitSquareMesh(2**n, 2**n)
    sig, u, ub, phi, dof = getSolution(meshCur)
    # s0, s1, u, ub, phi, dof = getSolution(meshCur)

    # Displacement error
    e2 = RigidMotionBase(meshCur)[2]
    uu = u + ub * e2
    erru = sqrt(assemble((u_ex - uu)**2 * dx))
    eus.append(erru)

    # Stress error
    # L2err = lambda u, v: sqrt(assemble(dot(u - v, u - v) * dx))
    # Hdiverr = lambda u, v: sqrt(assemble(inner(div(u - v), div(u - v)) * dx))
    # es00 = L2err(sigma_ex[0, :], s0)
    # es01 = L2err(sigma_ex[1, :], s1)

    # esdiv0 = L2err(div_sigma_ex[0], div(s0)) + es00
    # esdiv1 = L2err(div_sigma_ex[1], div(s1)) + es01

    esigs.append(errornorm(sigma_ex, sig, "Hdiv"))
    # esigs.append(esdiv0 + esdiv1)
    es0s.append(errornorm(sigma_ex, sig, "L2"))
    # es0s.append(es00 + es01)
    # Other variables
    ephis.append(errornorm(phi_ex, phi, "L2"))
    dofs.append(dof)
    hs.append(meshCur.hmax())

# Finally calculate errors
rsigs = []
rs0s = []
rus = []
rphis = []
Nsteps = len(dofs)
for i in range(1, Nsteps):
    lnhh = ln(hs[i] / hs[i - 1])
    rsigs.append(ln(esigs[i] / esigs[i - 1]) / lnhh)
    rs0s.append(ln(es0s[i] / es0s[i - 1]) / lnhh)
    rus.append(ln(eus[i] / eus[i - 1]) / lnhh)
    rphis.append(ln(ephis[i] / ephis[i - 1]) / lnhh)

print("dofs \t hh         \t  esigL2  \t  rsigL2   \t  esig     \t  rsig     \t  eu       \t  ru      \t ephi      \t rphi       \n")
for i in range(Nsteps):
    if i == 0:
        print(" {} \t {:.1e} \t  {:.1e}  \t ------- \t {:.1e}  \t -------  \t {:.1e}  \t  ------- \t {:.1e} \t ------- ".format(
            dofs[i], hs[i], es0s[i], esigs[i], eus[i], ephis[i]))
    else:
        print(" {} \t {:.1e} \t  {:.1e}  \t {:.1e}  \t  {:.1e}  \t {:.1e}  \t {:.1e}  \t  {:.1e} \t {:.1e}  \t  {:.1e} ".format(
            dofs[i], hs[i], es0s[i], rs0s[i - 1], esigs[i], rsigs[i - 1], eus[i], rus[i - 1], ephis[i], rphis[i - 1]))


print("\n\nLatex output:\n ")
print("dofs & hh  &  esigL2  &  rsigL2  &  esig  &  rsig  &  eu  &  ru & ephi & rphi  \\")
for i in range(Nsteps):
    if i == 0:
        print(" {} & {:.3e} &  {:.3e}  & -- & {:.3e}  & --  & {:.3e}  &  -- & {:.3e} & -- \\".format(
            dofs[i], hs[i], es0s[i], esigs[i], eus[i], ephis[i]))
    else:
        print(" {} & {:.3e} &  {:.3e}  & {:.3e}  &  {:.3e}  & {:.3e}  & {:.3e}  &  {:.3e} & {:.3e}  &  {:.3e} \\".format(
            dofs[i], hs[i], es0s[i], rs0s[i - 1], esigs[i], rsigs[i - 1], eus[i], rus[i - 1], ephis[i], rphis[i - 1]))
