#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Nicolas Barnafi

Comparison between extended formulation (Neumann BC) and Dirichlet BC.
"""

from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
import matplotlib.pyplot as plt
from time import time

name = "extended_comparison"

# Solver parameters
max_iters = 200
tolerance = 0.01  # Relative tolerance w.r.t initial similarity
doPlot = False

# Physical paramters
E = 15
nu = 0.2
lamb = E * nu / ((1. + nu) * (1. - 2. * nu))
mu = E / (2. * (1. + nu))
alpha = 600

# Mesh
N = 20
mesh = UnitSquareMesh(N, N)
dt = 0.1 / alpha


# Setup registration components and solver parameters

reg = Regularizer.ElasticPrimalRegularizer(mu, lamb)

solverParameters = Parameters.solverParameters()
solverParameters.max_iters = max_iters
solverParameters.tolerance = tolerance
solverParameters.stopping_criterion = "similarity"


def solve_translation(extended):
    images = Images.TranslationImages(mesh, (0, 0))
    sim = Similarity.SimilaritySSD(images)

    params = Parameters.ElasticRegistrationParamters()
    params.mu = mu
    params.lamb = lamb
    params.alpha = alpha
    params.dt = dt
    params.bc_type = "Neumann"
    params.do_extended = extended
    params.do_time_regularization = True

    RP = RegistrationProblem.ElasticRegistration(
        sim, reg, images, params, mesh)
    RP.setup()
    solverParameters.tolerance = tolerance * assemble(sim.F * dx)
    solverParameters.similarity_threshold = 0
    sol, its = RP.solve(solverParameters)

    extra = "extended" if extended else "not_extended"
    RP.export(name, "translation_{}".format(extra))

    # Visualization
    if doPlot:
        plt.subplot(131)
        plot(RP.images.R, mesh=mesh)
        plt.subplot(132)
        plot(RP.images.T, mesh=mesh)
        plt.subplot(133)
        RP.images.T.u = Constant((0, 0))
        plot(RP.images.T, mesh=mesh)
        plt.show()

    return sol, its


def solve_rotation(extended):
    images = Images.RotationImages(mesh, a=0.0)
    sim = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    params.mu = mu
    params.lamb = lamb
    params.alpha = alpha
    params.dt = dt
    params.bc_type = "Neumann"
    params.do_extended = extended
    params.do_time_regularization = True

    RP = RegistrationProblem.ElasticRegistration(
        sim, reg, images, params, mesh)
    RP.setup()
    solverParameters.tolerance = tolerance * assemble(sim.F * dx)
    solverParameters.similarity_threshold = 0
    sol, its = RP.solve(solverParameters)
    extra = "extended" if extended else "not_extended"
    RP.export(name, "rotation_{}".format(extra))

    # Visualization
    if doPlot:
        plt.subplot(131)
        plot(RP.images.R, mesh=mesh)
        plt.subplot(132)
        plot(RP.images.T, mesh=mesh)
        plt.subplot(133)
        RP.images.T.u = Constant((0, 0))
        plot(RP.images.T, mesh=mesh)
        plt.show()

    return sol, its


from time import time
t = time()
sol_te, its_TE = solve_translation(True)
time_TE = time() - t

t = time()
sol_re, its_RE = solve_rotation(True)
time_RE = time() - t

t = time()
sol_t, its_T = solve_translation(False)
time_T = time() - t

t = time()
sol_r, its_R = solve_rotation(False)
time_R = time() - t


print("Tranlation iterations: \n\tExtended={} iterations in {}s, \n\tRegular={} in {}s".format(
    its_TE, time_TE, its_T, time_T))
print("Rotation iterations: \n\tExtended={} in {}s, \n\tRegular={} in {}s".format(
    its_RE, time_RE, its_R, time_R))
