#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test for convergence of primal registration
"""


from dolfin import *
from RegFem import RegistrationProblem, Similarity, Regularizer, Images, Parameters
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

name = "convergence_primal"

# Solver parameters
max_iters = 1000
tolerance = 1e-10
Nreference = 8

# Physical paramters
mu    = 1
lamb  = 1
alpha = 1e-1



def getSolution(mesh):
    images = Images.TranslationImages(mesh)
    reg    = Regularizer.ElasticPrimalRegularizer(mu, lamb)
    sim    = Similarity.SimilaritySSD(images)
    params = Parameters.ElasticRegistrationParamters()
    params.mu      = mu
    params.lamb    = lamb
    params.alpha   = alpha
    params.bc_type = "Neumann"
    params.do_time_regularization = False

    RP = RegistrationProblem.ElasticRegistration(sim, reg, images, params, mesh)

    solverParameters =  Parameters.solverParameters()
    solverParameters.max_iters = max_iters
    solverParameters.tolerance = tolerance 
    RP.setup()
    RP.solve(solverParameters, printEvery=1)
    return RP.u_n.copy(True), RP.V.dim()

# Get reference solution
print("------- Compute reference solution")
uRef, _ = getSolution(UnitSquareMesh(2**Nreference, 2**Nreference))

dofs = []
hs   = []
eL2s = []
eH1s = []
for n in range(2, Nreference):
    print("Solving for {} elements per side".format(2**n))
    mesh = UnitSquareMesh(2**n, 2**n)
    u, dof = getSolution(mesh)
    eL2s.append(errornorm(uRef, u, "L2", degree_rise=1))
    eH1s.append(errornorm(uRef, u, "H1", degree_rise=1))
    dofs.append(dof)
    hs.append(mesh.hmax())
    
# Finally calculate errors
rL2s = []
rH1s = []
Nsteps = len(dofs)
for i in range(1, Nsteps):
    lnhh = ln(hs[i]/hs[i-1])
    rL2s.append(ln(eL2s[i]/eL2s[i-1])/lnhh)
    rH1s.append(ln(eH1s[i]/eH1s[i-1])/lnhh)
    
print("dofs & hh  &  eL2  &  rL2  &  eH1  &  rH1  \\")
for i in range(Nsteps):
    if i ==0:
        print(" {} & {:.3e} &  {:.3e}  & --  & {:.3e}  &  -- \\".format(dofs[i], hs[i], eL2s[i], eH1s[i]))
    else:
        print(" {} & {:.3e} &  {:.3e}  & {:.3e}  & {:.3e}  &  {:.3e} \\".format(dofs[i], hs[i], eL2s[i], rL2s[i-1], eH1s[i], rH1s[i-1]))